#include <qscrollbar.h>

#include "odinplot_range.h"

#include "odincomp.h"

RangeWidget::RangeWidget(double total_min, double total_max, double start_min, double start_max, QWidget *parent)
 : QWidget(parent),
  total_min_cache(total_min), total_max_cache(total_max), react_on_scrollbar_change(true) {

  grid=new GuiGridLayout(this, 1, 3);
  grid->set_col_stretch(1,1000);

  scrollbar=new GuiScrollBar(this);
  connect(scrollbar->get_widget(), SIGNAL(valueChanged(int)), SLOT(new_scroll_val(int)));
  grid->add_widget( scrollbar->get_widget(), 0, 1 );

  min_x=new floatLineEdit(total_min, total_max, start_min, 5, this, "min_x", 3*TEXTEDIT_WIDTH/2, TEXTEDIT_HEIGHT);
  max_x=new floatLineEdit(total_min, total_max, start_max, 5, this, "max_x", 3*TEXTEDIT_WIDTH/2, TEXTEDIT_HEIGHT);
  connect(min_x, SIGNAL(floatLineEditValueChanged(float)), SLOT(new_minmax_val(float)));
  connect(max_x, SIGNAL(floatLineEditValueChanged(float)), SLOT(new_minmax_val(float)));
  grid->add_widget( min_x->get_widget(), 0, 0, GuiGridLayout::Center );
  grid->add_widget( max_x->get_widget(), 0, 2, GuiGridLayout::Center );

//  setFixedHeight(2*min_x->sizeHint().height());

  set_range(min_x->get_value(), max_x->get_value()); // initialize scrollbar
}


RangeWidget::~RangeWidget() {
  delete grid;
}


void RangeWidget::update_scrollbar() {
  Log<OdinComp> odinlog("RangeWidget","update_scrollbar");
  int scrollmin=int(total_min_cache);
  int scrollmax=int(total_max_cache);
  int linestep=int(max_x->get_value()-min_x->get_value())/10;
  int pagestep=int(max_x->get_value()-min_x->get_value());
  int margin=pagestep/2;
  int value=int(0.5*(max_x->get_value()+min_x->get_value()));

  if(linestep<=0) linestep=1;
  if(pagestep<=0) pagestep=1;
  ODINLOG(odinlog,normalDebug) << "linestep/pagestep/value=" << linestep << "/" << pagestep << "/" << value << STD_endl;

  scrollbar->set_values(scrollmin+margin, scrollmax-margin, linestep, pagestep, value);
}


void RangeWidget::check_and_set_range(double min, double max) {
  Log<OdinComp> odinlog("RangeWidget","check_and_set_range");
  ODINLOG(odinlog,normalDebug) << "min/max/total_min_cache/total_max_cache=" << min << "/" << max << "/" << total_min_cache << "/" << total_max_cache << STD_endl;

  if(min<total_min_cache) min=total_min_cache;
  if(max>total_max_cache) max=total_max_cache;
  
  if(max<min) max=min;

  min_x->set_value(min);
  max_x->set_value(max);
}


void RangeWidget::set_range(double min, double max, bool discard_scrollbar) {
  Log<OdinComp> odinlog("RangeWidget","set_range");
  check_and_set_range(min,max);
  if(!discard_scrollbar) update_scrollbar();
}



void RangeWidget::new_scroll_val(int v) {
  Log<OdinComp> odinlog("RangeWidget","new_scroll_val");

  if(!react_on_scrollbar_change) return;

  double timepoint=double(v);

  double range_half=0.5*(max_x->get_value()-min_x->get_value());
  if(timepoint<(total_min_cache+range_half)) timepoint=total_min_cache+range_half;
  if(timepoint>(total_max_cache-range_half)) timepoint=total_max_cache-range_half;

  double newmin=timepoint-range_half;
  double newmax=timepoint+range_half;

  ODINLOG(odinlog,normalDebug) << "newmin/newmax=" << newmin << "/" << newmax << STD_endl;

  check_and_set_range(newmin,newmax);

  emit new_range();
}

void RangeWidget::new_minmax_val(float) {
  check_and_set_range(min_x->get_value(), max_x->get_value());

  react_on_scrollbar_change=false;
  update_scrollbar();
  react_on_scrollbar_change=true;
  emit new_range();
}

