#include "odindialog_progress.h"
#include "odincomp.h"


ProgressDisplayDialog::ProgressDisplayDialog(QWidget *parent, bool modal)
 : GuiProgressDialog(parent, modal, 0), counter(0) { // Non-percent display by default
}

void ProgressDisplayDialog::finish() {
  GuiProgressDialog::hide();
}

void ProgressDisplayDialog::init(unsigned int nsteps, const char* txt) {
  Log<OdinComp> odinlog("ProgressDisplayDialog","init");
  ODINLOG(odinlog,normalDebug) << "nsteps/txt=" << nsteps << "/" << txt << STD_endl;
  if(txt) GuiProgressDialog::set_text(txt);
  GuiProgressDialog::set_total_steps(nsteps);
  GuiProgressDialog::reset();
  GuiProgressDialog::show();  // Show dialog at this point, necessary for Bruker dialogs
  counter=0;
}

void ProgressDisplayDialog::increase(const char*) {
  counter++; // GuiProgressDialog expects [0...nsteps]
  GuiProgressDialog::set_progress( counter );
}

bool ProgressDisplayDialog::refresh() {
  GuiApplication::process_events();
  if(GuiProgressDialog::was_cancelled()) {
    throw OdinCancel();
    return true;
  }
  return false;
}

