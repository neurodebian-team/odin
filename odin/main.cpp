#include "odin.h"
#include "odinview.h"

int main(int argc, char *argv[]) {

  // do this here so we do not have to query X server in order to get usage for manual
  if(hasHelpOption(argc, argv)) {OdinView::usage();exit(0);}

  bool has_debug_cmdline=isCommandlineOption(argc,argv,"-d",false); // Do this here before -d option is removed from argv

  GuiApplication a(argc, argv); // debug handler will be initialized here

  Log<OdinComp> odinlog("","main");

  ODINLOG(odinlog,normalDebug) << "GuiApplication::argc/argv()=" << GuiApplication::argc() << "/" << GuiApplication::argv() << STD_endl;

  Odin* odin =new Odin();

  odin->initOdin(&a,has_debug_cmdline);

  int result=a.start(odin->get_widget());
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  Static::destroy_all(); // free resources
  ODINLOG(odinlog,normalDebug) << "destroy_all done" << STD_endl;
  return result;
}


