#include "odindialog_system.h"

#include <odinqt/enumbox.h>

#include <odinseq/seqplatform.h>

SystemDialog::SystemDialog(QWidget *parent)
 : GuiDialog(parent,"SystemDialog",false),
   sysInfo_widget(0) {

  grid = new GuiGridLayout( GuiDialog::get_widget(), 3, 2 );


  svector poss_platforms(SeqPlatformProxy::get_possible_platforms());
  odinPlatform current_pf=SeqPlatformProxy::get_current_platform();

  pf=new enumBox(poss_platforms,GuiDialog::get_widget(),"Platform");
  pf->setValue(current_pf);
  grid->add_widget( pf, 0, 0, GuiGridLayout::Center );
  connect( pf, SIGNAL(newVal(int)), this,SLOT(change_platform(int)) );

  create_systemInfo_widget();


  pb_done = new GuiButton( GuiDialog::get_widget(), this, SLOT(emitDone()), "Done" );
  grid->add_widget( pb_done->get_widget(), 2, 1, GuiGridLayout::Center );
//  connect( pb_done->get_widget(), SIGNAL(clicked()), this,SLOT(emitDone()) );

  this->show();
}

SystemDialog::~SystemDialog() {
  if(sysInfo_widget) delete sysInfo_widget;
  delete pf;
  delete pb_done;
  delete grid;
}

void SystemDialog::create_systemInfo_widget() {
  if(sysInfo_widget) delete sysInfo_widget;
  SeqPlatformProxy platform;
  sysInfo_widget=new LDRwidget(platform->get_systemInfo(),1,GuiDialog::get_widget());
  sysInfo_widget->show();
  grid->add_widget( sysInfo_widget, 1, 0, GuiGridLayout::Center, 1, 2 );
  connect( sysInfo_widget, SIGNAL(valueChanged()), this,SLOT(emitChanged()) );
}

void SystemDialog::emitChanged() {
  emit new_setting();
}

void SystemDialog::emitDone() {
  emit finished();
  GuiDialog::done();
}


void SystemDialog::change_platform(int pF) {
  emit new_platform(odinPlatform(pF)); // change platform before creating new widget
  create_systemInfo_widget();
}

