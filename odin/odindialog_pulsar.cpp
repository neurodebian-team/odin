#include "odindialog_pulsar.h"

#include "odincomp.h"

#include <odinseq/seqpulsar.h>

PulsarDialog::PulsarDialog(const STD_list<const SeqPulsar*>& pulses, const STD_string& pulsarcmd, STD_list<Process>& subprocs, const STD_string& tmpdir, const STD_string& systemInfoFile, QWidget *parent)
  : GuiDialog(parent,"Pulsar Pulses",false),
    pulsar_cmd(pulsarcmd), procs(subprocs), tmp_dir(tmpdir), systemInfo_file(systemInfoFile) {


  grid=new GuiGridLayout(GuiDialog::get_widget(), 2, 1);

  svector collabel; collabel.resize(3);
  collabel[0]="Pulse (Click to view in Pulsar)";
  collabel[1]="Duration["+STD_string(ODIN_TIME_UNIT)+"]";
  collabel[2]="Properties";
  pulsar_list=new GuiListView (GuiDialog::get_widget(), collabel, 200, 300, this);
  grid->add_widget( pulsar_list->get_widget(), 0, 0 );


  for(STD_list<const SeqPulsar*>::const_iterator it=pulses.begin(); it!=pulses.end(); ++it) {
    collabel[0]=(*it)->get_label();
    collabel[1]=ftos((*it)->get_Tp());
    collabel[2]=(*it)->get_properties();
    GuiListItem* pulse_item=new GuiListItem(pulsar_list, collabel);
    pulse_map[pulse_item]=(*it);
  }

  pb_done = new GuiButton( GuiDialog::get_widget(), this, SLOT(emitDone()), "Done" );
  grid->add_widget( pb_done->get_widget(), 1, 0, GuiGridLayout::Center );
//  connect( pb_done->get_widget(), SIGNAL(clicked()), this,SLOT(emitDone()) );

  GuiDialog::show();
}


void PulsarDialog::clicked(GuiListItem* item) {
  Log<OdinComp> odinlog("PulsarDialog","clicked");
  ODINLOG(odinlog,normalDebug) << "item=" << item << STD_endl;
  if(item) {
    STD_string plsfile(tmp_dir+"pulsar.pls");
    pulse_map[item]->write(plsfile);
    STD_string cmdstring("\""+pulsar_cmd+"\" -noedit -s \""+systemInfo_file+"\" \""+plsfile+"\"");
    ODINLOG(odinlog,normalDebug) << "cmdstring=" << cmdstring << STD_endl;

    Process proc;
    proc.start(cmdstring);
    procs.push_back(proc);
  }
}

void PulsarDialog::emitDone() {
  GuiDialog::done();
}


PulsarDialog::~PulsarDialog() {
  for(STD_map<GuiListItem*, const SeqPulsar*>::const_iterator it=pulse_map.begin(); it!=pulse_map.end(); ++it) {
    delete it->first;
  }
  delete pulsar_list;
  delete pb_done;
  delete grid;
}

