#include "odindialog_process.h"

#include <odinpara/ldrtypes.h>

#include "odincomp.h"


ProcessDialog::ProcessDialog(QWidget *parent)
 : ProgressDisplayDialog(parent,false) {
  Log<OdinComp> odinlog("ProcessDialog","ProcessDialog");
}


void ProcessDialog::update_progbar() {
  GuiProgressDialog::set_progress(progcount);
  progcount++;
  GuiApplication::process_events();
}



int ProcessDialog::execute_cmds(const svector& command_chain, bool log_std_streams) {
  Log<OdinComp> odinlog("ProcessDialog","execute_cmds");

  int status=0;
  bool firstcmd=true;

  progcount=0;

  if(!command_chain.size()) return status;

  Process* procs=new Process[command_chain.size()];

//  ODINLOG(odinlog,infoLog) << STD_endl; // make sure we start a new line

  for(unsigned int icmd=0; icmd<command_chain.size(); icmd++) {

    ODINLOG(odinlog,infoLog) << "[" << icmd << "] " << command_chain[icmd] << STD_endl;

    svector cmdtoks(tokens(command_chain[icmd]));
    unsigned int ntoks=cmdtoks.size();
    if(!ntoks) {
      ODINLOG(odinlog,errorLog) << "empty command_chain[" << icmd << "]" << STD_endl;
      delete[] procs;
      return -1;
    }

    LDRfileName firstarg(cmdtoks[0]);
    GuiProgressDialog::set_text(("Running "+firstarg.get_basename()).c_str());

    if(!procs[icmd].start(command_chain[icmd],false,log_std_streams)) {
      ODINLOG(odinlog,errorLog) << "Could not start " << command_chain[icmd] << STD_endl;
      delete[] procs;
      return -1;
    }

    if(firstcmd) GuiProgressDialog::show();

    do {
      update_progbar();

      if( procs[icmd].finished(status) ) {
        ODINLOG(odinlog,normalDebug)  << " finished(" << status << ")" << STD_endl;
        break;
      }

      if(GuiProgressDialog::was_cancelled()) {
        svector extra_kill; extra_kill.resize(2);
        extra_kill[0]="cc1plus";
        extra_kill[1]="cpp";
        for(unsigned int ipid=0; ipid<command_chain.size(); ipid++) {
          procs[ipid].kill(extra_kill);
        }
        delete[] procs;
        ProgressDisplayDialog::finish();
        return -2;
      }

      sleep_ms(TIMER_INTERVAL);
    } while(1);

    firstcmd=false;
    if(status) break;
  }

  delete[] procs;
  ProgressDisplayDialog::finish();
  ODINLOG(odinlog,normalDebug)  << "returning " << status << STD_endl;
  return status;
}

