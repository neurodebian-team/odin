#include <qevent.h> // for QMouseEvent
#include <qcombobox.h> // for connecting signals
#include <qpainter.h>

#include "odinplot.h"
#include "odinplot_range.h"

#include "odindialog_progress.h"

#include "odincomp.h"


#include <tjutils/tjprofiler.h>

#include <odinseq/seqmeth.h>

#include <odinqt/ldrblockwidget.h>





static const char *rectIcon[] = {
"22 22 3 1",
"  c black",
"# c white",
"+ c yellow",
/* pixels */
"          #           ",
" ++++++++###+++++++++ ",
" +      #####       + ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +#       #        #+ ",
" ##       #        ## ",
"######################",
" ##       #        ## ",
" +#       #        #+ ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +        #         + ",
" +      #####       + ",
" ++++++++###+++++++++ ",
"          #           "
};


static const char *hortIcon[] = {
"22 22 3 1",
"  c black",
"# c white",
"+ c yellow",
/* pixels */
"                      ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +#                #+ ",
" ##                ## ",
"######################",
" ##                ## ",
" +#                #+ ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
" +                  + ",
"                      "
};


static const char *vertIcon[] = {
"22 22 3 1",
"  c black",
"# c white",
"+ c yellow",
/* pixels */
"          #           ",
" ++++++++###+++++++++ ",
"        #####         ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"          #           ",
"        #####         ",
" ++++++++###+++++++++ ",
"          #           "
};

//////////////////////////////////////////////




///////////////////////////////////////////////

PlotView::PlotView(const char* method, const STD_string& nucleus, GuiMainWindow* parent )
 : QWidget(parent->get_widget()),
   nuc(nucleus) {
  Log<OdinComp> odinlog("PlotView","PlotView");

  int iplot;
  for(iplot=0; iplot<numof_plotchan; iplot++) plotter[iplot]=0;

  printer=new GuiPrinter();

  chan_toolbar = new GuiToolBar(parent, "Channels");

  zoom_toolbar = new GuiToolBar(parent, "Zoom");

  settings_toolbar = new GuiToolBar(parent, "Settings");



  zoomflag[rect]=      new GuiToolButton(zoom_toolbar, rectIcon, "Rectangular Zoom", this, SLOT(set_rect_zoom_tool()), true );
  zoomflag[horizontal]=new GuiToolButton(zoom_toolbar, hortIcon, "Horizontal Zoom",  this, SLOT(set_hort_zoom_tool()), true );
  zoomflag[vertical]=  new GuiToolButton(zoom_toolbar, vertIcon, "Vertical Zoom",    this, SLOT(set_vert_zoom_tool()), true );

  markid_vertzoom=0;
  markid_hortzoom=0;
  
  closest=0;
  lastplot_closest=-1;
  lastcurve_closest=-1;

  for(iplot=0; iplot<numof_plotchan; iplot++) {
    curveid_baseline[iplot]=0;
    for(unsigned itc=0; itc<numof_tcmodes; itc++) timecourse_curve_id[itc][iplot]=0;
  }


  grid=new GuiGridLayout(this, numof_plotchan+1, 3);


  // getting plot data from current platform which should be StandAlone
  // to return non-null
  plotdata=SeqPlatformProxy()->get_plot_data();
  if(!plotdata) {
    ODINLOG(odinlog,errorLog) << "Unable to get plot_data" << STD_endl;
    return;
  }
  ODINLOG(odinlog,normalDebug) << "plotdata->n_frames()=" << plotdata->n_frames() << STD_endl;


  totaldur=plotdata->get_total_duration();
  double min_x_cache=0.0;
  double max_x_cache=totaldur;
  if(max_x_cache>MAX_START_RANGE) max_x_cache=MAX_START_RANGE;

  ODINLOG(odinlog,normalDebug) << "min_x_cache/max_x_cache/totaldur=" << min_x_cache << "/" << max_x_cache << "/" << totaldur << STD_endl;

  baseline_x[0]=0.0;
  baseline_x[1]=totaldur;
  baseline_y[0]=0.0;
  baseline_y[1]=0.0;


  for(iplot=0; iplot<numof_plotchan; iplot++) {
    plotter[iplot] = new GuiPlot(this,false,MIN_WIDTH_PER_CHAN,MIN_HEIGHT_PER_CHAN);

    wheel[iplot]=new GuiWheel(this);

    bool cbon=plotdata->has_curves_on_channel(plotChannel(iplot)); // default visibility

    if(iplot==B1re_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot0(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot0(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot0(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot0(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot0()), autoscale_txt);
    }
    if(iplot==B1im_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot1(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot1(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot1(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot1(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot1()), autoscale_txt);
    }
    if(iplot==rec_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot2(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot2(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot2(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot2(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot2()), autoscale_txt);
      cbon=false; // override visibilty
    }
    if(iplot==signal_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot3(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot3(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot3(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot3(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot3()), autoscale_txt);
    }
    if(iplot==freq_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot4(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot4(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot4(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot4(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot4()), autoscale_txt);
    }
    if(iplot==phase_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot5(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot5(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot5(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot5(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot5()), autoscale_txt);
    }
    if(iplot==Gread_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot6(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot6(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot6(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot6(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot6()), autoscale_txt);
    }
    if(iplot==Gphase_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot7(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot7(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot7(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot7(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot7()), autoscale_txt);
    }
    if(iplot==Gslice_plotchan) {
      connect( plotter[iplot], SIGNAL(plotMouseMoved(const QMouseEvent&)),    SLOT(mouseMovedInPlot8(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot8(const QMouseEvent&)) );
      connect( plotter[iplot], SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot8(const QMouseEvent&)) );
      connect( wheel  [iplot], SIGNAL(valueChanged(double)),                  SLOT(rescalePlot8(double)) );
//      autoscalebutton[iplot]=new GuiButton(this, this, SLOT(autoscalePlot8()), autoscale_txt);
    }

    plotflag[iplot]=new GuiToolButton(chan_toolbar, 0, "", this, SLOT(hide_and_show()), true, cbon );

    grid->add_widget( plotter[iplot]->get_widget(), iplot, 0);

    grid->add_widget( wheel[iplot]->get_widget(), iplot, 1);

//    grid->add_widget( autoscalebutton[iplot]->get_widget(), iplot, 2);
    
  }

//  canvas_framewidth=plotter[0]->canvas()->lineWidth();
//  ODINLOG(odinlog,normalDebug) << "canvas_framewidth=" << canvas_framewidth << STD_endl;

  
  x_range=new RangeWidget(0.0,totaldur,min_x_cache,max_x_cache,this);
  connect( x_range, SIGNAL(new_range()),this, SLOT(update_x_axes()) );
  grid->add_widget( x_range, numof_plotchan, 0, GuiGridLayout::Default, 1, 2);



  chan_toolbar->add_separator();

  svector modelabel; modelabel.resize(numof_tcmodes);
  for(int imode=0; imode<numof_tcmodes; imode++) modelabel[imode]=timecourseLabel[imode];
  plotmode= new GuiComboBox(chan_toolbar, modelabel );
  plotmode->set_current_item(tcmode_curves);
  oldmode=tcmode_curves;
  connect( plotmode->get_widget(), SIGNAL(activated(int)),this, SLOT(changeMode(int)) );


  chan_toolbar->add_separator();
  oscibutton=new GuiToolButton(chan_toolbar, 0, "Osci", this, SLOT(osci()));
  oscibutton->set_tooltip("Triggered oscilloscope");
  printbutton=new GuiToolButton(chan_toolbar, 0, "Print", this, SLOT(print()));
  printbutton->set_tooltip("Print sequence plot");
  savebutton=new GuiToolButton(chan_toolbar,  0, "Save", this, SLOT(save()));
  savebutton->set_tooltip("Save sequence plot as multi-column ASCII file");
  closebutton=new GuiToolButton(chan_toolbar, 0, "Close", this, SLOT(close()));
  closebutton->set_tooltip("Close sequence plot");


  plotAxes   =new GuiToolButton(settings_toolbar, 0, "Axes",    this, SLOT(settingsChanged()), true, true );
  plotMarkers=new GuiToolButton(settings_toolbar, 0, "Markers", this, SLOT(settingsChanged()), true, false );
  plotGrid   =new GuiToolButton(settings_toolbar, 0, "Grid",    this, SLOT(settingsChanged()), true, true );


  for(int i=exttrigger_marker; i<numof_markers; i++) {
    TriggerType.add_item(markLabel[i],i);
  }
  TriggerType.set_actual(excitation_marker);
  TriggerType.set_description("The plot marker used as a trigger for the oscilloscope");
  TriggerType.set_label("TriggerType");

  TriggerWidth=100.0;
  TriggerWidth.set_unit(ODIN_TIME_UNIT).set_description("The width (duration) displayed after the trigger");
  TriggerWidth.set_minmaxval(0.1,totaldur);
  TriggerWidth.set_label("TriggerWidth");

  RefreshRate=1.0;
  RefreshRate.set_unit("s").set_description("The refresh rate of the plot");
  RefreshRate.set_minmaxval(0.0,10.0);
  RefreshRate.set_label("RefreshRate");

  change_toolbar();
  set_plot_labels();
  hide_and_show();
  set_hort_zoom_tool();
  update_x_axes();
  autoscale_all_y();
//  replot();
}

PlotView::~PlotView() {
  Log<OdinComp> odinlog("PlotView","~PlotView()");

  ODINLOG(odinlog,normalDebug) << "mem(pre )=" << Profiler::get_memory_usage() << STD_endl;
  plotdata->reset(); // free memory
  ODINLOG(odinlog,normalDebug) << "mem(post)=" << Profiler::get_memory_usage() << STD_endl;

  for(unsigned int iplot=0; iplot<numof_plotchan; iplot++) {
    ODINLOG(odinlog,normalDebug) << "Deleting plotter #" << iplot << STD_endl;
    delete plotter[iplot];
    ODINLOG(odinlog,normalDebug) << "Deleting wheel #" << iplot << STD_endl;
    delete wheel[iplot];
//    ODINLOG(odinlog,normalDebug) << "Deleting autoscalebutton #" << iplot << STD_endl;
//    delete autoscalebutton[iplot];
    ODINLOG(odinlog,normalDebug) << "Deleting plotflag #" << iplot << STD_endl;
    delete plotflag[iplot];
  }
  delete oscibutton;
  delete printbutton;
  delete savebutton;
  delete closebutton;
  delete x_range;
  delete grid;
  delete printer;

  // delete at last
  delete chan_toolbar;
  delete zoom_toolbar;
  delete settings_toolbar;
}

void PlotView::close() {
  Log<OdinComp> odinlog("PlotView","close()");
  ODINLOG(odinlog,normalDebug) << "mem(pre )=" << Profiler::get_memory_usage() << STD_endl;
  plotdata->reset(); // free memory
  ODINLOG(odinlog,normalDebug) << "mem(post)=" << Profiler::get_memory_usage() << STD_endl;
  emit closeMe();
}

void PlotView::changeMode(int newmode) {
  Log<OdinComp> odinlog("PlotView","changeMode");

  for(int iplot=0; iplot<numof_plotchan; iplot++) {
    ODINLOG(odinlog,normalDebug) << "Clearing plot #" << iplot << STD_endl;
    plotter[iplot]->clear();
  }

  if(newmode>tcmode_curves) {
    ODINLOG(odinlog,normalDebug) << "Creating timecourse for mode " << newmode << STD_endl;
    if(!create_timecourses(timecourseMode(newmode))) newmode=oldmode;
  }

  if(newmode==tcmode_curves) {
    ODINLOG(odinlog,normalDebug) << "Creating & Plotting curves" << STD_endl;
    create_plotcurves_and_markers();
  } else {
    ODINLOG(odinlog,normalDebug) << "Plotting timecourse" << STD_endl;
    plot_timecourses(timecourseMode(newmode));
  }


  plotmode->set_current_item(newmode);
  change_toolbar();

  set_plot_labels();

  bool do_autoscale=true;
  if(newmode==tcmode_plain && oldmode==tcmode_curves) do_autoscale=false;
  if(oldmode==tcmode_plain && newmode==tcmode_curves) do_autoscale=false;

  if(do_autoscale) {
    autoscale_y(Gread_plotchan);
    autoscale_y(Gphase_plotchan);
    autoscale_y(Gslice_plotchan);
  }

  replot();

  oldmode=timecourseMode(newmode);
}

void PlotView::change_toolbar() {

  if(plotmode->get_current_item()==tcmode_curves) {
    savebutton->set_enabled(false);

    plotflag[freq_plotchan]->set_enabled(false);
    plotflag[phase_plotchan]->set_enabled(false);

  } else {
    savebutton->set_enabled(true);

    plotflag[freq_plotchan]->set_enabled(true);
    plotflag[phase_plotchan]->set_enabled(true);
  }
}


void PlotView::create_baseline() {
  int iplot;
  for(iplot=0; iplot<numof_plotchan; iplot++) {
    curveid_baseline[iplot]=plotter[iplot]->insert_curve(false, false, true);
    plotter[iplot]->set_curve_data(curveid_baseline[iplot],baseline_x,baseline_y,2);
  }
}

void PlotView::add_plotcurve(const Curve4Qwt& curve) {

  double minx=x_range->get_min();
  double maxx=x_range->get_max();

  if(curve.x[curve.size-1]>=minx && curve.x[0]<=maxx) {

    long curveid=plotter[curve.channel]->insert_curve(false,curve.spikes);
    curves_map[curve.channel][curveid]=curve;
    plotter[curve.channel]->set_curve_data(curveid,curve.x,curve.y,curve.size);
  }
}

void PlotView::create_plotcurves() {
  Log<OdinComp> odinlog("PlotView","create_plotcurves");
  int iplot;
  for(iplot=0; iplot<numof_plotchan; iplot++) {
     plotter[iplot]->clear();
     curves_map[iplot].clear();
  }
  create_baseline();

  STD_list<Curve4Qwt>::const_iterator curves_begin;
  STD_list<Curve4Qwt>::const_iterator curves_end;
  STD_list<Curve4Qwt>::const_iterator curvit;

  plotdata->get_curves(curves_begin,curves_end,x_range->get_min(),x_range->get_max(),MAX_HIGHRES_INTERVAL);
  for(curvit=curves_begin; curvit!=curves_end; ++curvit) {
    add_plotcurve(*curvit);
  }

  plotdata->get_signal_curves(curves_begin, curves_end, x_range->get_min(), x_range->get_max());
  for(curvit=curves_begin; curvit!=curves_end; ++curvit) {
    add_plotcurve(*curvit);
  }

  set_curve_pens();
}

void PlotView::add_marker(const Marker4Qwt& marker) {
  Log<OdinComp> odinlog("PlotView","add_marker");
  double minx=x_range->get_min();
  double maxx=x_range->get_max();
  if(marker.x>=minx && marker.x<=maxx) {
    for(int iplot=0; iplot<numof_plotchan; iplot++) {
      plotter[iplot]->insert_marker(marker.label,marker.x);
    }
  }
}

void PlotView::add_tc_marker(const TimecourseMarker4Qwt& marker) {
  double minx=x_range->get_min();
  double maxx=x_range->get_max();
  if(marker.x>=minx && marker.x<=maxx) {
    for(int iplot=0; iplot<numof_plotchan; iplot++) {
      plotter[iplot]->insert_marker(ftos(marker.y[iplot]).c_str(),marker.x);
    }
  }
}


void PlotView::create_markers() {
  Log<OdinComp> odinlog("PlotView","create_markers");

  for(int iplot=0; iplot<numof_plotchan; iplot++) plotter[iplot]->remove_markers();

  if(!plotMarkers->is_on()) return;

  if((x_range->get_max()-x_range->get_min())>MAX_MARKER_INTERVAL) return;

  timecourseMode type=timecourseMode(plotmode->get_current_item());

  if(type==tcmode_curves) {
    STD_list<Marker4Qwt>::const_iterator markers_begin;
    STD_list<Marker4Qwt>::const_iterator markers_end;
    plotdata->get_markers(markers_begin,markers_end,x_range->get_min(),x_range->get_max());
    for(STD_list<Marker4Qwt>::const_iterator markit=markers_begin; markit!=markers_end; ++markit) {
      add_marker(*markit);
    }
  } else {
    STD_list<TimecourseMarker4Qwt>::const_iterator tc_markers_begin;
    STD_list<TimecourseMarker4Qwt>::const_iterator tc_markers_end;
    plotdata->get_timecourse_markers(type,tc_markers_begin,tc_markers_end,x_range->get_min(),x_range->get_max());
    for(STD_list<TimecourseMarker4Qwt>::const_iterator tc_markit=tc_markers_begin; tc_markit!=tc_markers_end; ++tc_markit) {
      add_tc_marker(*tc_markit);
    }
  }
}


void PlotView::create_plotcurves_and_markers() {
  create_plotcurves();
  create_markers();
}



void PlotView::plot_timecourses(timecourseMode type) {
  double min=x_range->get_min();
  double max=x_range->get_max();

  const SeqTimecourseData* subtimecourse=plotdata->get_subtimecourse(type,min, max);
  if(!subtimecourse) return;

  bool symbols=( (max-min) < PLOT_SYMBOLS_MAX_RANGE );


  for(int iplot=0; iplot<numof_plotchan; iplot++) {
    plotter[iplot]->clear();
    timecourse_curve_id[type][iplot]=plotter[iplot]->insert_curve(false,false);
    plotter[iplot]->set_curve_data(timecourse_curve_id[type][iplot],subtimecourse->x,subtimecourse->y[iplot],subtimecourse->size,symbols);
  }

  set_curve_pens();
  create_markers();
}


bool PlotView::create_timecourses(timecourseMode type) {
  Log<OdinComp> odinlog("PlotView","create_timecourses");

  bool do_plot=true;
  bool first_time=false;

  if(!plotdata->timecourse_created(type)) {

    ProgressDisplayDialog display(this);
    ProgressMeter progmeter(display);
    progmeter.new_task(plotdata->n_frames(),"Generating timecourse");

    try {
      plotdata->create_timecourses(type,nuc,&progmeter);
    } catch(OdinCancel) {
      ODINLOG(odinlog,normalDebug) << " aborted" << STD_endl;
      do_plot=false;
    }
    display.finish();
    first_time=true;
  }

  if(do_plot && first_time) {
    ODINLOG(odinlog,normalDebug) << "autoscaling freq/phase" << STD_endl;
    autoscale_y(freq_plotchan);
    autoscale_y(phase_plotchan);
  }

  return do_plot;
}

void PlotView::plot_all() {
  autoscale_x();
  autoscale_all_y();
}

void PlotView::autoscale_x() {
  set_range_and_update_x_axes(0.0,totaldur);
  replot();
}

void PlotView::set_range_and_update_x_axes(double min, double max, bool discard_scrollbar) {
  x_range->set_range(min,max,discard_scrollbar);
  update_x_axes();
}

void PlotView::autoscale_all_y() {
  for(int iplot=0; iplot<numof_plotchan; iplot++) autoscale_y(iplot);
}



void PlotView::autoscale_y(int iplot) {
  Log<OdinComp> odinlog("PlotView","autoscale_y");

  double maxBound;
  plotter[iplot]->autoscale_y(maxBound);

  ODINLOG(odinlog,normalDebug) << "maxBound[" << iplot << "]=" << maxBound << STD_endl;

  wheel[iplot]->set_range(0.01*maxBound,10.0*maxBound);
  wheel[iplot]->set_value(maxBound);
  
}

void PlotView::rescale_y(int iplot, double val) {
  plotter[iplot]->rescale_y(val);
}




double PlotView::get_x(int iplot, int x_pixel) {return plotter[iplot]->get_x(x_pixel);}
double PlotView::get_y(int iplot, int y_pixel) {return plotter[iplot]->get_y(y_pixel);}


void PlotView::mouseMovedInPlot(int iplot, const QMouseEvent& e) {
  Log<OdinComp> odinlog("PlotView","mouseMovedInPlot");
  double xpos=get_x(iplot,e.x());
  double ypos=get_y(iplot,e.y());
  STD_string msg("x="+ftos(xpos)+"\t  "+"y="+ftos(ypos));

  if(plotmode->get_current_item()==tcmode_curves) {

    int dist;
    closest=plotter[iplot]->closest_curve(e.x(),e.y(),dist);

    // check whether we have a valid plotting curve
    for(int i=0; i<numof_plotchan; i++) {
      if(closest==curveid_baseline[i]) closest=0;
    }
    if(dist<0 || dist>=CLOSEST_CURVE_MAX) closest=0;

    bool do_replot=true;
    if(left_button(&e,true)) do_replot=false;

    ODINLOG(odinlog,normalDebug) << "closest/dist=" << closest << "/" << dist << STD_endl;

    // remove highlighting from last curve
    bool different_plot= (lastplot_closest!=iplot);
    bool different_curve=(lastcurve_closest!=closest);
    if(lastcurve_closest>0 && (different_plot || different_curve) ) {
      ODINLOG(odinlog,normalDebug) << "removing highlighting from last curve #" << lastcurve_closest << STD_endl;
      plotter[lastplot_closest]->highlight_curve(lastcurve_closest,false);
      if(do_replot) plotter[lastplot_closest]->replot();
    }

    if(closest) {

      msg+="\t  ";
      const char* curve_label=curves_map[iplot][closest].label;
      if(curve_label) msg+=curve_label;

      if(curves_map[iplot][closest].has_freq_phase) {
        msg+="\t  ";
        msg+="freq="+ftos(curves_map[iplot][closest].freq)+ODIN_FREQ_UNIT;
        msg+="\t  ";
        msg+="phase="+ftos(curves_map[iplot][closest].phase)+ODIN_ANGLE_UNIT;
      }

      if(curves_map[iplot][closest].gradmatrix) {
        msg+="\t  ";
        msg+="gradmatrix="+curves_map[iplot][closest].gradmatrix->print();
      }

      // highlight closest curve
      if(closest!=lastcurve_closest) {
        ODINLOG(odinlog,normalDebug) << "highlighting closest curve #" << closest << STD_endl;
         plotter[iplot]->highlight_curve(closest,true);
         if(do_replot) plotter[iplot]->replot();
      }
    }

    lastplot_closest=iplot;
    lastcurve_closest=closest;
  }

  setMessage(msg.c_str());
}


#define IMPOSSIBLE_PIXELVAL -100000

void PlotView::mousePressedInPlot (int iplot, const QMouseEvent& e) {
  Log<OdinComp> odinlog("PlotView","mousePressedInPlot");
  markid_vertzoom=0;
  markid_hortzoom=0;

  // for some reason, mouse release event is issued twice on MacOS/Qt4/qwt5 so we will process only matched mouse press-release events
  x_pressed=IMPOSSIBLE_PIXELVAL;
  y_pressed=IMPOSSIBLE_PIXELVAL;

  ODINLOG(odinlog,normalDebug) << "iplot=" << iplot << STD_endl;


  if(left_button(&e,false)) {
    ODINLOG(odinlog,normalDebug) << "left_button" << STD_endl;
//    plotter[iplot]->enable_outline(true);
    plot_pressed=iplot;
    x_pressed=e.x();
    y_pressed=e.y();

    if(zoomflag[horizontal]->is_on()) {
      markid_hortzoom=plotter[iplot]->insert_marker("",get_x(iplot,x_pressed),true,false);
      plotter[iplot]->replot();
    }

    if(zoomflag[vertical]->is_on()) {
      markid_vertzoom=plotter[iplot]->insert_marker("",get_y(iplot,y_pressed),true,true);
      plotter[iplot]->replot();
    }

    ODINLOG(odinlog,normalDebug) << "markid_hortzoom/markid_vertzoom=" << markid_hortzoom << "/" << markid_vertzoom << STD_endl;
  }

  if(right_button(&e,false)) {
    ODINLOG(odinlog,normalDebug) << "right_button" << STD_endl;
//    plotter[iplot]->enable_outline(false);

    GuiPopupMenu pm(this);


    pm.insert_item("Display All", this, SLOT(plot_all()));

    pm.insert_item("Autoscale x-Axis", this, SLOT(autoscale_x()));

    if(iplot==0) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot0()));
    if(iplot==1) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot1()));
    if(iplot==2) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot2()));
    if(iplot==3) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot3()));
    if(iplot==4) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot4()));
    if(iplot==5) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot5()));
    if(iplot==6) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot6()));
    if(iplot==7) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot7()));
    if(iplot==8) pm.insert_item("Autoscale y-Axis", this, SLOT(autoscalePlot8()));

    pm.insert_item("Refresh Plot", this, SLOT(replot()));

    if(closest) {

      iplot_cache=iplot;
      if(plotmode->get_current_item()==tcmode_curves) {
        pm.insert_item(("Save Curve \'"+STD_string(curves_map[iplot_cache][closest].label)+"\'").c_str(), this, SLOT(save_closest_curve_data()));
      } else {
        STD_string chanlabel, chanunit;
        get_plot_label(iplot, chanlabel, chanunit);
        pm.insert_item(("Save Channel \'"+chanlabel+"\'").c_str(), this, SLOT(save_current_channel_data()));
      }

    }

    ODINLOG(odinlog,normalDebug) << "e.pos=" << e.pos().x() << "/" << e.pos().y() << STD_endl;
    QPoint pos=plotter[iplot]->get_widget()->mapToGlobal(e.pos());
    ODINLOG(odinlog,normalDebug) << "pos=" << pos.x() << "/" << pos.y() << STD_endl;
    pm.popup(pos);

  }
}

void PlotView::mouseReleasedInPlot(int iplot, const QMouseEvent& e) {
  Log<OdinComp> odinlog("PlotView","mouseReleasedInPlot");

  if(left_button(&e,false) && iplot==plot_pressed) {
    if(x_pressed!=IMPOSSIBLE_PIXELVAL && y_pressed!=IMPOSSIBLE_PIXELVAL) {

      int x_released=e.x();
      int y_released=e.y();

      int low_x=x_pressed;
      if(x_released<low_x) low_x=x_released;
      int low_y=y_pressed;
      if(y_released<low_y) low_y=y_released;
      int upp_x=x_pressed;
      if(x_released>upp_x) upp_x=x_released;
      int upp_y=y_pressed;
      if(y_released>upp_y) upp_y=y_released;

      ODINLOG(odinlog,normalDebug) << "low_x/low_y/upp_x/upp_y(" << iplot << ")=" << low_x << "/" << low_y << "/" << upp_x << "/" << upp_y << STD_endl;

      double y_axis_low=get_y(iplot,upp_y);
      double y_axis_upp=get_y(iplot,low_y);
      double x_axis_low=get_x(iplot,low_x);
      double x_axis_upp=get_x(iplot,upp_x);

      if(x_axis_low<x_axis_upp && y_axis_low<y_axis_upp) {
        ODINLOG(odinlog,normalDebug) << "x_axis_low/x_axis_upp=" << x_axis_low << "/" << x_axis_upp << STD_endl;
        ODINLOG(odinlog,normalDebug) << "y_axis_low/y_axis_upp=" << y_axis_low << "/" << y_axis_upp << STD_endl;

        bool rescale_y=false;
        bool rescale_x=false;
        if(zoomflag[rect]->is_on())       {rescale_y=true; rescale_x=true;}
        if(zoomflag[horizontal]->is_on()) {rescale_x=true;}
        if(zoomflag[vertical]->is_on())   {rescale_y=true;}

        if(rescale_y) plotter[iplot]->set_y_axis_scale(y_axis_low,y_axis_upp);
        if(rescale_x) set_range_and_update_x_axes(x_axis_low,x_axis_upp);
      }
    }

    if(markid_vertzoom) plotter[iplot]->remove_marker(markid_vertzoom);
    if(markid_hortzoom) plotter[iplot]->remove_marker(markid_hortzoom);
    replot();
  }

  // reset
  x_pressed=IMPOSSIBLE_PIXELVAL;
  y_pressed=IMPOSSIBLE_PIXELVAL;

  markid_vertzoom=0;
  markid_hortzoom=0;

}




void PlotView::update_x_axes() {
  Log<OdinComp> odinlog("PlotView","update_x_axes");
  int iplot;

  if(plotmode->get_current_item()==tcmode_curves) {
    create_plotcurves_and_markers();
  } else {
    timecourseMode type=timecourseMode(plotmode->get_current_item());
    plot_timecourses(type);
  }

  for(iplot=0; iplot<numof_plotchan; iplot++) {
    plotter[iplot]->set_x_axis_scale(x_range->get_min(),x_range->get_max());
  }

  replot();
}


void PlotView::get_plot_label(int iplot, STD_string& label, STD_string& unit) const {

  STD_string gradunit(timecourseUnit[plotmode->get_current_item()]);
  STD_string gradprefix(timecoursePrefix[plotmode->get_current_item()]);

  if(iplot==B1re_plotchan)   {label="B1re";   unit=ODIN_FIELD_UNIT;}
  if(iplot==B1im_plotchan)   {label="B1im";   unit=ODIN_FIELD_UNIT;}
  if(iplot==rec_plotchan)    {label="Rec";    unit="";}
  if(iplot==signal_plotchan) {label="Signal"; unit="";}
  if(iplot==freq_plotchan)   {label="Freq";   unit=ODIN_FREQ_UNIT;}
  if(iplot==phase_plotchan)  {label="Phase";  unit=ODIN_ANGLE_UNIT;}
  if(iplot==Gread_plotchan)  {label=gradprefix+"read"; unit=gradunit;}
  if(iplot==Gphase_plotchan) {label=gradprefix+"phase"; unit=gradunit;}
  if(iplot==Gslice_plotchan) {label=gradprefix+"slice"; unit=gradunit;}
}


void PlotView::set_plot_labels(bool extra_space) {
  if(plotAxes->is_on()) {

    STD_string cbtext;
    STD_string plotunit;

    for(int iplot=0; iplot<numof_plotchan; iplot++) {

      get_plot_label(iplot, cbtext, plotunit);

      plotflag[iplot]->set_label(cbtext.c_str());

      STD_string axtitle(cbtext);
      if(plotunit!="")axtitle+=" ["+plotunit+"]";
      if(extra_space) axtitle+="\n\n\n\n";
      plotter[iplot]->set_y_axis_label(axtitle.c_str());
    }
  }
}


void PlotView::set_curve_pens(bool thick_lines) {
  int cpen_width=1;
  int bpen_width=1;

  if(thick_lines) {
    cpen_width=3;
    bpen_width=2;
  }

  for(unsigned iplot=0; iplot<numof_plotchan; iplot++) {
    if(plotmode->get_current_item()==tcmode_curves) {
      for(STD_map<long,Curve4Qwt>::const_iterator it=curves_map[iplot].begin(); it!=curves_map[iplot].end(); ++it) {
        plotter[iplot]->set_curve_pen(it->first,_ARRAY_FOREGROUND_COLOR1_,cpen_width);
      }
      if(curveid_baseline[iplot]) plotter[iplot]->set_curve_pen(curveid_baseline[iplot],_ARRAY_FOREGROUND_COLOR2_,bpen_width);
    } else {
      for(unsigned itc=0; itc<numof_tcmodes; itc++) {
        if(timecourse_curve_id[itc][iplot]) plotter[iplot]->set_curve_pen(timecourse_curve_id[itc][iplot],_ARRAY_FOREGROUND_COLOR1_,cpen_width);
      }
    }
  }
}


void PlotView::hide_and_show() {
  int lastplot=-1;

  for(int iplot=0; iplot<numof_plotchan; iplot++) {
    if(plotflag[iplot]->is_on()) {
      plotter[iplot]->get_widget()->show();
      wheel[iplot]->get_widget()->show();
//      autoscalebutton[iplot]->get_widget()->show();
      lastplot=iplot;
      grid->set_row_stretch(iplot,100);
    } else {
      plotter[iplot]->get_widget()->hide();
      wheel[iplot]->get_widget()->hide();
//      autoscalebutton[iplot]->get_widget()->hide();
      grid->set_row_stretch(iplot,0);
    }
    plotter[iplot]->set_x_axis_label("",true);
  }

  // add axis label Time to last plotter
  if(lastplot>=0) {
    STD_string axtitle(STD_string("Time [")+ODIN_TIME_UNIT+"]");
    plotter[lastplot]->set_x_axis_label(axtitle.c_str());
  }
  if(lastplot_old!=lastplot) {
    if(lastplot_old>=0) plotter[lastplot_old]->replot();
    if(lastplot>=0)     plotter[lastplot]->replot();
  }

  lastplot_old=lastplot;
}


void PlotView::set_zoom_tool(zoomMode mode) {
  for(int imode=0; imode<numof_zoomModes; imode++) {
    if(mode==imode) zoomflag[imode]->set_on(true);
    else zoomflag[imode]->set_on(false);
  }
  
  int iplot;

  if(zoomflag[rect]->is_on())       for(iplot=0; iplot<numof_plotchan; iplot++) plotter[iplot]->set_rect_outline_style();
  if(zoomflag[horizontal]->is_on()) for(iplot=0; iplot<numof_plotchan; iplot++) plotter[iplot]->set_line_outline_style(false);
  if(zoomflag[vertical]->is_on())   for(iplot=0; iplot<numof_plotchan; iplot++) plotter[iplot]->set_line_outline_style(true);
}



void PlotView::replot() {
  Log<OdinComp> odinlog("PlotView","replot");
  for(int iplot=0; iplot<numof_plotchan; iplot++) {
    /*if(plotflag[iplot]->isOn())*/ plotter[iplot]->replot();
  }
}

const SeqTimecourseData* PlotView::get_timecourses()  {
  if(!create_timecourses(tcmode_plain)) return 0;
  return plotdata->get_timecourse(tcmode_plain);
}

const SeqTimecourseData* PlotView::get_kspace_trajs() {
  if(!create_timecourses(tcmode_kspace)) return 0;
  return plotdata->get_timecourse(tcmode_kspace);
}

bool PlotView::simulate(const STD_string& fidfile, const STD_string& samplefile, ProgressMeter* progmeter) {

  SeqSimFeedbackAbstract* feedback=0;
  VtkMagnPlotter magplot; // put it on stack so it gets deleted automatically on Cancel
  if(plotdata->monitor_simulation()) {
    active_magplot=&magplot;
    magplot.start();
    feedback=this;
    ipage=-1; // trigger page zoom on first call of plot_vector
  } else active_magplot=0;

  if(!plotdata->simulate(fidfile,samplefile,progmeter,feedback)) return false;

  plotflag[signal_plotchan]->set_on(true); // make signal plotter visible
  create_plotcurves_and_markers();  // update to show simulated signal
  autoscale_y(signal_plotchan);
  hide_and_show();
//  plotter[signal_plotchan]->replot();
  return true;
}

void PlotView::plot_vector(double timepoint, float M[3], float* dM) {
  int iplot;

  if(active_magplot) {
    active_magplot->plot_vector(M,dM);

    bool change_page=false;
    while( ipage < int(timepoint/MAGN_PLOT_PAGE_DURATION) ) {
      ipage++;
      change_page=true;
    }
    if(change_page) {
      set_range_and_update_x_axes(ipage*MAGN_PLOT_PAGE_DURATION,(ipage+1)*MAGN_PLOT_PAGE_DURATION);
      for(iplot=0; iplot<numof_plotchan; iplot++) {

        markid_magplot=plotter[iplot]->insert_marker("Simulation",timepoint,false,false,true);
      }
    }
    for(iplot=0; iplot<numof_plotchan; iplot++) {
      plotter[iplot]->set_marker_pos(markid_magplot,timepoint);
    }
    replot();

  }

}

void PlotView::settingsChanged() {
  for(int iplot=0; iplot<numof_plotchan; iplot++) {
    plotter[iplot]->enable_axes(plotAxes->is_on());
    plotter[iplot]->enable_grid(plotGrid->is_on());
  }
  create_markers();
  replot();
}


void PlotView::save_closest_curve_data() {
  if(!closest) return;

  STD_string fname=get_save_filename("Save Curve as ASCII", "", "", this);
  if(fname=="") return;

  STD_ofstream file(fname.c_str());

  const Curve4Qwt& closcurve=curves_map[iplot_cache][closest];
  if(!closcurve.size) return;

  double x0=closcurve.x[0]; // substract base time point to avoid round-off errors
  for(int i=0; i<closcurve.size; i++) {
    file << (closcurve.x[i]-x0) << "\t" << closcurve.y[i] << "\n";
  }

  file.close();
}


void PlotView::save_current_channel_data() {

  timecourseMode type=timecourseMode(plotmode->get_current_item());
  if(!create_timecourses(type)) return;

  const SeqTimecourseData* timecourse=plotdata->get_timecourse(type);
  if(!timecourse) return;


  STD_string fname=get_save_filename("Save channel as ASCII", "", "", this);
  if(fname=="") return;

  STD_ofstream file(fname.c_str());

  for(unsigned int i=0; i<timecourse->size; i++) {
    file << timecourse->x[i] << "\t" << timecourse->y[iplot_cache][i] << "\n";
  }

  file.close();
}


void PlotView::osci() {
  Log<OdinComp> odinlog("PlotView","osci");

  LDRblock oscisettings("Settings for Oscilloscope");
  oscisettings.append(TriggerType);
  oscisettings.append(TriggerWidth);
  oscisettings.append(RefreshRate);
  new LDRwidgetDialog(oscisettings,1,this,true);

  markType marktype=markType(int(TriggerType));

  timecourseMode tc_mode=timecourseMode(plotmode->get_current_item());

  dvector timepoints;

  if(tc_mode==tcmode_curves) {
    STD_list<Marker4Qwt>::const_iterator markers_begin;
    STD_list<Marker4Qwt>::const_iterator markers_end;
    plotdata->get_markers(markers_begin,markers_end,0.0,totaldur);
    for(STD_list<Marker4Qwt>::const_iterator it=markers_begin; it!=markers_end; ++it) {
      if(it->type==marktype) timepoints.push_back(it->x);
    }
  } else {
    STD_list<TimecourseMarker4Qwt>::const_iterator tc_markers_begin;
    STD_list<TimecourseMarker4Qwt>::const_iterator tc_markers_end;
    plotdata->get_timecourse_markers(tc_mode,tc_markers_begin,tc_markers_end,0.0,totaldur);
    for(STD_list<TimecourseMarker4Qwt>::const_iterator it=tc_markers_begin; it!=tc_markers_end; ++it) {
      if(it->type==marktype) timepoints.push_back(it->x);
    }
  }
  ODINLOG(odinlog,normalDebug) << "timepoints=" << timepoints.printbody() << STD_endl;

  if(timepoints.size()) {

    ProgressDisplayDialog display(this);
    ProgressMeter progmeter(display);
    progmeter.new_task(timepoints.size(),"Oscilloscope");

    // cache min/max
    double minx=x_range->get_min();
    double maxx=x_range->get_max();

    try {
      for(unsigned int i=0; i<timepoints.size(); i++) {
        ODINLOG(odinlog,normalDebug) << "timepoints[" << i << "]=" << timepoints[i] << STD_endl;
        minx=timepoints[i];
        maxx=timepoints[i]+TriggerWidth;
        set_range_and_update_x_axes(minx, maxx, true); // ignore scrollbar min/max values since they only allow values on an equidistant grid
        progmeter.increase_counter();
        sleep_ms(int(1000.0*RefreshRate));
      }
    } catch(OdinCancel) {
      ODINLOG(odinlog,normalDebug) << " aborted" << STD_endl;
    }
    display.finish();
    set_range_and_update_x_axes(minx,maxx); // bring scrollbar into sync with current range
  } else {
    message_question(justificate("No markers of type \'"+STD_string(markLabel[marktype])+"\' available to trigger oscilloscope").c_str(), "Markers Missing", this, false, false);
  }
}


void PlotView::print() {
  Log<OdinComp> odinlog("PlotView","print");

  int iplot;
  int nplots=0;
  for(iplot=0; iplot<numof_plotchan; iplot++) if(plotflag[iplot]->is_on()) nplots++;
  if(!nplots) return;

  ODINLOG(odinlog,normalDebug) << "nplots=" << nplots << STD_endl;

  if(!printer->setup(this)) return;
  QPainter painter(printer->get_device());

  int plotheight=paintdevice_height(printer->get_device())/nplots;
  int plotwidth =paintdevice_width(printer->get_device());
  ODINLOG(odinlog,normalDebug) << "plotheight/plotwidth=" << plotheight << "/" << plotwidth << STD_endl;


  int hmargin=int(float(plotwidth)/20.0);
  plotwidth-=2*hmargin;
  int vmargin=-int(float(plotheight)/20.0);
  plotheight-=2*vmargin;
  ODINLOG(odinlog,normalDebug) << "hmargin/vmargin=" << hmargin << "/" << vmargin << STD_endl;

  // prepare printing
  set_plot_labels(true);
  set_curve_pens(true);
  
  int plotpos=0;
  for(iplot=0; iplot<numof_plotchan; iplot++) {

    if(plotflag[iplot]->is_on()) {

      int left=hmargin;
      int top=plotpos*(2*vmargin+plotheight)+vmargin;
      int width=plotwidth;
      int height=plotheight;
      QRect rect( left, top, width, height);

      ODINLOG(odinlog,normalDebug) << "left/top/width/height[" << plotpos << "]=" << left << "/" << top << "/" << width << "/" << height << STD_endl;

      plotter[iplot]->print(&painter,rect);
      plotpos++;
    }
  }

  // reset after printing
  set_plot_labels(false);
  set_curve_pens(false);
}


void PlotView::save() {
  int iplot;
  if(plotmode->get_current_item()==tcmode_curves) return;

  timecourseMode type=timecourseMode(plotmode->get_current_item());
  if(!create_timecourses(type)) return;

  const SeqTimecourseData* timecourse=plotdata->get_timecourse(type);
  if(!timecourse) return;

  STD_string fname=get_save_filename("Save plot as ASCII", "", "", this);
  if(fname=="") return;


  bool doplot[numof_plotchan];
  for(iplot=0; iplot<numof_plotchan; iplot++) {
    if(plotflag[iplot]->is_on()) doplot[iplot]=true;
    else doplot[iplot]=false;
  }

  STD_ofstream file(fname.c_str());

  for(unsigned int i=0; i<timecourse->size; i++) {
    file << timecourse->x[i] << "\t";
    for(iplot=0; iplot<numof_plotchan; iplot++) {
      if(doplot[iplot]) file << timecourse->y[iplot][i] << "\t";
    }
    file << "\n";
  }

  file.close();
}


int PlotView::lastplot_old=-1;

///////////////////////////////////////////////////////////

PlotWindow::PlotWindow(const char* method, const STD_string& nucleus, QWidget *parent)
  : GuiMainWindow(parent) {
  Log<OdinComp> odinlog("PlotWindow","PlotWindow");

  GuiMainWindow::set_caption(("Sequence Plot of "+STD_string(method)).c_str());



  view=new PlotView(method, nucleus,this);
  connect(view, SIGNAL(setMessage(const char*)),this, SLOT  (statusBarMessage(const char*)));
  connect(view, SIGNAL(closeMe()),              this, SLOT  (close()));

  GuiMainWindow::show(view,true);

}

PlotWindow::~PlotWindow() {
  Log<OdinComp> odinlog("PlotWindow","~PlotWindow");
  delete view;
}

void PlotWindow::statusBarMessage(const char* text) {
  GuiMainWindow::set_status_message(text);
}

void PlotWindow::close() {
  GuiMainWindow::close();
}
