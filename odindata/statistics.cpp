#include "statistics.h"

#include <tjutils/tjtest.h>

STD_ostream& operator << (STD_ostream& s, statisticResult stats) {
  s << stats.mean << " +/- " << stats.meandev << " (min=" << stats.min << ", max=" << stats.max << ", stdev=" << stats.stdev << ")";
  return s;
}

/////////////////////////////////////////////////////

// instantiate one special template for checking at compile time
#ifdef ODIN_DEBUG
template statisticResult statistics(const Array<float,1>&, const Array<float,1>*);
template float median(const Array<float,1>&,const Array<float,1>*);
template double weightmean(const Array<double,1>& , const Array<float,1>&);
#endif


/////////////////////////////////////////////////////


// unit test


#ifndef NO_UNIT_TEST
class StatisticsTest : public UnitTest {

 public:
  StatisticsTest() : UnitTest("statistics") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    int testsize=10;

    Data<float,2> testarray(testsize,testsize);
    TinyVector<int, 2> index;
    for(unsigned int i=0; i<testarray.numElements(); i++) {
      index=testarray.create_index(i);
      testarray(index)=float(index(0))+float(index(1));
    }
    statisticResult statres=statistics(testarray);

    if(statres.mean!=9.0) {
      ODINLOG(odinlog,errorLog) << "statres.mean=" << statres.mean << STD_endl;
      return false;
    }

    if(statres.meandev>0.5 || statres.meandev<0.4) {
      ODINLOG(odinlog,errorLog) << "statres.meandev=" << statres.meandev << STD_endl;
      return false;
    }

    Data<float,1> testensemble(7);

    testensemble(0)=10;

    testensemble(1)=1;
    testensemble(2)=5;
    testensemble(3)=6;

    testensemble(4)=12;
    testensemble(5)=16;
    testensemble(6)=1000;

    float calcmedian=median(testensemble);
    float expectmedian=10.0;

    if(calcmedian!=expectmedian) {
      ODINLOG(odinlog,errorLog) << "calcmedian/expectmedian=" << calcmedian << "/" << expectmedian << STD_endl;
      return false;
    }


    testensemble.resize(6);
    testensemble(0)=5;
    testensemble(1)=12;
    testensemble(2)=16;
    testensemble(3)=1;
    testensemble(4)=9;
    testensemble(5)=7;


    calcmedian=median(testensemble);
    expectmedian=8.0;

    if(calcmedian!=expectmedian) {
      ODINLOG(odinlog,errorLog) << "calcmedian/expectmedian=" << calcmedian << "/" << expectmedian << STD_endl;
      return false;
    }



    return true;
  }

};

void alloc_StatisticsTest() {new StatisticsTest();} // create test instance
#endif
