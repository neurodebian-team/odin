#include "correlation.h"


fmriResult fmri_eval(const Data<float,1>& timecourse, const Data<float,1>& designvec) {
  Log<OdinData> odinlog("","fmri_eval");
  fmriResult result;

  if( !same_shape(timecourse, designvec) ) {
    ODINLOG(odinlog,errorLog) << "design file size mismatch" << STD_endl;
    return result;
  }

  int nrep=timecourse.numElements();

  float maxdesign=max(designvec);
  float mindesign=min(designvec);

  // Calculate baseline
  int nbaseline=0;
  for(int i=0; i<nrep; i++) {
    if(designvec(i)) break;
    nbaseline++;
  }
  if(nbaseline) result.Sbaseline=mean(timecourse(Range(0,nbaseline-1)));

  // calculate fMRI contrast
  int nrest=0;
  int nstim=0;
  for(int i=0; i<nrep; i++) {
    if(designvec(i)==mindesign) nrest++;
    if(designvec(i)==maxdesign)  nstim++;
  }

  Array<float,1> restdata(nrest);
  Array<float,1> stimdata(nstim);

  int irest=0;
  int istim=0;

  for(int i=0; i<nrep; i++) {
    if(designvec(i)==mindesign) {restdata(irest)=timecourse(i); irest++;}
    if(designvec(i)==maxdesign)  {stimdata(istim)=timecourse(i); istim++;}
  }


  statisticResult reststat=statistics(restdata);
  statisticResult stimstat=statistics(stimdata);


  result.Srest=reststat.mean;
  result.Sstim=stimstat.mean;

  result.rel_diff=secureDivision(stimstat.mean-reststat.mean,reststat.mean);
  result.rel_err=secureDivision(stimstat.meandev+reststat.meandev,reststat.mean);

  return result;
}

//////////////////////////////////////////////////////////////////////////////////


// instantiate one special template for checking at compile time
#ifdef ODIN_DEBUG
template correlationResult correlation(const Array<float,1>&, const Array<float,1>&);
#endif


