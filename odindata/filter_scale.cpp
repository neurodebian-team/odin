#include "filter_scale.h"

void FilterScale::init(){
  slope=1.0;
  slope.set_description("Slope");
  append_arg(slope,"slope");

  offset=0.0;
  offset.set_description("Offset");
  append_arg(offset,"offset");
}


bool FilterScale::process(Data<float,4>& data, Protocol& prot) const {
  data=slope*data+offset;
  return true;
}
