#include "filter_merge.h"


bool FilterMerge::process(FileIO::ProtocolDataMap& pdmap) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  STD_map<double,FileIO::ProtocolDataMap> acqstartmap; // Create a temporary protocol-data pair for each AcquisitionStart
  TinyVector<int,4> shape=0;
  for(FileIO::ProtocolDataMap::const_iterator it=pdmap.begin();it!=pdmap.end();it++) {
    const Protocol& prot=it->first;
    const Data<float,4>& data=it->second;
    shape(timeDim)+=data.extent(timeDim);
    for(int idim=sliceDim; idim<n_dataDim; idim++) shape(idim)=STD_max(shape(idim),data.extent(idim));
    acqstartmap[prot.seqpars.get_AcquisitionStart()][prot].reference(data);
  }
  ODINLOG(odinlog,normalDebug) << "shape" << shape << STD_endl;
  pdmap.clear();


  Data<float,4> result(shape);
  int repoffset=0;
  Protocol prot;
  bool prot_set=false;
  for(STD_map<double,FileIO::ProtocolDataMap>::const_iterator acqit=acqstartmap.begin();acqit!=acqstartmap.end();acqit++) {
    ODINLOG(odinlog,normalDebug) << "acqstart=" << acqit->first << STD_endl;
    const FileIO::ProtocolDataMap& acqpdmap=acqit->second;
    for(FileIO::ProtocolDataMap::const_iterator it=acqpdmap.begin();it!=acqpdmap.end();it++) {
      if(!prot_set) {prot=it->first; prot_set=true;} // Use first protocol for dataset
      Data<float,4> oneset(it->second);
      TinyVector<int,4> newshape(shape);
      int nrep=oneset.extent(timeDim);
      newshape(timeDim)=nrep;
      oneset.congrid(newshape);
      ODINLOG(odinlog,normalDebug) << "newshape" << newshape << STD_endl;
      result(Range(repoffset,repoffset+nrep-1),all,all,all)=oneset;
      repoffset+=nrep;
    }
  }

  pdmap[prot].reference(result);

  return true;
}
