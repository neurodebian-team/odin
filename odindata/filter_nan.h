//
// C++ Interface: filter_nan
//
// Description: 
//
//
// Author:  <Enrico Reimer>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FILTER_NAN_H
#define FILTER_NAN_H

#include <odindata/filter_step.h>

/**
	@author Enrico Reimer
*/
class FilterNaN : public FilterStep
{
  LDRfloat replace;

  STD_string label() const {return "noNaN";}
  STD_string description() const {return "Replaces every NaN by the given value";}
  bool process(Data<float,4>& data, Protocol& prot)const;
  FilterStep*  allocate() const {return new FilterNaN();}
  void init();
};

#endif
