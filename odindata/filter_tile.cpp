#include "filter_tile.h"

void FilterTile::init(){
  cols.set_description("columns");
  append_arg(cols,"cols");
}


bool FilterTile::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  int nrep=data.extent(0);
  int nslice=data.extent(1);
  int nphase=data.extent(2);
  int nread=data.extent(3);

  int tilecols=cols;
  tilecols=STD_max(tilecols,1);
  tilecols=STD_min(tilecols,nslice);


  int rows=nslice/tilecols;
  if(nslice%tilecols) rows++;

  ODINLOG(odinlog,normalDebug) << "nslice/cols/rows=" << nslice << "/" << tilecols << "/" << rows << STD_endl;

  int newnphase=rows*nphase;
  int newnread=tilecols*nread;

  Data<float,4> outdata(nrep,1,newnphase,newnread); outdata=0.0;

  int icol=0;
  int irow=0;
  for(int islice=0; islice<nslice; islice++) {

    int rowoffset=irow*nphase;
    int coloffset=icol*nread;
    outdata(all, 0, Range(rowoffset, rowoffset+nphase-1), Range(coloffset, coloffset+nread-1))=data(all,islice,all,all);

    icol++;
    if(icol>=tilecols) {
      icol=0;
      irow++;
    }
  }

  data.reference(outdata);

  // adjust protocol
  prot.geometry.set_nSlices(1);
  prot.seqpars.set_MatrixSize(phaseDirection,newnphase);
  prot.seqpars.set_MatrixSize(readDirection,newnread);


  return true;
}
