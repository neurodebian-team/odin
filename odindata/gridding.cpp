#include "gridding.h"

#include <tjutils/tjtest.h>


// instantiate one special template for checking at compile time
#ifdef ODIN_DEBUG
template class Gridding<float,1>;
template class CoordTransformation<STD_complex,2>;
#endif


/////////////////////////////////////////////////////

// unit test

#ifndef NO_UNIT_TEST

#include "statistics.h"

class GriddingTest : public UnitTest {

 public:
  GriddingTest() : UnitTest("Gridding") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // Test using a 3*FOV/4 (filled with 1) and FOV/2 (filled with 2) box
    int testsize=128;
    Data<float,2> testbox(testsize,testsize); testbox=0.0;
    Range inner(testsize/8, 7*testsize/8-1);
    testbox(inner,inner)=1.0;
    inner=Range(testsize/4, 3*testsize/4-1);
    testbox(inner,inner)=2.0;
//    testbox.autowrite("testbox.jdx");

    float radialfactor=1.2; // so that sampling disk covers the whole box
    int ncycles=int(radialfactor*testsize/2+0.5); // suffucient sampling in radial direction
    int spirsize=int(radialfactor*int(2.0*PII*ncycles*testsize)+0.5); // suffucient along spiral

    STD_vector<GriddingPoint<2> > src_coords(spirsize);
    Data<float,1> src(spirsize); src=0.0;
    for(int i=0; i<spirsize; i++) {
      float s=radialfactor*float(i)/float(spirsize);
      float phase=2.0*PII*ncycles*s;
      float x=s*cos(phase);
      float y=s*sin(phase);
      src_coords[i].coord(0)=x;
      src_coords[i].coord(1)=y;
      if(fabs(x)<0.75 && fabs(y)<0.75) src(i)=1.0;
      if(fabs(x)<0.5 && fabs(y)<0.5) src(i)=2.0;
    }
//    src.autowrite("src.jdx");

    Data<float,2> dst(testbox.shape());

    LDRfilter gridkernel;
    gridkernel.set_function("Gauss");

    Gridding<float,2> gridder;
    gridder.init(dst.shape(), 2.0, src_coords, gridkernel, sqrt(2.0)*2.0/float(testsize));

    Data<float,2> griddedbox(gridder(src));
//    griddedbox.autowrite("griddedbox.jdx");

    Data<float,2> diff(testbox-griddedbox);
//    diff.autowrite("diff.jdx");
    float absdiff=sum(fabs(diff));
    if(absdiff>30.0) {
      ODINLOG(odinlog,errorLog) << "absdiff=" << absdiff << STD_endl;
      return false;
    }


    return true;
  }

};

void alloc_GriddingTest() {new GriddingTest();} // create test instance
#endif
