#include "fileio.h"
#include "image.h"

/////////////////////////////////////////////////////////////

void resize4dim(farray& fdata) {
  if(fdata.dim()==4) return; // leave untouched if it is already 4dim
  fdata.autosize();
  ndim nn=fdata.get_extent();
  bool add_first_dim=true;
  if(nn.dim()==1) add_first_dim=false;
  while(nn.dim()<4) nn.add_dim(1,add_first_dim);
  while(nn.dim()>4) --nn;
  fdata.redim(nn);
}


/////////////////////////////////////////////////////////////


struct JdxFormat : public FileFormat {
  STD_string description() const {return "JCAMP-DX data sets";}
  svector suffix() const  {
    svector result; result.resize(2);
    result[0]="smp";
    result[1]="coi";
    return result;
  }
  svector dialects() const {return svector();}
  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("JdxFormat","read");

    STD_string arrlabel=opts.ldr;

    if(arrlabel=="") {
      if(LDRfileName(filename).get_suffix()=="smp") arrlabel="spinDensity";
      else {
        ODINLOG(odinlog,errorLog) << "No array label provided, use the option '-" << opts.ldr.get_cmdline_option() <<"' to specify one" << STD_endl;
        return -1;
      }
    }

    LDRblock block;
    farray fdata;

    bool valid_type=false;

    if(!valid_type) {
      LDRdoubleArr dpar;
      dpar.set_label(arrlabel);
      block.clear(); block.append(dpar);
      if(block.load(filename,serializer)>0) {
        ODINLOG(odinlog,normalDebug) << "LDRdoubleArr detected" << STD_endl;
        fdata.redim(dpar.get_extent());
        for(unsigned int i=0; i<dpar.length(); i++) fdata[i]=dpar[i];
        valid_type=true;
      }
    }

    if(!valid_type) {
      LDRfloatArr fpar;
      fpar.set_label(arrlabel);
      block.clear(); block.append(fpar);
      if(block.load(filename,serializer)>0) {
        ODINLOG(odinlog,normalDebug) << "LDRfloatArr detected" << STD_endl;
        fdata.redim(fpar.get_extent());
        for(unsigned int i=0; i<fpar.length(); i++) fdata[i]=fpar[i];
        valid_type=true;
      }
    }

    if(!valid_type) {
      LDRcomplexArr cpar;
      cpar.set_label(arrlabel);
      block.clear(); block.append(cpar);
      if(block.load(filename,serializer)>0) {
        ODINLOG(odinlog,normalDebug) << "LDRcomplexArr detected" << STD_endl;
        ndim nn=cpar.get_extent();
        nn[0]*=2;
        fvector amp=amplitude(cpar);
        fvector pha=phase(cpar);
        ODINLOG(odinlog,normalDebug) << "nn=" << STD_string(nn) << STD_endl;
        fdata.redim(nn);
        unsigned int n=cpar.length();
        for(unsigned int i=0; i<n; i++) {
          fdata[i]=amp[i];
          fdata[n+i]=pha[i];
        }
        valid_type=true;
      }
    }

    if(!valid_type) {
      ODINLOG(odinlog,errorLog) << "Array parameter " << arrlabel << " not found"  << STD_endl;
      return -1;
    }

    resize4dim(fdata);
    data=fdata;
    return data.extent(0)*data.extent(1);
  }

  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts&, const Protocol& prot) {
    Log<FileIO> odinlog("JdxFormat","write");
    ODINLOG(odinlog,errorLog) << "Not implemented"  << STD_endl;
    return -1;
  }

 private:
  LDRserJDX serializer;

};

/////////////////////////////////////////////////////////////

template<class Serializer>
struct ImageFormat : public FileFormat {
  STD_string description() const {return "ODIN Image based on "+serializer.get_description();}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]=serializer.get_default_file_prefix();
    return result;
  }
  svector dialects() const {return svector();}

  int read(FileIO::ProtocolDataMap& pdmap, const STD_string& filename, const FileReadOpts& opts, const Protocol& protocol_template) {
    Log<FileIO> odinlog("ImageFormat","read");
    int result=0;

    ImageSet imgset;
    if(imgset.load(filename,serializer)<0) return -1;

    int nsets=imgset.get_numof_images();
    if(nsets<=0) return -1;
    ODINLOG(odinlog,normalDebug) << "nsets=" << nsets << STD_endl;

    Protocol prot(protocol_template);

    for(int i=0; i<nsets; i++) {
      prot.geometry=imgset.get_image(i).get_geometry();
      prot.study.set_Series(imgset.get_image(i).get_label(), i); // Make protocols distinguishable

      Data<float,4>& data=pdmap[prot];
      farray fdata=imgset.get_image(i).get_magnitude();
      resize4dim(fdata);
      data=fdata;
      result+=data.extent(0)*data.extent(1);
    }
    return result;
  }


  int write(const FileIO::ProtocolDataMap& pdmap, const STD_string& filename, const FileWriteOpts& opts) {
    Log<FileIO> odinlog("ImageFormat","write");
    int result=0;
    ImageSet imgset(LDRfileName(filename).get_basename());
    for(FileIO::ProtocolDataMap::const_iterator pdit=pdmap.begin(); pdit!=pdmap.end(); ++pdit) {
      STD_string series;
      int number;
      pdit->first.study.get_Series(series, number);

      Image img(series); // use series description as image label
      img.set_geometry(pdit->first.geometry);
      img.set_magnitude(pdit->second);
      imgset.append_image(img);
      result+=pdit->second.extent(0)*pdit->second.extent(1);
    }
    if(imgset.write(filename,serializer)<0) return -1;
    return result;
  }

 private:
  Serializer serializer;

};

/////////////////////////////////////////////////////////////


template<class Serializer>
struct ProtFormat : public FileFormat {

  STD_string description() const {return "ODIN protocols based on "+serializer.get_description();}

  svector suffix() const  {
    svector result; result.resize(1);
    if(serializer.get_default_file_prefix()=="xml") result[0]="x";
    result[0]+="pro";
    return result;
  }

  svector dialects() const {return svector();}

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("ProtFormat","read");

    if(prot.load(filename,serializer)<0) return false;

    int nslices=prot.geometry.get_nSlices();
    if(prot.geometry.get_Mode()==voxel_3d) nslices=prot.seqpars.get_MatrixSize(sliceDirection);

    data.resize(1, nslices, prot.seqpars.get_MatrixSize(phaseDirection), prot.seqpars.get_MatrixSize(readDirection)); // only one repetition
    data=0.0;

    return data.extent(0)*data.extent(1);
  }

  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts&, const Protocol& prot) {
    Log<FileIO> odinlog("ProtFormat","write");
    return prot.write(filename,serializer);
  }

 private:
  Serializer serializer;

};



//////////////////////////////////////////////////////////////

void register_ser_format() {
 static JdxFormat jf;
 static ImageFormat<LDRserJDX> jdximf;
 static ImageFormat<LDRserXML> xmlimf;
 static ProtFormat<LDRserJDX> jdxpf;
 static ProtFormat<LDRserXML> xmlpf;
 jf.register_format();
 jdximf.register_format();
 xmlimf.register_format();
 jdxpf.register_format();
 xmlpf.register_format();
}

