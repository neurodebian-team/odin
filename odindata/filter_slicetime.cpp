#include "filter_slicetime.h"

void FilterSliceTime::init(){

  sliceorderstr.set_description("space-separated list of slice indices in order of acquisition");
  append_arg(sliceorderstr,"sliceorderstr");

}

bool FilterSliceTime::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  TinyVector<int,4> shape=data.shape();

  int nrep=shape(0);
  int nslices=shape(1);

  if(nrep<=1) return true;

  ivector sliceorder;
  if(sliceorderstr=="") {
    // try protocol
    LDRbase* soptr=prot.get_parameter("SliceOrder");
    if(soptr) {
      iarray* sovec=0;
      sovec=soptr->cast(sovec);
      if(sovec) {
        sliceorder=(*sovec);
      }
    }
  } else {
    svector toks=tokens(sliceorderstr);
    sliceorder.resize(toks.size());
    for(unsigned int i=0; i<toks.size(); i++) sliceorder[i]=atoi(toks[i].c_str());
  }

  ODINLOG(odinlog,normalDebug) << "sliceorderstr=" << sliceorderstr << STD_endl;
  ODINLOG(odinlog,normalDebug) << "sliceorder=" << sliceorder.printbody() << STD_endl;

  if(int(sliceorder.size())!=nslices) {
    ODINLOG(odinlog,errorLog) << "size mismatch: nslices/sliceorder.size()=" << nslices << "/" << sliceorder.printbody() << STD_endl;
    return false;
  }

  dvector sliceshift(nslices); sliceshift=0.0;
  for(int islice=0; islice<nslices; islice++) {
    sliceshift[sliceorder[islice]]=secureDivision(islice,nslices);
  }
  ODINLOG(odinlog,normalDebug) << "sliceshift=" << sliceshift.printbody() << STD_endl;


  Data<float,1> tcourse(nrep);
  TinyVector<float,1> subpixel_shift;

  for(int islice=0; islice<nslices; islice++) {
    for(int iphase=0; iphase<shape(2); iphase++) {
      for(int iread=0; iread<shape(3); iread++) {
        tcourse(all)=data(all,islice,iphase,iread);
        subpixel_shift(0)=sliceshift[islice];
        tcourse.congrid(tcourse.shape(), &subpixel_shift);
        data(all,islice,iphase,iread)=tcourse(all);
      }
    }
  }

  return true;
}
