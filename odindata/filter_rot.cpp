#include "filter_rot.h"

#include "gridding.h"

void FilterRot::init(){

  angle=0.0;
  angle.set_unit("deg").set_description("angle");
  append_arg(angle,"angle");

  kernel=sqrt(2.0);
  kernel.set_unit("pixel").set_description("kernel size");
  append_arg(kernel,"kernel");
}

bool FilterRot::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  ODINLOG(odinlog,normalDebug) << "angle=" << angle << STD_endl;

  RotMatrix rotmat;
  rotmat.set_inplane_rotation(angle*PII/180.0);
  ODINLOG(odinlog,normalDebug) << "rotmat=" << rotmat.print() << STD_endl;


  TinyVector<float,2> offset=0.0;
  TinyMatrix<float,2,2> rotation;
  for(int irow=0; irow<2; irow++) {
    for(int icol=0; icol<2; icol++) {
      rotation(1-irow,1-icol)=rotmat[irow][icol];
    }
  }

  CoordTransformation<float,2> transform(TinyVector<int,2>(data.extent(phaseDim),data.extent(readDim)), rotation, offset, kernel);

  for(int irep=0; irep<data.extent(timeDim); irep++) {
    for(int islice=0; islice<data.extent(sliceDim); islice++) {
      data(irep,islice,all,all)=transform(data(irep,islice,all,all));
    }
  }

  // Adjust protocol
  Geometry& geo=prot.geometry;
  geo.set_orientation_and_offset(
    rotmat*geo.get_readVector(),
    rotmat*geo.get_phaseVector(),
    rotmat*geo.get_sliceVector(),
    geo.get_center()
  );

  return true;
}
