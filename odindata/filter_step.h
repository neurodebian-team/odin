//
// C++ Interface: filterstep
//
// Description:
//
//
// Author:  <Enrico Reimer>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FILTERSTEP_H
#define FILTERSTEP_H

#include <odindata/data.h>
#include <odindata/step.h>
#include <odindata/fileio.h>


// helper class for debugging the filter component
struct Filter {
  static const char* get_compName();
};

///////////////////////////////////////////////////////////////////////////////////


/**
	@author Enrico Reimer
*/
class FilterStep : public Step<FilterStep> {
public:
  virtual ~FilterStep() {}

  /**
   * Apply filter to protocol-data pair.
   */
  virtual bool process(Data<float,4>& data, Protocol& prot)const;

  /**
   * Apply filter to a 'pdmap'.
   */
  virtual bool process(FileIO::ProtocolDataMap& pdmap)const;

  // To be used by factory via duck typing
  static void create_templates(STD_list<FilterStep*>& result);
  static STD_string manual_group() {return "filter_steps";}
  static void interface_description(const FilterStep* step, STD_string& in, STD_string& out) {}

};

///////////////////////////////////////////////////////////////////////////////////



typedef StepFactory<FilterStep> FilterFactory; // The factory for filter steps


#endif
