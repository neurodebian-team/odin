#include "oversampling.h"
#include "data.h"


bool RecoOversampling::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  float os_factor=rd.coord().overSamplingFactor;

  if(os_factor>1.0) {

    Range all=Range::all();

    ComplexData<3>& indata=rd.data(Rank<3>());
    TinyVector<int,3> inshape=indata.shape();

    TinyVector<int,3> outshape=inshape;

    int insize=inshape(2);
    int outsize=int(float(insize)/os_factor+0.5);

    outshape(2)=outsize;

    int offset=insize/2-outsize/2; // Make zero-frequency column is zero-frequency column after cropping

    ODINLOG(odinlog,normalDebug) << "insize/outsize/offset=" << insize << "/" << outsize << "/" << offset << STD_endl;

    ComplexData<3> outdata(outshape);
    outdata(all,all,all)=indata(all,all,Range( offset, offset+outsize-1 ));

    rd.data(Rank<3>()).reference(outdata);
  }

  rd.coord().overSamplingFactor=1.0; // inidicate that oversampling has been removed

  return execute_next_step(rd,controller);
}
