/***************************************************************************
                          debug.h  -  description
                             -------------------
    begin                : Mon Oct 13 2008
    copyright            : (C) 2000-2021 by Thies Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RECODEBUG_H
#define RECODEBUG_H

#include "step.h"

class RecoDebug : public RecoStep {

  // implementing virtual functions of RecoStep
  STD_string label() const {return "debug";}
  STD_string description() const {return "Write messages to log";}
  bool process(RecoData& rd, RecoController& controller);
  RecoCoord input_coord() const {return RecoCoord::any();}
  void modify_coord(RecoCoord& coord) const {}
  RecoStep* allocate() const {return new RecoDebug;}
  void init();

  LDRstring messageprefix;

};


#endif

