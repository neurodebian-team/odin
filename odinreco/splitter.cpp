#include "splitter.h"
#include "splitter_code.h"

// Instantiations used in step.cpp
template class RecoSplitter<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, channel> >;
template class RecoSplitter<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, repetition> >;