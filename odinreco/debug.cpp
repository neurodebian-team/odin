#include "debug.h"
#include "data.h"


void RecoDebug::init() {
  Log<Reco> odinlog(c_label(),"init");

  messageprefix.set_description("Prefix of debug messages");
  append_arg(messageprefix,"messageprefix");
}


bool RecoDebug::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ODINLOG(odinlog,infoLog) << messageprefix << " - " << rd.coord().print() << STD_endl;

  return execute_next_step(rd,controller);
}
