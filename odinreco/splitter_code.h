#include "splitter.h"
#include "data.h"


template<unsigned int Nunmod, class Unmod, unsigned int Nsplit, class Split>
bool RecoSplitter<Nunmod,Unmod,Nsplit,Split>::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");


  // create new view of input data
  ComplexData<Nsplit+Nunmod>& indata=rd.data(Rank<Nsplit+Nunmod>());

  TinyVector<int,Nsplit> splitshape;
  for(int i=0; i<int(Nsplit); i++) splitshape(i)=indata.extent(i);

  TinyVector<int,Nunmod> outshape;
  for(int i=0; i<int(Nunmod); i++) outshape(i)=indata.extent(Nsplit+i);

  ODINLOG(odinlog,normalDebug) << "indata.shape()/outshape/splitshape=" << indata.shape() << "/" << outshape << "/" << splitshape << STD_endl;

  // Splitted output data with same (padded) shape as input data
  TinyVector<int,Nsplit+Nunmod> outshape_padded=1;
  for(unsigned int i=0; i<Nunmod; i++) outshape_padded(Nsplit+i)=outshape(i);


  // create domains for retrieving input data from the correct position
  // initialize unmodified part with outshape
  TinyVector<int,Nsplit+Nunmod> lowin=0;
  TinyVector<int,Nsplit+Nunmod> uppin=outshape_padded-1;

  TinyVector<int,Nsplit+Nunmod> lowout=0;
  TinyVector<int,Nsplit+Nunmod> uppout=outshape_padded-1;

   // output domain, the same for each dataset
  RectDomain<Nsplit+Nunmod> outdomain(lowout,uppout);

  ComplexData<Nsplit+Nunmod> outdata_padded(outshape_padded);
  ComplexData<Nunmod> outdata(outshape);


  for(int isplit=0; isplit<product(splitshape); isplit++) {

    // create Nsplit-dim index from linear index
    TinyVector<int,Nsplit> splitindex=index2extent(splitshape, isplit);

    // set input inidices
    for(unsigned int i=0; i<Nsplit; i++) lowin(i)=uppin(i)=splitindex(i);

    // copy data
    RectDomain<Nsplit+Nunmod> indomain(lowin,uppin);
    outdata_padded(outdomain)=indata(indomain);

    outdata_padded.convert_to(outdata); // bring to output shape

    // set data and index and feed data into rest of pipeline
    RecoData rdout; // separate data object since indata is just a reference
    rdout.copy_meta(rd); // Use all meta data from indata
    Split::modify(RecoIndex::separate, rdout.coord());
    Split::set_index(splitindex, rdout.coord());
    rdout.data(Rank<Nunmod>()).reference(outdata);
    if(!execute_next_step(rdout,controller)) return false;

  }

  return true;
}
