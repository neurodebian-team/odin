#include "collector.h"
#include "controller.h"
#include "phasecorr.h" // for phasemapcoll
#include "epinavcorr.h" // for epinavcoll
#include "collector_code.h"


RecoCollectorBase::~RecoCollectorBase() {
  Log<Reco> odinlog(label_cache.c_str(),"~RecoCollectorBase");
  if(todo.size()) {
    ODINLOG(odinlog,errorLog) << "TODO list not empty:" << STD_endl;
    for(CoordCountMap::const_iterator it=todo.begin(); it!=todo.end(); ++it) {
      ODINLOG(odinlog,errorLog) << "(" << it->first.print(RecoIndex::all) << "): " << it->second << STD_endl;
    }
  }
}


bool RecoCollectorBase::completed(const RecoCoord& coord, unsigned int custom_count) {
  Log<Reco> odinlog(c_label(),"completed");

  unsigned int numof_todo=count;
  if(custom_count) numof_todo=custom_count;

  if(numof_todo<2) return true; // dataset is always complete in this case

  RecoCoord coord_copy=coord;
  modify_coord(coord_copy); // just make sure we check against the output side of the functor

  ODINLOG(odinlog,normalDebug) << "coord_copy=" << coord_copy.print()  << STD_endl;

  MutexLock lock(todomutex);

  CoordCountMap::iterator it=todo.find(coord_copy);
  if(it==todo.end()) {

    // add new entry in todo
    todo[coord_copy]=(numof_todo-1);

  } else {

    it->second--; // reduce todo count by one
    if(!it->second) { // is count zero ?
      todo.erase(it);
      return true;
    }

  }

  return false;
}


bool RecoCollectorBase::calculate_count(RecoQueryContext& context, const RecoCoord& inmask, const RecoCoord& outmask) {
  Log<Reco> odinlog(c_label(),"calculate_count");

  label_cache=label();

  ODINLOG(odinlog,normalDebug) << "context.coord=" << context.coord.print()  << STD_endl;

//    modify_input_coord(context.coord);
  const CoordCountMap* inmap=context.controller.create_count_map(inmask);
  ODINLOG(odinlog,normalDebug) << "inmask=" << inmask.print() << STD_endl;
  if(!inmap) return false;

  const CoordCountMap* outmap=context.controller.create_count_map(outmask);
  ODINLOG(odinlog,normalDebug) << "outmask=" << outmask.print() << STD_endl;
  if(!outmap) return false;

  unsigned int incount=inmap->size();
  unsigned int outcount=outmap->size();

  ODINLOG(odinlog,normalDebug) << "incount/outcount=" << incount << "/" << outcount << STD_endl;

  if(!incount) {
    ODINLOG(odinlog,errorLog) << "No input data" << STD_endl;
    return false;
  }
  if(!outcount) {
    ODINLOG(odinlog,errorLog) << "No output data" << STD_endl;
    return false;
  }

  if(incount%outcount) {
    ODINLOG(odinlog,errorLog) << "incount(" << incount << ") not a multiple of outcount(" << outcount << ")" << STD_endl;
    ODINLOG(odinlog,errorLog) << "inmask=" << inmask.print() << STD_endl;
    ODINLOG(odinlog,errorLog) << "outmask=" << outmask.print() << STD_endl;
    return false;
  }

  count=incount/outcount; // simply take the quotient
  ODINLOG(odinlog,normalDebug) << "count=" << count << STD_endl;

  return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////


// Instantiations used in step.cpp
template class RecoCollector<1, RecoDim<1, readout>,                      2, RecoDim<2, line3d, line> >;
template class RecoCollector<2, RecoDim<2, line, readout>,                1, RecoDim<1, line3d> >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, slice>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, channel>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, cycle>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, te>, true >;
//template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, epi>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, userdef>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, repetition>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        1, RecoDim<1, freq>, true >;
template class RecoCollector<4, RecoDim<4, slice, line3d, line, readout>, 1, RecoDim<1, repetition>, true >;
template class RecoCollector<1, RecoDim<1, readout>,                      1, RecoDim<1, average>, true >;
template class RecoCollector<1, RecoDim<1, readout>,                      1, RecoDim<1, echo>, true >;
template class RecoCollector<1, RecoDim<1, readout>,                      1, RecoDim<1, channel>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        2, RecoDim<2, channel, repetition>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        3, RecoDim<3, channel, repetition, slice>, true >;
template class RecoCollector<3, RecoDim<3, line3d, line, readout>,        5, RecoDim<5, cycle, slice, echo, epi, channel>, true >;

