#include "halffour.h"

#include <odindata/fitting.h>


bool RecoHalfFour::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> shape=data.shape();

  int nread=shape(2);
  int nline=shape(1);
  int startindex=nline/2;
  int nline3d=shape(0);

  // Apply phase correction using the central line
  data.partial_fft(TinyVector<bool,3>(false,false,true)); // FFT in read

  ComplexData<1> centline(data(nline3d/2,startindex,all));

  Data<float,1> centphase(centline.phasemap());
  Data<float,1> centmagn(cabs(centline));
  Data<float,1> centerr(nread);
  for(int iread=0; iread<nread; iread++) centerr(iread)=secureInv(pow(centmagn(iread),2));

  LinearFunction linf;
  linf.fit(centphase,centerr);

  for(int iread=0; iread<nread; iread++) {
    data(all,all,iread)*=expc(STD_complex(0.0, -(iread*linf.m.val+linf.c.val)));
  }

  data.partial_fft(TinyVector<bool,3>(false,false,true),false); // inv FFT in read


  ComplexData<1> oneline(nread);
  int nmeas=nline-startindex;
  for(int iphase3d=0; iphase3d<nline3d; iphase3d++) {
    for(int iline=1; iline<nmeas; iline++) {

      // mirror remaining lines
      int isrc=startindex+iline;
      int idst=startindex-iline;

      int idst3d=nline3d-1-iphase3d; // also mirror 3rd dim

      if(idst>=0) {
        oneline=conjc(data(iphase3d,isrc,all).reverse(0));
        if(!(nread%2)) oneline.shift(0,1); // for even nread, the center frequency is at nread/2
        data(idst3d,idst,all)=oneline;
      }
    }
  }

  if(!(nread%2)) data(all,0,all)=STD_complex(0.0); // zero-fill garbage from cyclic shift

  return execute_next_step(rd,controller);
}
