#include "swi.h"
#include "data.h"
#include "controller.h"

#include <odindata/statistics.h>
#include <odindata/linalg.h>


STD_vector<TinyVector<int,3> > neighb_indices(int radius_pixels) {
  STD_vector<TinyVector<int,3> > result;

  int n=radius_pixels;

  for(int k=-n; k<=n; k++) {
    for(int j=-n; j<=n; j++) {
      for(int i=-n; i<=n; i++) {
        float radius=sqrt(float(k*k+j*j+i*i));
        if(radius<=radius_pixels && (k || j || i) ) {
          result.push_back(TinyVector<int,3>( k, j, i));
        }
      }
    }
  }

  return result;
}



///////////////////////////////////////////////////////////////////////


bool RecoSwi::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> shape=data.shape();

  Data<float,3> pha(phase(data));
  Data<float,3> magn(cabs(data));

  int pixelradius=3; // best


  // Create linear mask for valid pixels
  Data<char,1> mask(magn.size()); mask=1;
  for(unsigned int i=0; i<magn.size(); i++) {
    TinyVector<int,3> index=magn.create_index(i);
    for(int j=0; j<3; j++) {
      if(index(j)<pixelradius)            mask(i)=0;
      if(index(j)>shape(j)-pixelradius-1) mask(i)=0;
    }
  }


  STD_vector<TinyVector<int,3> > neighboffset=neighb_indices(pixelradius);
  int numof_neigb=neighboffset.size();

  for(int j=0; j<numof_neigb; j++) {
    ODINLOG(odinlog,normalDebug) << "neighboffset[" << j << "]=" << neighboffset[j] << STD_endl;
  }

  Data<float,1> neighbensemble(numof_neigb);

  Data<float,2> matrix(numof_neigb,4);
  Data<float,1> phavec(numof_neigb);
  Data<float,1> xvec(4); // will contain 3 spatial indices and phase offset

  // Evaluate local phase contrast
  Data<float,3> cog(shape); cog=0.0;
  Data<float,3> phadiff(shape); phadiff=0.0;
  Data<float,3> med(shape); med=0.0;
  Data<float,3> swi(shape); swi=0.0;

  TinyVector<float,3> cogvec;

  for(unsigned int i=0; i<magn.size(); i++) {
    if(mask(i)) {
      TinyVector<int,3> index=magn.create_index(i);

      float centpha=pha(index);

      cogvec=0.0;

      for(int j=0; j<numof_neigb; j++) {
        TinyVector<int,3> indexoffset=neighboffset[j];
        TinyVector<int,3> neighbindex=index+indexoffset;

        float magnitude=magn(neighbindex);

        neighbensemble(j)=magnitude;

        cogvec+=magnitude*indexoffset;


        // Subtract center phase
        float phadiff=pha(neighbindex)-centpha;

        // Unwrap phase
        while(phadiff>PII)  phadiff-=2.0*PII;
        while(phadiff<-PII) phadiff+=2.0*PII;

        // Set up matrix for pseudo-inversion
        float weight=magn(neighbindex);
        matrix(j,0)=weight*indexoffset(0);
        matrix(j,1)=weight*indexoffset(1);
        matrix(j,2)=weight*indexoffset(2);
        matrix(j,3)=weight;
        phavec(j)=weight*phadiff;
      }

      float neighbsum=sum(neighbensemble);
      if(neighbsum) cogvec/=neighbsum;
      else cogvec=0.0;
      float cogval=sqrt(sum(cogvec*cogvec));
      cog(index)=cogval;

      float medval=median(neighbensemble);
      med(index)=medval;

      xvec=solve_linear(matrix, phavec, 0.05); // Linear 3D fit using neighborhood
      float phadiffval=fabs(xvec(3)); // Use phase offset/difference at central spatial position
      phadiff(index)=phadiffval;

      if(cogval<0.5) swi(index)=medval*phadiffval;

    }
  }


  // feed arrays into rest of pipeline
  rd.coord().index[userdef].set_numof(4);

  Protocol prot(controller.protocol()); // Local copy of protocol, used for override_protocol
  rd.override_protocol=&prot;
  STD_string series;
  int number;
  prot.study.get_Series(series, number);

  RecoData rdswi(rd);
  rdswi.coord().index[userdef]=0;
  rdswi.data(Rank<3>()).reference(float2real(swi));
  if(!execute_next_step(rdswi,controller)) return false;

  RecoData rdcog(rd);
  rdcog.coord().index[userdef]=1;
  rdcog.data(Rank<3>()).reference(float2real(cog));
  prot.study.set_Series(series+"_cog",number);
  if(!execute_next_step(rdcog,controller)) return false;

  RecoData rdphadiff(rd);
  rdphadiff.coord().index[userdef]=2;
  rdphadiff.data(Rank<3>()).reference(float2real(phadiff));
  prot.study.set_Series(series+"_phadiff",number);
  if(!execute_next_step(rdphadiff,controller)) return false;

  RecoData rdmed(rd);
  rdmed.coord().index[userdef]=3;
  rdmed.data(Rank<3>()).reference(float2real(med));
  prot.study.set_Series(series+"_median",number);
  if(!execute_next_step(rdmed,controller)) return false;

  return true;
}

