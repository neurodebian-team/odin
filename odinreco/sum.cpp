#include "sum.h"
#include "sum_code.h"

// Instantiations used in step.cpp
template class RecoSum<1, RecoDim<1, readout>, 1,               RecoDim<1, average> >;
template class RecoSum<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, cycle> >;
template class RecoSum<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, repetition> >;
template class RecoSum<3, RecoDim<3, line3d, line, readout>, 1, RecoDim<1, repetition>, true >;
