#include "fmri.h"
#include "data.h"
#include "controller.h"

#include <odindata/correlation.h>

void RecoFMRI::init() {

  stimsize=0;
  stimsize.set_description("Number of repetitions during stimulation");
  append_arg(stimsize,"stimsize");

  restsize=0;
  restsize.set_description("Number of repetitions during rest");
  append_arg(restsize,"restsize");
}


bool RecoFMRI::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  if(!stimsize || !restsize) {
    ODINLOG(odinlog,errorLog) << "stimsize/restsize=" << stimsize << "/" << restsize << STD_endl;
    return false;
  }

  ComplexData<4>& indata=rd.data(Rank<4>());
  Data<float,4> magn(cabs(indata));
  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));



  int nrep=inshape(0);
  int blocksize=stimsize+restsize;

  Data<float,1> design(nrep); design=0.0;
  for(int irep=0; irep<nrep; irep++) {
    int i=irep%blocksize;
    if(i<stimsize) design(irep)=1.0;
  }
  float tr=controller.protocol().seqpars.get_RepetitionTime();
  int hemodynamic_delay=int(secureDivision(5.0,0.001*tr)+0.5);
  if(hemodynamic_delay) {
    design.shift(0,hemodynamic_delay);
    design(Range(0,hemodynamic_delay-1))=0.0;
  }
  ODINLOG(odinlog,normalDebug) << "hemodynamic_delay/design=" << hemodynamic_delay << "/" << design << STD_endl;


  float pThresh=1e-3; //secureDivision(0.05,magn.size()); // Bonferroni corrected

  // Create mean image as background
  Data<float,3> overlay(outshape);
  for(int iphase3d=0; iphase3d<inshape(1); iphase3d++) {
    for(int iphase=0; iphase<inshape(2); iphase++) {
      for(int iread=0; iread<inshape(3); iread++) {
        overlay(iphase3d,iphase,iread)=mean(magn(all,iphase3d,iphase,iread));
      }
    }
  }

  float maxmean=max(overlay);

  Data<float,1> tcourse(nrep);
  for(int iphase3d=0; iphase3d<inshape(1); iphase3d++) {
    for(int iphase=0; iphase<inshape(2); iphase++) {
      for(int iread=0; iread<inshape(3); iread++) {
        tcourse=magn(all,iphase3d,iphase,iread);

        correlationResult co=correlation(tcourse, design);

        if( co.p<pThresh ) { // Is voxel activated ?
          overlay(iphase3d,iphase,iread)=(0.3*co.z)*maxmean; // outline activated region
        }
      }
    }
  }


  rd.coord().index[repetition].set_numof(0);
  rd.data(Rank<4>()).free();
  rd.data(Rank<3>()).resize(outshape);
  rd.data(Rank<3>())=float2real(overlay);
  rd.coord().index[repetition]=0;

  return execute_next_step(rd,controller);
}
