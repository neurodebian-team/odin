#include "dump.h"
#include "store.h"
#include "data.h"

template<int N_rank>
bool dump_recodata(const ComplexData<N_rank>& indata, const STD_string& filename, RecoData::dataMode mode) {

  if(indata.size()) {
    Data<float,N_rank> magn(indata.shape());
    if (mode==RecoData::real_data || mode==RecoData::weighted_real_data)
      magn = creal(indata);
    else magn = cabs(indata);
    magn.autowrite(filename); 
    return true;
  }
  return false;
}


void RecoDump::init() {
  Log<Reco> odinlog(c_label(),"init");

  prefix=STD_string(".")+SEPARATOR_STR+"dump";
  prefix.set_cmdline_option("dp").set_description("Prefix of dump files");
  append_arg(prefix,"prefix");

  format=RecoStore::default_format();
  format.set_cmdline_option("df").set_description("Output format (file extension) of dump files");
  append_arg(format,"format");

}


bool RecoDump::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  STD_string filename=prefix;

  STD_string coordstr=rd.coord().print(RecoIndex::filename);
  if(coordstr!="") filename+="_"+coordstr;
  filename+="."+format;
  ODINLOG(odinlog,normalDebug) << "filename=" << filename << STD_endl;

  bool dumped= // stop if data with highest dim written
    dump_recodata(rd.data(Rank<5>()),filename,rd.mode) ||
    dump_recodata(rd.data(Rank<4>()),filename,rd.mode) ||
    dump_recodata(rd.data(Rank<3>()),filename,rd.mode) ||
    dump_recodata(rd.data(Rank<2>()),filename,rd.mode) ||
    dump_recodata(rd.data(Rank<1>()),filename,rd.mode);

  ODINLOG(odinlog,normalDebug) << "dumped=" << dumped << STD_endl;

  return execute_next_step(rd,controller);
}
