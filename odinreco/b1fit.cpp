#include "b1fit.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>

bool RecoB1Fit::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  TinyVector<int,4> inshape=indata.shape();

  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));
  ComplexData<3>& outdata=rd.data(Rank<3>());
  outdata.resize(outshape);
  outdata=STD_complex(0.0);

  dvector flips=controller.dim_values(userdef);
  ODINLOG(odinlog,normalDebug) << "flips=" << flips.printbody() << STD_endl;
  int nflips=flips.size();
  if(nflips!=inshape(0)) {
    ODINLOG(odinlog,errorLog) << "size mismatch" << STD_endl;
    return false;
  }

  Data<float,4> magn(cabs(indata));
  float masklevel=0.2;
  float noiselevel=masklevel*mean(magn(0,all,all,all));

  LinearFunction linf;
  Data<float,1> pixel4fit(nflips);
  Data<float,1> flipwinkel(nflips);
  for ( int icount = 0; icount < nflips; icount++ ) {
    flipwinkel(icount)=flips[icount];
  }
  Data<float,1> pixel4fitcalc(nflips-2);
  Data<float,1> ysigmacalc(nflips-2); ysigmacalc=1.0;
  Data<float,1> flipwinkelcalc(nflips-2);

  for(int iphase3d=0; iphase3d<inshape(1); iphase3d++) {
    for(int iphase=0; iphase<inshape(2); iphase++) {
      for(int iread=0; iread<inshape(3); iread++) {
        if(magn(0,iphase3d,iphase,iread)>noiselevel) {
          pixel4fit=magn(all,iphase3d,iphase,iread);
	  int checkmin = minIndex(pixel4fit)(0);
	  
	  if ( (checkmin == 0) || (checkmin == 1) ) { flipwinkelcalc(0) = flipwinkel(0); flipwinkelcalc(1) = flipwinkel(1); flipwinkelcalc(2) = flipwinkel(2); pixel4fitcalc(0) = pixel4fit(0); pixel4fitcalc(1) = pixel4fit(1); pixel4fitcalc(2) = pixel4fit(2); }
	  if ( checkmin == 2 ) { flipwinkelcalc(0) = flipwinkel(1); flipwinkelcalc(1) = flipwinkel(2); flipwinkelcalc(2) = flipwinkel(3); pixel4fitcalc(0) = pixel4fit(1); pixel4fitcalc(1) = pixel4fit(2); pixel4fitcalc(2) = pixel4fit(3); }
	  if ( (checkmin == 3) || (checkmin == 4) ) { flipwinkelcalc(0) = flipwinkel(2); flipwinkelcalc(1) = flipwinkel(3); flipwinkelcalc(2) = flipwinkel(4); pixel4fitcalc(0) = pixel4fit(2); pixel4fitcalc(1) = pixel4fit(3); pixel4fitcalc(2) = pixel4fit(4); }

          linf.fit(pixel4fitcalc,ysigmacalc,flipwinkelcalc);
	  float error_ppp = linf.m.err;
	  float m_ppp = linf.m.val;
	  float c_ppp = linf.c.val;
	  pixel4fitcalc(2) = -pixel4fitcalc(2);
          linf.fit(pixel4fitcalc,ysigmacalc,flipwinkelcalc);
	  float error_ppm = linf.m.err;
	  float m_ppm = linf.m.val;
	  float c_ppm = linf.c.val;
	  pixel4fitcalc(1) = -pixel4fitcalc(1);
          linf.fit(pixel4fitcalc,ysigmacalc,flipwinkelcalc);
	  float error_pmm = linf.m.err;
	  float m_pmm = linf.m.val;
	  float c_pmm = linf.c.val;

	  float B1;
	  if ( error_ppp < error_ppm && error_ppp < error_pmm ) {
	    B1 = fabs(secureDivision(m_ppp*180.0,c_ppp));
	  } else if ( error_ppm < error_pmm ) {
	    B1 = fabs(secureDivision(m_ppm*180.0,c_ppm));
	  } else {
	    B1 = fabs(secureDivision(m_pmm*180.0,c_pmm));
          }

	  if ( B1 > 1.8 ) { B1 = 0; }
          check_range<float>(B1, 0.4, 1.6);
          outdata(iphase3d,iphase,iread)=B1;
        }
      }
    }
  }


  return execute_next_step(rd,controller);
}
