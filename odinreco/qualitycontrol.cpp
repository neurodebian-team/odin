#include "qualitycontrol.h"
#include "data.h"


bool RecoQualityControlSpike::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> shape=data.shape();

  float kspace_center_frac=0.5;
  float kspace_magn_excess=4.0;
  float spike_excess=3.0;

  TinyVector<int,3> lowerBounds, upperBounds;
  for(int idim=0; idim<3; idim++) {

    int low=int(0.5*shape(idim) * (1.0-kspace_center_frac) + 0.5);
    if(low<0) low=0;
    lowerBounds(idim) = low;

    int upp=int(0.5*shape(idim) * (1.0+kspace_center_frac) + 0.5);
    if(upp>(shape(idim)-1)) upp=shape(idim)-1;
    upperBounds(idim) = upp;
  }
  ODINLOG(odinlog,normalDebug) << "lowerBounds/upperBounds=" << lowerBounds << "/" << upperBounds << STD_endl;

  RectDomain<3> kspace_center(lowerBounds, upperBounds);

  int npts_kspace=data.size();
  int npts_kspace_center=product(TinyVector<int,3>(upperBounds-lowerBounds+1));
  int npts_kspace_periphery=npts_kspace-npts_kspace_center;
  ODINLOG(odinlog,normalDebug) << "npts_kspace/npts_kspace_center=" << npts_kspace << "/" << npts_kspace_center << STD_endl;

  totalmutex.lock();
  total_acqpts+=npts_kspace_periphery;
  totalmutex.unlock();

  Data<float,3> magn(cabs(data));

  magn(kspace_center)=0.0;
  float meanmagn_periphery=secureDivision(sum(magn),npts_kspace_periphery);
  ODINLOG(odinlog,normalDebug) << "meanmagn_periphery=" << meanmagn_periphery << STD_endl;

  float kpsace_thresh=kspace_magn_excess*meanmagn_periphery;

  for(int iphase3d=0; iphase3d<shape(0); iphase3d++) {
    for(int iphase=0; iphase<shape(1); iphase++) {
      for(int iread=2; iread<(shape(2)-2); iread++) {
        float magnval=magn(iphase3d,iphase,iread);
        if(magnval>kpsace_thresh) {
          if(magnval>(spike_excess*magn(iphase3d,iphase,iread-2)) && magnval>(spike_excess*magn(iphase3d,iphase,iread+2))) {
            MutexLock lock(spikemutex);
            spikecount[ rd.coord().index[channel]]++;
//            data(iphase3d,iphase,iread)=STD_complex(1.0);
            ODINLOG(odinlog,normalDebug) << "Spike detected at iphase3d/iphase/iread=" << iphase3d << "/" << iphase << "/" << iread << " in k-space of " << rd.coord().print() << STD_endl;
          }
        }
      }
    }
  }

  return execute_next_step(rd,controller);
}


//////////////////////////////////////////////////////////////////////


bool RecoQualityControlSpike::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {
    total_acqpts=0;
    spikecount.clear();
  }

  unsigned int total=0;
  STD_string spikestr;
  if(context.mode==RecoQueryContext::finalize) {
    for(STD_map<unsigned int, UInt>::const_iterator it=spikecount.begin(); it!=spikecount.end(); ++ it) {
      spikestr+=itos(it->second)+"("+itos(it->first)+") ";
      total+=it->second;
    }
    ODINLOG(odinlog,infoLog) << "spikecount=" << spikestr << "    total / ratio = " << total << " / " << secureDivision(total,total_acqpts) << STD_endl;
    if(spikecount.size()) {
      ODINLOG(odinlog,warningLog) << "Spikes detected" << STD_endl;
    }
  }

  return RecoStep::query(context);
}
