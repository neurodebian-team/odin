#include "blackboard.h"
#include "controller.h"


RecoBlackBoard::~RecoBlackBoard() {
  // Free thread events
  for(LabelEventMap::iterator lit=eventmap.begin(); lit!=eventmap.end(); ++lit) {
    for(CoordEventMap::iterator cit=lit->second.begin(); cit!=lit->second.end(); ++cit) {
      delete cit->second;
    }
  }
}


void RecoBlackBoard::post(const STD_string& label, const RecoData& data) {
  Log<Reco> odinlog("RecoBlackBoard","post");

  postedmutex.lock();
  CoordDataMap& cdmap=posted[label];
  if(cdmap.find(data.coord())!=cdmap.end()) {
    ODINLOG(odinlog,warningLog) << label << "(" << data.coord().print() << ") posted already, will overwrite it" << STD_endl;
  }
  cdmap[data.coord()]=data; // deep copy
  postedmutex.unlock();

  ODINLOG(odinlog,normalDebug) << "Posted " << label << "(" << data.coord().print() << ")" << STD_endl;


  // Notify other waiting threads via event
  STD_list<Event*> eventlist;

  eventmutex.lock();
  LabelEventMap::iterator lit=eventmap.find(label);
  if(lit!=eventmap.end()) {
    CoordEventMap& ccm=lit->second;
    for(CoordEventMap::const_iterator cit=ccm.begin(); cit!=ccm.end(); ++cit) {
      if(cit->first==data.coord()) eventlist.push_back(cit->second); // Find ALL coords whicht match the data
    }
  }
  eventmutex.unlock();

  if(eventlist.size()) {
    ODINLOG(odinlog,normalDebug) << "Signaling " << eventlist.size() << " events waiting for " << label << "(" << data.coord().print() << ")" << STD_endl;
    for(STD_list<Event*>::const_iterator it=eventlist.begin(); it!=eventlist.end(); ++it) {
      (*it)->signal();
    }
  }
}


bool RecoBlackBoard::get_data(const STD_string& label, RecoData& data) {
  Log<Reco> odinlog("RecoBlackBoard","get_data");

  MutexLock lock(postedmutex);

  LabelDataMap::const_iterator lit=posted.find(label);
  if(lit==posted.end()) return false;

  const CoordDataMap& cdmap=lit->second;
  CoordDataMap::const_iterator cit=cdmap.find(data.coord());
  if(cit==cdmap.end()) return false;

  data=cit->second; // deep copy

  return true;
}



bool RecoBlackBoard::inquire(const STD_string& label, RecoData& data, bool blocking) {
  Log<Reco> odinlog("RecoBlackBoard","inquire");

  ODINLOG(odinlog,normalDebug) << "Requested " << label << "(" << data.coord().print() << ")" << STD_endl;


  if(get_data(label,data)) {return true;} // We're done here

  if(!blocking) {return false;} // We're done here

  Event* event=0;

  eventmutex.lock();
  CoordEventMap& ccm=eventmap[label];
  CoordEventMap::iterator cit=ccm.find(data.coord());
  if(cit==ccm.end()) {
    ODINLOG(odinlog,normalDebug) <<  "New event for " << label << "(" << data.coord().print() << ")" << STD_endl;
    event=new Event;
    ccm[data.coord()]=event;
  } else {
    ODINLOG(odinlog,normalDebug) <<  "Using existing event for " << label << "(" << data.coord().print() << ")" << STD_endl;
    event=cit->second; // Already there
  }
  eventmutex.unlock();

  if(event) event->wait(); // Block

  ODINLOG(odinlog,normalDebug) << "received signal for " << label << "(" << data.coord().print() << ")" << STD_endl;

  bool result=get_data(label,data); // Try again

  if(!result) {
    ODINLOG(odinlog,warningLog) << "Data not available after wait: " << label << "(" << data.coord().print() << ")" << STD_endl;
  }

  return result;
}


void RecoBlackBoard::release_waiting_threads() {
  MutexLock lock(eventmutex);
  for(LabelEventMap::const_iterator lit=eventmap.begin(); lit!=eventmap.end(); ++lit) {
    for(CoordEventMap::const_iterator cit=lit->second.begin(); cit!=lit->second.end(); ++cit) {
      cit->second->signal();
    }
  }
}

///////////////////////////////////////////////////////////////////////////////////////////

void RecoPost::init() {
  postlabel.set_description("Post data on blackboard with this label");
  append_arg(postlabel,"postlabel");
}


bool RecoPost::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");
  controller.post_data(postlabel, rd);
  return execute_next_step(rd,controller);
}


bool RecoPost::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");
  if(context.mode==RecoQueryContext::prep) {
    context.controller.announce_data(postlabel);
  }
  return RecoStep::query(context);
}
