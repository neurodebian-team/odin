#include "slidingwindow.h"
#include "data.h"


bool RecoSlidingWindow::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  int oldnrep=rd.coord().numof(repetition);
  int ncycle=rd.coord().numof(cycle);

  int newnrep=(oldnrep-1)*ncycle+1;
  ODINLOG(odinlog,normalDebug) << "oldnrep/ncycle/newnrep=" << oldnrep << "/" << ncycle << "/" << newnrep << STD_endl;

  rd.coord().index[repetition].set_numof(newnrep);

  int icycle=rd.coord().index[cycle];
  int ioldrep=rd.coord().index[repetition];

  int index=ioldrep*ncycle+icycle;
  ODINLOG(odinlog,normalDebug) << "index=" << index << STD_endl;

  ivector inewreplist;
  for(int i=0; i<ncycle; i++) {
    int inewrep=index-i;
    if(inewrep>=0 && inewrep<newnrep) inewreplist.push_back(inewrep);
  }
  ODINLOG(odinlog,normalDebug) << "inewreplist(" << index << ")=" << inewreplist.printbody() << STD_endl;


  for(unsigned int i=0; i<inewreplist.size(); i++) {
    RecoData rdcopy(rd);
    rdcopy.coord().index[repetition]=inewreplist[i];
    if(!execute_next_step(rdcopy,controller)) return false;
  }

  return true;
}
