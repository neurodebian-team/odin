#include "reader_custom.h"

#include <odinpara/sample.h> // for loading sample file


bool RecoReaderCustom::init(const STD_string& input_filename) {
  Log<Reco> odinlog("RecoReaderCustom","init");

  Range all=Range::all();

  // Create synthetic raw data using a virtual sample

  // Load sample
  Sample smp;
  if(smp.load(input_filename)<0) {
    ODINLOG(odinlog,errorLog) << "Cannot read sample file " << input_filename << STD_endl;
    return false;
  }
  farray spinDensity=smp.get_spinDensity();
  spinDensity.autosize();
  ODINLOG(odinlog,infoLog) << "Read sample with extent " << STD_string(spinDensity.get_extent()) << STD_endl;


  /////////////////////////////////////////////////////////////////////////////////////
  // Create the raw data

  // Create signal data for one image
  ComplexData<2> oneimage(float2real(Data<float,2>(spinDensity)));
  oneimage.fft();

  // Create 2-dim raw data array, these dims are: (repetition x slice x phase, read)
  int nrep=15;
  int nslice=7;
  int nphase=oneimage.extent(1);
  int nread=oneimage.extent(0);
  int nadcs=nrep*nslice*nphase;
  rawdata.resize(nadcs, nread);

  // Resize the vector of reco coordinates (indices)
  coord_vec_cache.resize(nadcs);

  // Iterate over dimensions and create raw data and reco indices
  count=0;
  for(int irep=0; irep<nrep; irep++) {
    for(int islice=0; islice<nslice; islice++) {
      for(int iphase=0; iphase<nphase; iphase++) {

        // Create signal data
        STD_complex factor=(1.0+0.2*sin(0.5*PII*irep))*(1.0+0.3*islice/nslice); // some modulation so we get modified in repetion and slice dimension
        rawdata(count,all)=factor*oneimage(iphase,all);

        // Create corresponding coordinate, this is equivalent to the MDH on Siemens
        RecoCoord& coord=coord_vec_cache[count]; // use reference for convenience
        coord.index[repetition]=irep;
        coord.index[slice]=islice;
        coord.index[line]=iphase;
        // For other indices and their explanation see enum recoDim in odinpara/reco.h

        // Other important stuff to set for EPI
        // coord.flags=coord.flags|recoReflectBit;  // For reflected ADCs, e.g. odd echoes in EPI
        // coord.readoutShape // Pointer to 1-dim array representating the readout gradient shape for ramp-sampling correction
        coord.dwellTime=0.01; // Dwell time, required for field-map based off-resonance correction
        coord.overSamplingFactor=1.0; // Degree of oversampling

        count++; // global counter
      }
    }
  }

  // adjust size that will be returned by reader, this is required for gridding (e.g. ramp-sampling correction)
  size_cache(0)=1; // 2nd phase encoding direction, not uesd by now
  size_cache(1)=nphase;
  size_cache(2)=nread;


  // This will shift the image in 3D-space according to the offsets
  // given in relative units of the FOV
  reloffset_cache=TinyVector<float,3>(0.0,0.05,0.1); // shift image in phase and read direction by 0.05*FOV and 0.1*FOV, respectively


  /////////////////////////////////////////////////////////////////////////////////////

  // Adjust protocol. We can keep the default settings, or fill in our own values.
  // Most of these settings are irrelevant for the reconstruction, but will
  // stored together with the raw data, e.g. in DICOM files

  // Study information, which is stored together with the raw data
  prot_cache.study.set_Patient("TESTID", "John Doe", "19730913", 'M', 80.0, 2000.0);
  prot_cache.study.set_Context("RecoReaderCustom","DrNo");
  prot_cache.study.set_Series("Test",7);

  // Set some parameters that might be relevant to reco
  prot_cache.seqpars.set_RepetitionTime(1300.0).set_FlipAngle(66.0); // For correct T1 fit
  prot_cache.seqpars.set_ReductionFactor(1); // For correct GRAPPA interpolation

  // Geometry settings
  prot_cache.geometry.set_orientation(-66.7, 78.2, -124.7);
  prot_cache.geometry.set_offset(readDirection,22.7);
  prot_cache.geometry.set_offset(phaseDirection,-5.9);
  prot_cache.geometry.set_offset(sliceDirection,1.9);
  prot_cache.geometry.set_FOV(readDirection,192.6);
  prot_cache.geometry.set_FOV(phaseDirection,200.2);
  prot_cache.geometry.set_sliceDistance(6.1);
  prot_cache.geometry.set_sliceThickness(3.2);


  /////////////////////////////////////////////////////////////////////////////////////

  count=0; // reset global counter because it is used in fetch

  return true;
}




bool RecoReaderCustom::fetch(RecoCoord& coord, ComplexData<1>& adc) {
  if(count>=coord_vec_cache.size()) return false; // This check is necessary for proper multi-threading

  // Return the next raw data ADC
  coord=coord_vec_cache[count];
  adc.resize(rawdata.extent(1));
  adc=rawdata(count, Range::all()); // Return deep copy
  bool contin=int(count)<rawdata.extent(0); // return true as long as raw data is available
  count++;
  return contin;
}


dvector RecoReaderCustom::dim_values(recoDim dim) const {
  // Return values associated with dimension 'dim', i.e. the TEs of a multi-echo readout, if available
  return dvector();
}


STD_string RecoReaderCustom::cmdline_opts() const {
  // Return extra command-line options of the reco
  return "";
}
