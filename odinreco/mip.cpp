#include "mip.h"
#include "data.h"
#include "controller.h"


void  RecoMip::init() {
  neighbors=4;
  neighbors.set_cmdline_option("mn").set_description("In sliding-window maximum-intensity projection, use this number of neighbors in each direction");
  append_arg(neighbors,"neighbors");

}

///////////////////////////////////////////////////////////////////////////


bool RecoMip::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<3>& indata=rd.data(Rank<3>());
  TinyVector<int,3> shape=indata.shape();


  Array<float,3> magn(cabs(indata));

  // Sliding window maximum-intensity projection (MIP)
  Array<float,3> mip(shape); mip=0.0;
  for(int iline=0; iline<shape(1); iline++) {
    for(int iread=0; iread<shape(2); iread++) {
      for(int i=neighbors; i<(shape(0)-1-neighbors); i++) {
        mip(i,iline,iread)=max(magn(Range(i-neighbors,i+neighbors),iline,iread));
      }
    }
  }


  // MIP of each dimension
  Array<float,3> mipall[3];
  mipall[0].resize(1,shape(0),shape(1));
  mipall[1].resize(1,shape(0),shape(2));
  mipall[2].resize(1,shape(1),shape(2));
  for(int j=0; j<shape(0); j++) for(int i=0; i<shape(1); i++) mipall[0](0,j,i)=max(magn(j,i,all));
  for(int j=0; j<shape(0); j++) for(int i=0; i<shape(2); i++) mipall[1](0,j,i)=max(magn(j,all,i));
  for(int j=0; j<shape(1); j++) for(int i=0; i<shape(2); i++) mipall[2](0,j,i)=max(magn(all,j,i));



  rd.coord().index[userdef].set_numof(5);

  Protocol prot(controller.protocol()); // Local copy of protocol, used for override_protocol
  rd.override_protocol=&prot;
  STD_string series;
  int number;
  prot.study.get_Series(series, number);


  // feed indata unchanged into rest of pipeline
  RecoData rdcopy(rd);
  rdcopy.coord().index[userdef]=0;
  if(!execute_next_step(rdcopy,controller)) return false;


  // feed mip into rest of pipeline
  RecoData rdmip(rd);
  rdmip.data(Rank<3>()).reference(float2real(mip));
  rdmip.coord().index[userdef]=1;
  prot.study.set_Series(series+"_mip",number);
  if(!execute_next_step(rdmip,controller)) return false;

  for(int idir=0; idir<3; idir++) {
    STD_string id;
    if(idir==0) id=recoDimLabel[readout];
    if(idir==1) id=recoDimLabel[line];
    if(idir==2) id=recoDimLabel[line3d];

    // feed mipall into rest of pipeline
    RecoData rdmipall(rd);
    rdmipall.data(Rank<3>()).reference(float2real(mipall[idir]));
    rdmipall.coord().index[userdef]=2+idir;
    prot.study.set_Series(series+"_mip"+id,number);
    if(!execute_next_step(rdmipall,controller)) return false;
  }

  return true;
}

