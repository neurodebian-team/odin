#include "miview_rois.h"
#include "miviewview.h" // for Log

#include <odindata/data.h>

MiViewRois::MiViewRois() {

  valid=false;


  roismode.set_parmode(hidden).set_cmdline_option("rois").set_description("Activate ROIs mode");
  append_member(roismode,"roismode");

  clearall.set_description("Clear all ROIs");
  append_member(clearall,"ClearAll");

  clearslice.set_description("Clear ROIs of current slice");
  append_member(clearslice,"ClearSlice");

}


MiViewRois::~MiViewRois() {
}


bool MiViewRois::init(int nx, int ny, int nz) {
  Log<MiViewComp> odinlog("MiViewRois","init");
//  Range all=Range::all();

  valid=roismode;
  ODINLOG(odinlog,normalDebug) << "valid=" << valid << STD_endl;


  if(!valid) return false;

  overlay_map.redim(nz,ny,nx);

  return true;
}


void MiViewRois::new_slice(const float *data, int slice) {
  Log<MiViewComp> odinlog("MiViewRois","new_slice");

  ODINLOG(odinlog,normalDebug) << "valid/slice=" << valid << "/" << slice << STD_endl;

  ndim nn=overlay_map.get_extent();
  ODINLOG(odinlog,normalDebug) << "nn=" << nn << STD_endl;

  ndim index=nn;
  index[0]=slice;
  int ny=nn[1];
  int nx=nn[2];
  for(int iy=0; iy<ny; iy++) {
    for(int ix=0; ix<nx; ix++) {
      index[1]=iy;
      index[2]=ix;
      if(data[iy*nx+ix]>0.0) overlay_map(index)=1.0;
    }
  }

}


const farray& MiViewRois::get_overlay_map(int current_z) const {
  Log<MiViewComp> odinlog("MiViewRois","get_overlay_map");


  if(clearall) {
    ODINLOG(odinlog,normalDebug) << "clearall" << STD_endl;
    overlay_map=0.0;
  }

  if(clearslice) {
    ODINLOG(odinlog,normalDebug) << "clearslice" << STD_endl;
    ODINLOG(odinlog,normalDebug) << "current_z=" << current_z << STD_endl;

    ndim nn=overlay_map.get_extent();
    ODINLOG(odinlog,normalDebug) << "nn=" << nn << STD_endl;

    if(current_z>=0 && current_z<int(nn[0])) {
      ndim index=nn;
      index[0]=current_z;
      int ny=nn[1];
      int nx=nn[2];
      for(int iy=0; iy<ny; iy++) {
        for(int ix=0; ix<nx; ix++) {
          index[1]=iy;
          index[2]=ix;
          overlay_map(index)=0.0;
        }
      }
    }
  }

  return overlay_map;
}


bool MiViewRois::write_overlay_map(const STD_string& filename, const Protocol& prot) {
  Data<float,3> tmpmap(get_overlay_map());
  return (tmpmap.autowrite(filename, FileWriteOpts(), &prot)>=0);
}


void MiViewRois::update() {
  Log<MiViewComp> odinlog("MiViewRois","update");

}
