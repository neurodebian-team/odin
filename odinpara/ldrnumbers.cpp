#include "ldrnumbers.h"
#include "ldrblock.h" // for UnitTest
#include <tjutils/tjtest.h>

///////////////////////////////////////////////////////////////

template<class T>
void LDRnumber<T>::common_init() {
  val=T(0);
  minval=0.0;
  maxval=0.0;
//  ParxEquiv pe=get_parx_equiv();
//  pe.type=TypeTraits::type2label(val);
//  set_parx_equiv(pe);
}

template<class T>
LDRnumber<T>::LDRnumber(T v,const STD_string& name)  {
  common_init();
  val=v;
  set_label(name);
}

template<class T>
LDRnumber<T>& LDRnumber<T>::operator = (const LDRnumber<T>& bi) {
  LDRbase::operator = (bi);
  val=bi.val;
  minval=bi.minval;
  maxval=bi.maxval;
  return *this;
}


template<class T>
STD_string LDRnumber<T>::printvalstring(const LDRserBase*) const {
  return TypeTraits::type2string(val);
}


template<class T>
bool LDRnumber<T>::parsevalstring(const STD_string& parstring, const LDRserBase* ser) {
  TypeTraits::string2type(parstring,val);
  return true;
}


template<class T>
STD_string LDRnumber<T>::get_parx_code(parxCodeType type) const {
#ifdef PARAVISION_PLUGIN
  Log<LDRcomp> odinlog(this,"get_parx_code");
  STD_string result=LDRbase::get_parx_code(type);

  if(type==parx_passval) {
    STD_string transformation;
    if (get_jdx_props().parx_equiv_offset) transformation+=ftos(get_jdx_props().parx_equiv_offset,_DEFAULT_DIGITS_,neverExp)+"+";
    if (get_jdx_props().parx_equiv_factor!=1.0) transformation+=ftos(get_jdx_props().parx_equiv_factor,_DEFAULT_DIGITS_,neverExp)+"*";
    result=get_jdx_props().parx_equiv_name+"="+transformation+get_label()+";\n";
  }

  return result;
#else
  return "";
#endif
}



///////////////////////////////////////////////////////////////////
// Instantiations

template class LDRnumber<int>;
template class LDRnumber<float>;
template class LDRnumber<double>;
template class LDRnumber<STD_complex>;


///////////////////////////////////////////////////////////////////

#ifndef NO_UNIT_TEST
class LDRintTest : public UnitTest {

 public:
  LDRintTest() : UnitTest("LDRint") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // testing LDRint
    LDRint testint(23,"testint");
    STD_string expected="##$testint=23\n";
    STD_string printed=testint.print();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRint::print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    LDRblock builtinblock;
    builtinblock.append(testint);
    builtinblock.parseblock("##TITLE=builtinblock\n##$testint=46\n##END=");
    if(int(testint)!=46) {
      ODINLOG(odinlog,errorLog) << "after builtinblock.parseblock(): for int " << int(testint) << "!=" << 46 << STD_endl;
      return false;
    }
    testint*=2;
    if(int(testint)!=92) {
      ODINLOG(odinlog,errorLog) << "LDRint *= " << int(testint) << "!=" << 92 << STD_endl;
      return false;
    }

    return true;
  }

};
void alloc_LDRintTest() {new LDRintTest();} // create test instance



class LDRcomplexTest : public UnitTest {

 public:
  LDRcomplexTest() : UnitTest("LDRcomplex") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // testing LDRcomplex
    LDRcomplex testcplx(STD_complex(1.2,3.4),"testcplx");
    STD_string expected="##$testcplx=1.20+3.40i\n";
    STD_string printed=testcplx.print();
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "LDRcomplex::print() failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }
    LDRblock cplxblock;
    cplxblock.append(testcplx);
    cplxblock.parseblock("##TITLE=cplxblock\n##testcplx=5.6+7.8i\n##END=");
    if(STD_complex(testcplx)!=STD_complex(5.6,7.8)) {
      ODINLOG(odinlog,errorLog) << "after cplxblock.parseblock(): for complex " << STD_complex(testcplx) << "!=" << STD_complex(5.6,7.8) << STD_endl;
      return false;
    }
    testcplx/=STD_complex(2.0);
    if(STD_complex(testcplx)!=STD_complex(2.8,3.9)) {
      ODINLOG(odinlog,errorLog) << "LDRcomplex /= " << STD_complex(testcplx) << "!=" << STD_complex(2.8,3.9) << STD_endl;
      return false;
    }

    return true;
  }

};
void alloc_LDRcomplexTest() {new LDRcomplexTest();} // create test instance


#endif
