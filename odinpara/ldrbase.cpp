#include "ldrbase.h"
#include "ldrtypes.h"
#include "ldrblock.h"
#include <tjutils/tjlog_code.h>


const char* LDRcomp::get_compName() {return "LDR";}
LOGGROUNDWORK(LDRcomp)


/////////////////////////////////////////////////

ArrayScale::ArrayScale(const STD_string& scalelabel, const STD_string& scaleunit, float scalemin, float scalemax, bool enablescale)
            : label(scalelabel), unit(scaleunit), minval(scalemin), maxval(scalemax), enable(enablescale) {
}

STD_string ArrayScale::get_label_with_unit() const {
  STD_string result(label);
  if(unit!="") result+=" ["+unit+"]";
  return result;
}

/////////////////////////////////////////////////

void PixmapProps::get_overlay_range(float& min, float& max) const {
  min=overlay_minval;
  max=overlay_maxval;
  if(overlay_minval==0.0 && overlay_maxval==0.0) {
    min=overlay_map.minvalue();
    max=overlay_map.maxvalue();
  }
}


/////////////////////////////////////////////////


LDRbase::LDRbase () :
 parmode(edit),
 filemode(include),
 id(-1) {
}


LDRbase::LDRbase(const LDRbase& ldr) : id(-1) {
  LDRbase::operator = (ldr);
}


LDRbase::~LDRbase() { // for some reason, making this virtual destructor inline on GCC4, causes a crash ...
  Log<LDRcomp> odinlog(this,"~LDRbase");
  ODINLOG(odinlog,normalDebug) << "called" << STD_endl;
}


LDRbase& LDRbase::operator = (const LDRbase& ldr) {
  Log<LDRcomp> odinlog(this,"LDRbase::operator = ");
  ListItem<LDRbase>::operator = (ldr);
  Labeled::operator = (ldr);
  jdx_props=ldr.jdx_props;
  parmode=ldr.parmode;
  filemode=ldr.filemode;
  description=ldr.description;
  unit=ldr.unit;
  cmdline_option=ldr.cmdline_option;
  return *this;
}


STD_string LDRbase::print(const LDRserBase& serializer) const {
  Log<LDRcomp> odinlog(this,"print");
  if(get_filemode()==exclude) return "";
  return serializer.get_prefix(*this)+serializer.escape_characters(printvalstring(&serializer))+serializer.get_postfix(*this);
}


STD_ostream& LDRbase::print2stream(STD_ostream& os, const LDRserBase& serializer) const {
  return os << serializer.escape_characters(printvalstring(&serializer));
}


int LDRbase::write(const STD_string &filename, const LDRserBase& serializer) const {
  LDRbase* cc=create_copy();
  LDRblock block;
  block.append(*cc);
  int retval=block.write(filename,serializer);
  delete cc;
  return retval;
}


bool LDRbase::parse(STD_string& parstring, const LDRserBase& serializer) {
  bool result=parsevalstring(
                serializer.deescape_characters(
                  serializer.extract_valstring(parstring)
                ),&serializer
              );
  serializer.remove_next_ldr(parstring);
  return result;
}


int LDRbase::load(const STD_string& filename, const LDRserBase& serializer) {
  LDRblock block;
  block.append(*this);
  return block.load(filename,serializer);
}


STD_string LDRbase::get_parx_code(parxCodeType type) const {
#ifdef PARAVISION_PLUGIN
  STD_string result;
  if(type==parx_def) result=get_parx_def_string(get_typeInfo(true),0); 
  if(type==parx_passval) result=get_jdx_props().parx_equiv_name+"="+get_label()+";\n";
  return result;
#else
  return "";
#endif
}


STD_string LDRbase::get_parx_def_string(const STD_string type, unsigned int dim) const {
#ifdef PARAVISION_PLUGIN
  STD_string retstring;

  STD_string typelabel(type);
  if(typelabel.find("bit")!=STD_string::npos) typelabel="int";

  retstring+=typelabel + " parameter {\n";
  retstring+="  display_name \""+STD_string(get_label())+ "\";\n";
//  retstring+="  relations method_relations;\n";
  retstring+="}  "+STD_string(get_label());
  retstring+=n_times("[]",dim);
  retstring+=";\n\n\n";

  return retstring;
#else
  return "";
#endif
}

