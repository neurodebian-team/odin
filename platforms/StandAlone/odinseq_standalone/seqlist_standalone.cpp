#include "seqlist_standalone.h"

void SeqListStandAlone::pre_event (eventContext& context, const RotMatrix* rotmatrix) const {
  if(rotmatrix) SeqStandAlone::current_rotmatrix=rotmatrix;
}

void SeqListStandAlone::post_event(eventContext& context, const RotMatrix* rotmatrix) const {
  if(rotmatrix) SeqStandAlone::current_rotmatrix=0;
}

void SeqListStandAlone::pre_itemevent (const SeqTreeObj* item, eventContext& context) const {
  if(context.action==seqRun && !context.noflush) new_plot_frame(context);
}

void SeqListStandAlone::post_itemevent(const SeqTreeObj* item, eventContext& context) const {
  if(context.action==seqRun && !context.noflush) flush_plot_frame(context);
}
