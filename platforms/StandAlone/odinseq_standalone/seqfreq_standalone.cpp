#include "seqfreq_standalone.h"

void SeqFreqChanStandAlone::prep_iteration(double current_frequency, double current_phase, double freqchan_duration) const {
  Log<SeqStandAlone> odinlog(this,"prep_iteration");
  ODINLOG(odinlog,normalDebug) << "current_frequency/current_phase=" << current_frequency << "/" << current_phase << STD_endl;
  freq_cache =current_frequency;
  phase_cache=current_phase;
}

void SeqFreqChanStandAlone::pre_event(eventContext& context,double starttime) const {
  current_rf_rec_freq =freq_cache;
  current_rf_rec_phase=phase_cache;
}



