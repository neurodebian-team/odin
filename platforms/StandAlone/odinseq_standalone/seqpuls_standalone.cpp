#include "seqpuls_standalone.h"

SeqPulsStandAlone::SeqPulsStandAlone(const SeqPulsStandAlone& sps) {
  set_label(sps.get_label());
  rf_energy=sps.rf_energy;
  current_pls=0;
}

double SeqPulsStandAlone::get_rf_energy() const {
  if(current_pls<rf_energy.size()) rf_energy[current_pls];
  return -1.0;
}

bool SeqPulsStandAlone::prep_driver(const cvector& wave, double pulsduration, double pulscenter, float b1max, float power, float flipangle, const fvector& flipscales, pulseType plstype) {
  Log<SeqStandAlone> odinlog(this,"prep_driver");


  fvector b1maxvals;
  if(flipscales.size()) {
    b1maxvals=b1max*flipscales;
  } else {
    b1maxvals.resize(1);
    b1maxvals[0]=b1max;
  }

  unsigned int n_flips=b1maxvals.size();
  ODINLOG(odinlog,normalDebug) << "n_flips=" << n_flips << STD_endl;

  unsigned int n=wave.size();
  double dt=secureDivision(pulsduration,n);

  B1re_curve.resize(n_flips);
  B1im_curve.resize(n_flips);

  rf_energy.resize(n_flips);

  fvector amp=amplitude(wave);
  amp=amp*amp;
  float ampsum=amp.sum();

  has_real=false;
  has_imag=false;

  relabel=STD_string(get_label())+"_re";
  imlabel=STD_string(get_label())+"_im";

  for(unsigned int iflp=0; iflp<n_flips; iflp++) {

    B1re_curve[iflp].label=relabel.c_str();
    B1im_curve[iflp].label=imlabel.c_str();
    B1re_curve[iflp].channel=B1re_plotchan;
    B1im_curve[iflp].channel=B1im_plotchan;

    B1re_curve[iflp].resize(n);
    B1im_curve[iflp].resize(n);


    for(unsigned int i=0; i<n; i++) {
      double timep=double(0.5+i)*dt;
      B1re_curve[iflp].x[i]=timep;
      B1im_curve[iflp].x[i]=timep;
      float re=b1maxvals[iflp]*wave[i].real();
      float im=b1maxvals[iflp]*wave[i].imag();
      B1re_curve[iflp].y[i]=re;
      B1im_curve[iflp].y[i]=im;
      if(re) has_real=true;
      if(im) has_imag=true;
    }
    if(n) {
      ODINLOG(odinlog,normalDebug) << "wave[" << n/2 << "]=" << wave[n/2] << STD_endl;
    }

    B1re_curve[iflp].marker=markType(excitation_marker+plstype);
    B1re_curve[iflp].marklabel=markLabel[B1re_curve[iflp].marker];
    B1re_curve[iflp].marker_x=pulscenter;

    rf_energy[iflp]=dt*b1maxvals[iflp]*b1maxvals[iflp]*ampsum;

    if(dump2console) {
      STD_cout << B1re_curve[iflp] << STD_endl;
      STD_cout << B1im_curve[iflp] << STD_endl;
    }
  }

  return true;
}

void SeqPulsStandAlone::event(eventContext& context, double start) const {
  Log<SeqStandAlone> odinlog(this,"event");
  ODINLOG(odinlog,normalDebug) << "start=" << start << STD_endl;
  if(has_real) append_curve2plot(start,&B1re_curve[current_pls],current_rf_rec_freq,current_rf_rec_phase);
  if(has_imag) append_curve2plot(start,&B1im_curve[current_pls],current_rf_rec_freq,current_rf_rec_phase);
}

bool SeqPulsStandAlone::prep_flipangle_iteration(unsigned int count) {
  current_pls=count;
  return true;
}

