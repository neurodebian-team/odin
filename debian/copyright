Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ODIN
Source: http://od1n.sourceforge.net

Files: *
Copyright: 2000-2015 Thies Jochimsen
           2000,2015 Michael von Mengershausen. Andreas Schaefer, Markus Koerber,
                     Robert Trampel, Enrico Reimer, Dirk Mueller, Torsten Schlumm
License: GPL-2+

Files: odindata/fileio_gzip.cpp
Comment: derived from the minigz-example by Jean-loup Gailly
         of the zlib distribution
Copyright: 1995-2005 Jean-loup Gailly and Mark Adler
License: as_IS
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
 .
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

Files: debian/*
Copyright: 2009-2016 Michael Hanke <michael.hanke@gmail.com>
License: GPL-2+

License: GPL-2+
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 .
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
