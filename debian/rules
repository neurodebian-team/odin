#!/usr/bin/make -f

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

export QT_SELECT := qt5

DCMMAJOR := $(shell readlink -f /usr/lib/libdcmimgle.so | cut -f3 -d.)

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- \
    --libdir="/usr/lib/$(DEB_HOST_MULTIARCH)/odin" \
    --with-pic \
    --enable-only-standalone-plugin \
    --enable-static \
    --enable-shared \
    --enable-niftisupport \
    --enable-vtksupport \
    --enable-dcmtksupport \
    --enable-pngsupport \

# do not install unittest suite
override_dh_auto_install:
	dh_auto_install
	rm $(CURDIR)/debian/tmp/usr/bin/odintestsuite

# there is also a 'test' target that doesn't do the right thing
override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	export DCMDICTPATH=`find /usr/share -name dicom.dic` ; \
	if [ "$${DCMDICTPATH}" = "" ] ; then \
	    echo "No package contains dicom.dic ... test failed" ; \
	    exit 1 ; \
	else \
	    $(MAKE) check ; \
	fi
endif

# shared libs in odin package a only for private use
override_dh_makeshlibs:
	dh_makeshlibs -n

# let dh_shlibdeps find the other private libs
# but it does not seem to work though...
override_dh_shlibdeps:
	dh_shlibdeps -l $(CURDIR)/debian/mitools/usr/lib/$(DEB_HOST_MULTIARCH)/odin

override_dh_clean:
	dh_clean
	find $(CURDIR) -name '*.loT' -delete

override_dh_install:
	dh_install
	find debian/tmp \( -name "*.desktop" -o -name "*.la" \) -delete
	# Protect PACKAGE_NAME from multiple definitions in case the
	# macro collides with other projects declaring it the same way.
	# This works around #733598 without breaking upstream
	# expectation that this file is available to end users (as
	# pointed out in #1079274).
	sed -i 's/#define \(PACKAGE_NAME\) ".*"/#ifndef \1\n&\n#endif/' \
		$(CURDIR)/debian/libodin-dev/usr/include/tjutils/config.h
