#include <qslider.h>

#include <odinpara/ldrtypes.h> // for LDRfileName

#include "float3d.h"


floatBox3D::floatBox3D(const float *data, float lowbound, float uppbound, long int nx, long int ny, long int nz,
                       bool disable_scale, int coarseFactor, QWidget *parent, const char *name,
                       const float *overlay_map, float lowbound_map, float uppbound_map, unsigned int nx_map, unsigned int ny_map, unsigned int nz_map, bool map_firescale, float map_rectsize,
                       bool colormap)
 : QGroupBox(name,parent) {
  Log<OdinQt> odinlog("floatBox3D","floatBox3D");

  data_cache=data;
  oneimagesize=nx*ny;
  nz_cache=nz;

  lowbound_cache=lowbound;
  uppbound_cache=uppbound;

  map_cache=0;
  onemapsize=0;

  lowbound_map_cache=lowbound_map;
  uppbound_map_cache=uppbound_map;
  rectsize_map_cache=map_rectsize;

  if(overlay_map) {
    if(nz==nz_map) {
      map_cache=overlay_map;
      onemapsize=nx_map*ny_map;
      ODINLOG(odinlog,normalDebug) << "map_cache/onemapsize/nz_map=" << map_cache << "/" << onemapsize << "/" << nz_map << STD_endl;
    } else {
      ODINLOG(odinlog,errorLog) << "Cannot handle overlay_map with nz(" << nz_map << ") differing from data's nz(" << nz << ")" << STD_endl;
    }
  }

  int nrow=1;
  if(nz>1) nrow++;

  int ncol=2;
  if(overlay_map) ncol++;

  grid=new GuiGridLayout( this, nrow, ncol);

  label = new floatLabel2D(data,lowbound,uppbound,nx,ny,disable_scale,coarseFactor,this,name,overlay_map,lowbound_map,uppbound_map,nx_map,ny_map,map_firescale,map_rectsize,colormap);
  grid->add_widget( label, 0, 0, GuiGridLayout::Default, 1, 2 );
  connect(label,SIGNAL(clicked(int,int)),this,SLOT(emitClicked(int,int)));
  connect(label,SIGNAL(newProfile(const float *, int, bool, int)),this,SLOT(emitNewProfile(const float *, int, bool, int)));
  connect(label,SIGNAL(newMask(const float *)),this,SLOT(emitNewMask(const float *)));

  maplegend=0;
  if(overlay_map) {
    maplegend = label->get_map_legend(this);
    if(maplegend) grid->add_widget( maplegend, 0, 2 );
  }


  zslider=0;
  zval=0;
  if(nz>1) {
    zslider=new GuiSlider(this, 0, nz-1, 1, 0, 1);
    connect(zslider->get_widget(), SIGNAL(valueChanged(int)), this, SLOT(changez(int)));
    grid->add_widget( zslider->get_widget(), 1, 0 );

    zval=new QLabel(this);
    grid->add_widget( zval, 1, 1 );

    // set size according to max num of digits
    zval->setMinimumWidth(_FONT_SIZE_*int(log10(nz-1)+1));
    zval->setNum(0);
  }


  mask3d=new float[nx*ny*nz];
  for(int i=0; i<nx*ny*nz; i++) mask3d[i]=0.0;

}

void floatBox3D::repaint_slice(int iz) const {
  label->refresh(data_cache+iz*oneimagesize,lowbound_cache,uppbound_cache);
  if(map_cache) label->refreshMap(map_cache+iz*onemapsize,lowbound_map_cache,uppbound_map_cache,rectsize_map_cache);
}



void floatBox3D::write_pixmap(const char* fname, const char* format, bool dump_all) const {
  if(dump_all) {

    LDRfileName dumpfname(fname);
    STD_string dumpprefix=dumpfname.get_dirname()+SEPARATOR_STR+dumpfname.get_basename_nosuffix();

    for(unsigned int iz=0; iz<nz_cache; iz++) {
      repaint_slice(iz);
      STD_string onefname(dumpprefix);
      if(nz_cache>1) onefname+=itos(iz,nz_cache-1);
      onefname+="."+::tolowerstr(format);
      label->write_pixmap( onefname.c_str(), format);
    }
    repaint_slice(get_current_z());
  } else {
    label->write_pixmap(fname,format);
  }
}

void floatBox3D::refresh(const float* data, float lowbound, float uppbound) {
  data_cache=data;
  label->refresh(data_cache+oneimagesize*get_current_z(),lowbound,uppbound);
  lowbound_cache=lowbound;
  uppbound_cache=uppbound;
}


void floatBox3D::changez(int iz) {
  Log<OdinQt> odinlog("floatBox3D","changez");
  repaint_slice(iz);
  repaint();
  if(zval) zval->setNum(iz);
}

int floatBox3D::get_current_z() const {
  if(zslider) return zslider->get_value();
  return 0;
}


floatBox3D::~floatBox3D() {
  if(zslider) delete zslider;
  if(zval) delete zval;
  if(maplegend) delete maplegend;
  delete label;
  delete grid;
  delete[] mask3d;
}




