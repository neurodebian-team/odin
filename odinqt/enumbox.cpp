#include <qcombobox.h> // for connecting signals

#include "enumbox.h"


enumBox::enumBox(const svector& items,QWidget *parent,const char *name,bool editButton,bool infoButton )
 : QGroupBox(name, parent ) {

  pb_edit=0;
  pb_info=0;

  unsigned int row=1;
  if(editButton) row++;
  if(infoButton) row++;

  grid=new GuiGridLayout( this, 1, row);

  row=0;
  cb = new GuiComboBox( this, items );
  grid->add_widget( cb->get_widget(), 0, row );
  connect( cb->get_widget(), SIGNAL(activated(int)),this, SLOT(emitNewVal(int)) );
  row++;

  if(editButton) {
    pb_edit = new GuiButton( this, this, SLOT(reportEditClicked()), "Edit" );
    grid->add_widget( pb_edit->get_widget(), 0, row, GuiGridLayout::VCenter );
    row++;
//    connect( pb_edit->get_widget(), SIGNAL(clicked()), SLOT(reportEditClicked()) );
  }

  if(infoButton) {
    pb_info = new GuiButton( this, this, SLOT(reportInfoClicked()),  "Info" );
    grid->add_widget( pb_info->get_widget(), 0, row, GuiGridLayout::VCenter );
    row++;
//    connect( pb_info->get_widget(), SIGNAL(clicked()), SLOT(reportInfoClicked()) );
  }
}

void enumBox::emitNewVal( int selected ) {
  old_val=selected;
  emit newVal(old_val);
}

void enumBox::reportEditClicked() {emit edit();}
void enumBox::reportInfoClicked() {emit info();}

void enumBox::setValue( int val ) {
  old_val=val;
  cb->set_current_item(val);
}

enumBox::~enumBox(){
  delete cb;
  if(pb_edit) delete pb_edit;
  if(pb_info) delete pb_info;
  delete grid;
}

