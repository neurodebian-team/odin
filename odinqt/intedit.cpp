#include <qslider.h>

#include "intedit.h"

#include <tjutils/tjstring.h> // for itos
#include <tjutils/tjcstd.h> // for strtol

intLineEdit::intLineEdit(int minValue, int maxValue, int value, QWidget *parent, const char *name, int width, int height ) {

  gle = new GuiLineEdit(parent, this, SLOT(emitSignal()), width, height);

   /* minValue & maxValue unused so far: */
  minValue=maxValue=0;

  set_value(value);
}

intLineEdit::~intLineEdit() {
  delete gle;
}

void intLineEdit::setintLineEditValue( int newValue ) {
  set_value(newValue);
}


void intLineEdit::emitSignal() {
  if(gle->is_modified()) {
    int value=atoi(gle->get_text());
    set_value(value);
    emit intLineEditValueChanged(value);
  }
}

void intLineEdit::set_value(int value) {
  gle->set_text( itos(value).c_str() );
}


//////////////////////////////////////////////////////////////



intLineBox::intLineBox(int value,QWidget *parent, const char *name )
 : QGroupBox(name,parent) {

  grid=new GuiGridLayout( this, 1, 1);

  le = new intLineEdit( 0, 0, value, this, "LineEdit", TEXTEDIT_WIDTH, TEXTEDIT_HEIGHT );

  grid->add_widget( le->get_widget(), 0, 0);

  connect(le,SIGNAL(intLineEditValueChanged( int )), this,SLOT(emitSignal( int )));
  connect(this,SIGNAL(SignalToChild( int )), le, SLOT(setintLineEditValue( int)));
}


void intLineBox::setintLineBoxValue( int newvalue ) {
  emit SignalToChild( newvalue );

}

void intLineBox::emitSignal( int newvalue ) {
  emit intLineBoxValueChanged(newvalue);
}


intLineBox::~intLineBox() {
  delete le;
  delete grid;
}


//////////////////////////////////////////////////////////////


intScientSlider::intScientSlider(int minValue, int maxValue, int Step, int value, QWidget *parent, const char *name )
 : QGroupBox(name, parent ) {

  grid=new GuiGridLayout( this, 1, 4);

  slider=new GuiSlider(this, minValue, maxValue, Step, value, int(((float)maxValue-(float)minValue)/20.0));

  le = new intLineEdit( minValue, maxValue, value, this, "LineEdit" , SLIDER_CELL_WIDTH, SLIDER_CELL_HEIGHT );

  grid->add_widget(slider->get_widget(), 0, 0, GuiGridLayout::Default, 1, 3 );
  grid->add_widget(le->get_widget(), 0, 3);

  // cross-wise connection of slider and line edit
  connect(slider->get_widget(), SIGNAL(valueChanged(int)), le, SLOT(setintLineEditValue(int)));
  connect(le, SIGNAL(intLineEditValueChanged(int)), slider->get_widget(), SLOT(setValue(int)));

  // emit signal whenever of one of both has been changed
  connect(slider->get_widget(),SIGNAL(valueChanged(int)), this,SLOT(emitSignal( int )));
  connect(le,SIGNAL(intLineEditValueChanged( int )), this,SLOT(emitSignal( int )));

}

void intScientSlider::setintScientSliderValue( int newvalue ) {
  slider->set_value(newvalue);
  le->setintLineEditValue(newvalue);
}

void intScientSlider::emitSignal( int newvalue ) {
  emit intScientSliderValueChanged(newvalue);
}

intScientSlider::~intScientSlider(){
  delete le;
  delete slider;
  delete grid;
}
