#include "boolbutton.h"

buttonBox::buttonBox(const char *text, QWidget *parent, const char *buttonlabel)
 : QGroupBox(buttonlabel,parent) {

  grid=new GuiGridLayout( this, 1, 1);
  gb=new GuiButton(this, this, SLOT(reportclicked()), text);

  grid->add_widget( gb->get_widget(), 0, 0, GuiGridLayout::VCenter);

//  connect( gb->get_widget(), SIGNAL(clicked()), SLOT(reportclicked()) );
}


buttonBox::buttonBox(const char *ontext,const char *offtext, bool initstate, QWidget *parent, const char *buttonlabel)
 : QGroupBox(buttonlabel,parent) {

  grid=new GuiGridLayout( this, 1, 1);
  gb=new GuiButton(this, this, SLOT(setButtonState()), ontext, offtext, initstate);

  grid->add_widget( gb->get_widget(), 0, 0, GuiGridLayout::VCenter);

//  connect( gb->get_widget(), SIGNAL(toggled(bool)), SLOT(setButtonState(bool)) );
}


void buttonBox::reportclicked() {
    emit  buttonClicked();
}

void buttonBox::setToggleState(bool state) {
  gb->set_toggled(state);
}

void buttonBox::setButtonState() {
  bool state=gb->is_on();
  gb->set_text(state);
  emit buttonToggled(state);
}

buttonBox::~buttonBox(){
  delete gb;
  delete grid;
}


