#include <qobject.h>
#include <qlistview.h>
#include <qpushbutton.h>
#include <qtoolbutton.h>
#include <qlineedit.h>

#include "odinqt.h"

#if QT_VERSION > 0x03FFFF
#define QT_VERSION_4
#else
#if QT_VERSION > 299
#define QT_VERSION_3
#else
#define QT_VERSION_PRE3
#endif
#endif



/**
  * This class dispatches calls of Qt slots to virtual member functions
  */
class SlotDispatcher : public QObject  {
   Q_OBJECT

 public:

  SlotDispatcher(GuiListView* glv, GuiListViewCallback* glv_cb) {
    glv_cache=glv;
    glv_cb_cache=glv_cb;
#ifdef QT_VERSION_4
    connect(glv->get_widget(), SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(qtwi_clicked(QTableWidgetItem*)) );
#else
    connect(glv->get_widget(), SIGNAL(clicked(QListViewItem*)), this, SLOT(qlvi_clicked(QListViewItem*)) );
#endif
  }


  SlotDispatcher(GuiToolButton* gtb, QObject* receiver, const char* member) {
    if(receiver) connect(gtb->qtb, SIGNAL(clicked()), receiver, member );
  }

  SlotDispatcher(GuiButton* gb, QObject* receiver, const char* member) {
    if(receiver) connect(gb->qpb, SIGNAL(clicked()), receiver, member );
  }

  SlotDispatcher(GuiLineEdit* gle, QObject* receiver, const char* member) {
    if(receiver) {
#ifdef QT_VERSION_4
      connect(gle->qle, SIGNAL(editingFinished()), receiver, member );
#else
#ifdef QT_VERSION_3
      connect(gle->qle, SIGNAL(lostFocus()), receiver, member ); // only available on Qt3
#endif
      connect(gle->qle, SIGNAL(returnPressed()), receiver, member );
#endif
    }
  }


 public slots:

  void qlvi_clicked(QListViewItem* item) {
#ifndef QT_VERSION_4
    Log<OdinQt> odinlog("SlotDispatcher","qlvi_clicked");
    ODINLOG(odinlog,normalDebug) << "item=" << item << STD_endl;
    if(!glv_cb_cache) return;
    GuiListItem* itemptr=0;
    GuiListItem(); // create listmap
    ODINLOG(odinlog,normalDebug) << "GuiListItem(); done" << STD_endl;
    STD_map<QListViewItem*,GuiListItem*>::iterator it=GuiListItem::listmap->find(item);
    if(it!=GuiListItem::listmap->end()) itemptr=it->second;
    ODINLOG(odinlog,normalDebug) << "itemptr=" << itemptr << STD_endl;
    glv_cb_cache->clicked(itemptr);
#endif
  }

  void qtwi_clicked(QTableWidgetItem* item) {
#ifdef QT_VERSION_4
    Log<OdinQt> odinlog("SlotDispatcher","qtwi_clicked");
    ODINLOG(odinlog,normalDebug) << "item=" << item << STD_endl;
    if(!glv_cb_cache) return;
    GuiListItem* itemptr=0;
    GuiListItem(); // create tablemap
    ODINLOG(odinlog,normalDebug) << "GuiListItem(); done" << STD_endl;
    STD_map<QTableWidgetItem*,GuiListItem*>::iterator it=GuiListItem::tablemap->find(item);
    if(it!=GuiListItem::tablemap->end()) itemptr=it->second;
    ODINLOG(odinlog,normalDebug) << "itemptr=" << itemptr << STD_endl;
    glv_cb_cache->clicked(itemptr);
#endif
  }


 private:
  void common_int() {
    glv_cache=0;
    glv_cb_cache=0;
  }

  GuiListView* glv_cache;
  GuiListViewCallback* glv_cb_cache;

};
