#include <qevent.h> // for mouse events

#include <qpixmap.h>
#include <qregion.h>

#include "float2d.h"
#include <tjutils/tjtools.h>
#include <tjutils/tjcstd.h>

#define LOGMAP_FACTOR 2

#define LEGEND_DIGITS 3

#define MAX_LOGMAP_HUE 70.0
#define MAX_MAP_HUE 270.0

#define X_ENUM 0
#define Y_ENUM 1

#define CROSSHAIR_SIZE 4

#define MAP_RECT_SIZE 0.8



void floatArray2pixbuff(unsigned char* imagebuff, const float* farray, int nx, int ny, int coarseFactor, int scalespace) {
  Log<OdinQt> odinlog("floatLabel2D","floatArray2pixbuff");

  int nx_aligned=((nx*coarseFactor+scalespace+3)/4)*4; // 32-bit aligned lines for QImage

  ODINLOG(odinlog,normalDebug) << "nx_aligned/nx/ny/coarseFactor/scalespace=" << nx_aligned << "/" << nx << "/" << ny << "/" << coarseFactor << "/" << scalespace << STD_endl;
  int i,j;

  // Draw data
  for (j = 0; j < ny; j++) {
    int jrev=ny-1-j;
    for (i = 0; i < nx; i++) {
      float floatval = farray[j * nx + i];
      if (floatval > 1.0) floatval = 1.0;
      if (floatval < 0.0) floatval = 0.0;
      unsigned char byteval = (unsigned char) (255.0 * floatval);
      for (int jj = 0; jj < coarseFactor; jj++) {
        for (int ii = 0; ii < coarseFactor; ii++) {
          imagebuff[(jrev * coarseFactor + jj) * nx_aligned  + (i * coarseFactor + ii)] = byteval;
        }
      }
    }

    // Draw scale
    float floatval = float(j)/float(ny-1);
    unsigned char byteval = (unsigned char) (255.0 * floatval + 0.5);
    for (i = (nx*coarseFactor); i < nx_aligned; i++) {
      for (int jj = 0; jj < coarseFactor; jj++) {
        imagebuff[(jrev * coarseFactor + jj) * nx_aligned  + i] = byteval;
      }
    }
  }

}




floatLabel2D::floatLabel2D(const float *data, float lowbound, float uppbound, unsigned int nx, unsigned int ny, bool disable_scale, unsigned int coarseFactor,
             QWidget *parent, const char *name,
             const float *overlay_map, float lowbound_map, float uppbound_map, unsigned int nx_map, unsigned int ny_map, bool map_firescale, float map_rectsize,
             bool colormap)
 : QLabel(parent ) {
  Log<OdinQt> odinlog("floatLabel2D","floatLabel2D");

  ODINLOG(odinlog,normalDebug) << "nx/ny/coarseFactor=" << nx << "/" << ny << "/" << coarseFactor << STD_endl;

  colormap_cache=colormap;

  pixmap=0;
  maplegend_pixmap=0;

  scale_size_cache=-1; // -1 means that it has to be initialized by get_scale_size()

  nx_cache=nx;
  ny_cache=ny;


  nx_map_cache=nx_map;
  ny_map_cache=ny_map;
  fire_map_cache=map_firescale;
  lowbound_map_cache=lowbound_map;
  uppbound_map_cache=uppbound_map;
  ODINLOG(odinlog,normalDebug) << "lowbound_map_cache/uppbound_map_cache=" << lowbound_map_cache << "/" << uppbound_map_cache << STD_endl;


  lowbound_cache=lowbound;
  uppbound_cache=uppbound;
  ODINLOG(odinlog,normalDebug) << "lowbound/uppbound=" << lowbound << "/" << uppbound << STD_endl;

  disable_scale_cache=disable_scale;

  roi_mask=new float[nx_cache*ny_cache];

  profile_x=new float[nx_cache];
  for(i=0;i<nx_cache;i++) profile_x[i]=0.0;
  profile_y=new float[ny_cache];
  for(i=0;i<ny_cache;i++) profile_y[i]=0.0;

  coarseFactor_cache=coarseFactor;

  int nx_aligned=((nx_cache*coarseFactor_cache+get_scale_size()+3)/4)*4; // 32-bit aligned lines for QImage
  int buffsize=nx_aligned*ny_cache*coarseFactor_cache;
  ODINLOG(odinlog,normalDebug) << "nx_aligned/ny/buffsize=" << nx_aligned << "/" << ny << "/" << buffsize << STD_endl;
  imagebuff=(unsigned char*)new int[buffsize/sizeof(int)+1]; // get 32-bit aligned data for QImage
  for(int i=0; i<buffsize; i++) imagebuff[i]=0;


  init_label(this);
  this->setFixedSize( (nx*coarseFactor+get_scale_size())+this->frameWidth()*2,ny*coarseFactor+this->frameWidth()*2 );

  connect(this,SIGNAL(clicked(int,int)),this,SLOT(drawcross(int,int)));


  refresh(data, lowbound, uppbound);
  refreshMap(overlay_map, lowbound_map, uppbound_map_cache, map_rectsize);
}


void  floatLabel2D::init_pixmap(bool clear) {
  Log<OdinQt> odinlog("floatLabel2D","init_pixmap");
  if(clear || !pixmap) {
    if(pixmap) delete pixmap;
    floatArray2pixbuff(imagebuff, data_cache, nx_cache, ny_cache, coarseFactor_cache, get_scale_size());
    GuiImage img( imagebuff, nx_cache*coarseFactor_cache+get_scale_size(), ny_cache*coarseFactor_cache, colormap_cache);
    pixmap = img.create_pixmap();

    GuiPainter gp(pixmap);
    draw_scale_text(gp, nx_cache*coarseFactor_cache, 3*_FONT_SIZE_/2, uppbound_cache);
    draw_scale_text(gp, nx_cache*coarseFactor_cache, ny_cache*coarseFactor_cache-_FONT_SIZE_/2, lowbound_cache);
    gp.end();

  }
}


void  floatLabel2D::set_pixmap() {
  Log<OdinQt> odinlog("floatLabel2D","set_pixmap");
//    bitBlt( this, 2, 2, pixmap );
  ODINLOG(odinlog,normalDebug) << "pixmap=" << (void*)pixmap << STD_endl;
  this->setPixmap(*pixmap);
}


int floatLabel2D::scale_width(float lowbound, float uppbound) {
  Log<OdinQt> odinlog("floatLabel2D","scale_width");
  int low_length=ftos(lowbound,LEGEND_DIGITS).length();
  int upp_length=ftos(uppbound,LEGEND_DIGITS).length();
  ODINLOG(odinlog,normalDebug) << "low_length/upp_length=" << low_length << "/" << upp_length << STD_endl;

  return STD_max(low_length,upp_length)*_FONT_SIZE_;
}


void floatLabel2D::draw_text(GuiPainter& gp, int xpos, int ypos, const char* txt) const {
  gp.drawText(xpos+1,ypos+1,txt,"Black");
  gp.drawText(xpos,  ypos,  txt,"White");
}

void floatLabel2D::draw_scale_text(GuiPainter& gp, int xpos, int ypos, float val) const {
  draw_text(gp, xpos, ypos, ftos(val,LEGEND_DIGITS).c_str());
}




void floatLabel2D::refresh(const float* data, float lowbound, float uppbound) {
  data_cache=data;
  lowbound_cache=lowbound;
  uppbound_cache=uppbound;

  init_pixmap();
  set_pixmap();
}


void  floatLabel2D::write_pixmap(const char* fname, const char* format) const {
  if(pixmap && fname)  pixmap->save (fname,toupperstr(format).c_str());
}


int floatLabel2D::get_map_hue(float relval) const {
  float hue_factor=1.0;
  if(fire_map_cache) hue_factor=0.25;
  else relval=1.0-relval;
  relval=STD_max(float(0.0),relval);
  relval=STD_min(float(1.0),relval);
  
  if (fire_map_cache) { // fmri coloring
    // adjust balance between red, orange and yellow ... more red, less orange
    if (relval < 0.25) // more red
      relval = 0.0;
    else if ((relval > 0.6) && (relval < 0.7)) // small transition orange to yellow
      relval = (0.7 - 7.0 / 15.0) / 0.1 * (relval - 0.6) + 7.0 / 15.0;
    else if (relval > 0.7)
      relval = relval;
    else relval = (relval - 0.25) * (4.0 / 3.0);
  }

  return MAX_MAP_HUE*hue_factor*relval;
}

int floatLabel2D::get_map_value(float relval) const {
  if ((relval < 0.4) && (fire_map_cache))  // darken lower values -> dark red
    return 255-255*(0.4 - relval);
  else return 255;
}

int floatLabel2D::get_map_saturation(float relval) const {
  if ((relval > 0.8) && (fire_map_cache))  // remove saturation for highest values -> white color
    return 255-255*(relval-0.8)*5;
  else return 255;
}

int floatLabel2D::get_scale_size() const {
  if(disable_scale_cache) return 0;
  if(scale_size_cache<0) scale_size_cache=scale_width(lowbound_cache, uppbound_cache);
  return scale_size_cache;
}

void  floatLabel2D::write_legend(const char* fname, const char* format) const {
  Log<OdinQt> odinlog("floatLabel2D","write_legend");

  int width=scale_width(lowbound_cache, uppbound_cache);
  int nx_aligned_scale=((width+3)/4)*4; // 32-bit aligned lines for QImage
  int buffsize_scale=nx_aligned_scale*ny_cache*coarseFactor_cache;
  ODINLOG(odinlog,normalDebug) << "width/nx_aligned_scale/buffsize_scale=" << width << "/" << nx_aligned_scale << "/" << buffsize_scale << STD_endl;
  unsigned char* imagebuff_scale=(unsigned char*)new int[buffsize_scale/sizeof(int)+1]; // get 32-bit aligned data for QImage
  for(int i=0; i<buffsize_scale; i++) imagebuff_scale[i]=0;

  floatArray2pixbuff(imagebuff_scale, 0, 0, ny_cache, coarseFactor_cache, width); // draw only scale
  GuiImage legend_img( imagebuff_scale, width, ny_cache*coarseFactor_cache, colormap_cache);

  QPixmap* legend_pixmap=legend_img.create_pixmap();

  GuiPainter legend_gp(legend_pixmap);
  draw_scale_text(legend_gp, 0, 3*_FONT_SIZE_/2, uppbound_cache);
  draw_scale_text(legend_gp, 0, ny_cache*coarseFactor_cache-_FONT_SIZE_/2, lowbound_cache);
  legend_gp.end();

  legend_pixmap->save(fname, toupperstr(format).c_str());

  delete legend_pixmap;
  delete[] imagebuff_scale;
}


QLabel* floatLabel2D::get_map_legend(QWidget *parent) const {

  QLabel* result=new QLabel(parent);

  int width=6*_FONT_SIZE_;
  int height=ny_cache*coarseFactor_cache;
  maplegend_pixmap = new QPixmap(width,height);
  GuiPainter *maplegend_painter = new GuiPainter(maplegend_pixmap);

  QColor qc;
  QColor qc_txt("Black");

  for(int iy=0; iy<height; iy++) {
    float relval = 1.0-float(iy)/float(height);
    qc.setHsv(get_map_hue(relval),get_map_saturation(relval),get_map_value(relval));
    maplegend_painter->fillRect (0,iy,width,1,qc);
    draw_text(*maplegend_painter, 0, 1.5*_FONT_SIZE_, ftos(uppbound_map_cache,LEGEND_DIGITS).c_str());
    draw_text(*maplegend_painter, 0, ny_cache*coarseFactor_cache-_FONT_SIZE_/2, ftos(lowbound_map_cache,LEGEND_DIGITS).c_str());
  }

  result->setPixmap(*maplegend_pixmap);
  return result;
}


void  floatLabel2D::write_map_legend(const char* fname, const char* format) const {
  if(maplegend_pixmap && fname)  maplegend_pixmap->save(fname, toupperstr(format).c_str());
}



void floatLabel2D::refreshMap(const float* map, float map_lowbound, float map_uppbound, float map_rectsize) {
  Log<OdinQt> odinlog("floatLabel2D","refreshMap");

  ODINLOG(odinlog,normalDebug) << "map=" << (void*)map << STD_endl;
  ODINLOG(odinlog,normalDebug) << "map_lowbound/map_uppbound=" << map_lowbound << "/" << map_uppbound << STD_endl;

  if(!map) return;

  init_pixmap();
  GuiPainter *painter = new GuiPainter(pixmap);

  float regridfactor_x=float(nx_cache)/float(nx_map_cache);
  float regridfactor_y=float(ny_cache)/float(ny_map_cache);

  if(map_rectsize<0.1) map_rectsize=0.1;
  if(map_rectsize>1.0) map_rectsize=1.0;


  int width =int(map_rectsize*float(coarseFactor_cache)*regridfactor_x+0.5); if(width<=0)  width=1;
  int height=int(map_rectsize*float(coarseFactor_cache)*regridfactor_y+0.5); if(height<=0) height=1;

  ODINLOG(odinlog,normalDebug) << "regridfactor_x/regridfactor_y/width/height=" << regridfactor_x << "/" << regridfactor_y << "/" << width << "/" << height << STD_endl;

  QColor qc;

  for(unsigned int iym=0; iym<ny_map_cache; iym++) {
    for(unsigned int ixm=0; ixm<nx_map_cache; ixm++) {

      float mapval=map[iym*nx_map_cache+ixm];

      if(mapval>map_lowbound && mapval<=map_uppbound) {
        float relval=secureDivision(mapval-map_lowbound,map_uppbound-map_lowbound);
        qc.setHsv(get_map_hue(relval),get_map_saturation(relval),get_map_value(relval));

        int lx=int(float(ixm)*regridfactor_x*float(coarseFactor_cache)+0.5);
        int ly=int(float(ny_map_cache-1-iym)*regridfactor_y*float(coarseFactor_cache)+0.5);

        painter->fillRect (lx,ly,width,height,qc);
      }
    }
  }


  painter->end();
  set_pixmap();
  delete painter;
}


void floatLabel2D::mousePressEvent(QMouseEvent *e) {
  Log<OdinQt> odinlog("floatLabel2D","mousePressEvent");
  if ( left_button(e,false) ) {
    ODINLOG(odinlog,normalDebug) << "left_button" << STD_endl;
    roi_polygon.clear();
    roi_painter = new GuiPainter(pixmap);
    roi_painter->moveTo(e->x(),e->y());
    mouse_moved=false;
  }

  if( middle_button(e,false) ) {
    ODINLOG(odinlog,normalDebug) << "middle_button" << STD_endl;
    drawprofil(labelxpos2xpos(e->x()),X_ENUM);
  }

  if( right_button(e,false)  ) {
    ODINLOG(odinlog,normalDebug) << "right_button" << STD_endl;
    drawprofil(labelypos2ypos(e->y()),Y_ENUM);
  }
}


void floatLabel2D::mouseReleaseEvent (QMouseEvent *e) {
  Log<OdinQt> odinlog("floatLabel2D","mouseReleaseEvent");
  if ( left_button(e,false) ) {
    ODINLOG(odinlog,normalDebug) << "Qt::LeftButton used" << STD_endl;
    roi_painter->end();
    delete roi_painter;
    if(mouse_moved) {
      ODINLOG(odinlog,normalDebug) << "calling drawroi()" << STD_endl;
      drawroi();
    } else {
      int xcross=labelxpos2xpos(e->x());
      int ycross=labelypos2ypos(e->y());
      if(xcross>=0 && xcross<int(get_nx()) && ycross>=0 && ycross<int(get_ny())) emit clicked(xcross,ycross);
      newMask(0);
    }
  } else newMask(0);
}


void floatLabel2D::mouseMoveEvent( QMouseEvent *e) {
  if ( left_button(e,true) ) {
    roi_polygon.push_back(QPoint(e->x(),e->y()));
    roi_painter->lineTo(e->x(),e->y());
    roi_painter->repaint(this);
    mouse_moved=true;
  }
}


int floatLabel2D::check_range(int val, int min, int max) {
  int result=val;
  if(result<min) result=min;
  if(result>=max) result=max-1;
  return result;
}


int floatLabel2D::xpos2labelxpos(int pos) {
  int result=int((float(pos)+0.5)*float(coarseFactor_cache));
  return result;
//  return check_range(result, 0, nx_cache*coarseFactor_cache);  // does not work with out-of-FOV slices in geoedit
}

int floatLabel2D::ypos2labelypos(int pos) {
  int result=int((float(ny_cache)-1.0-float(pos)+0.5)*float(coarseFactor_cache));
  return result;
//  return check_range(result, 0, ny_cache*coarseFactor_cache);  // does not work with out-of-FOV slices in geoedit
}

int floatLabel2D::labelxpos2xpos(int pos) {
  int result=(pos/coarseFactor_cache);
  return check_range(result, 0, nx_cache);
}

int floatLabel2D::labelypos2ypos(int pos) {
  int result=(ny_cache-1-pos/coarseFactor_cache);
  return check_range(result, 0, ny_cache);
}

int floatLabel2D::xypos2index(int xpos,int ypos) {
  int result=ypos*nx_cache+xpos;
  return check_range(result, 0, nx_cache*ny_cache);
}




void floatLabel2D::drawprofil(int clickpos,int direction) {
  Log<OdinQt> odinlog("floatLabel2D","drawprofil");
  unsigned int j;
  int ix,iy,profpos;

  init_pixmap();

  GuiPainter *painter = new GuiPainter(pixmap);

  painter->setPen("Green");
  switch (direction) {
    case X_ENUM: painter->moveTo(xpos2labelxpos(clickpos),0);painter->lineTo(xpos2labelxpos(clickpos),ny_cache*coarseFactor_cache); break;
    case Y_ENUM: painter->moveTo(0,ypos2labelypos(clickpos));painter->lineTo(nx_cache*coarseFactor_cache,ypos2labelypos(clickpos)); break;
  }
  ODINLOG(odinlog,normalDebug) << "moveTo done" << STD_endl;


  painter->setPen("Red");
  switch (direction) {
    case X_ENUM:
      ODINLOG(odinlog,normalDebug) << "X point" << STD_endl;
      profpos=int((float)(nx_cache-1)*data_cache[xypos2index(clickpos,0)]);
      ix=xpos2labelxpos(profpos);
      iy=ypos2labelypos(0);
      painter->moveTo(ix,iy);
      if(ny_cache) profile_y[0]=data_cache[xypos2index(clickpos,0)];
      for(j=1; j<ny_cache; j++) {
        profile_y[j]=data_cache[xypos2index(clickpos,j)];
        profpos=int((float)(nx_cache-1)*profile_y[j]);
        ix=xpos2labelxpos(profpos);
        iy=ypos2labelypos(j);
        painter->lineTo(ix,iy);
      }
      emit newProfile(profile_y,ny_cache,false,clickpos);
    break;

    case Y_ENUM:
      ODINLOG(odinlog,normalDebug) << "Y point" << STD_endl;
      ix=xpos2labelxpos(0);
      profpos=int((float)(ny_cache-1)*data_cache[xypos2index(0,clickpos)]);
      iy=ypos2labelypos(profpos);
      ODINLOG(odinlog,normalDebug) << "profpos/iy=" << profpos << "/" << iy << STD_endl;
      painter->moveTo(ix,iy);
      if(nx_cache) profile_x[0]=data_cache[xypos2index(0,clickpos)];
      for(j=1; j<nx_cache; j++) {
        profile_x[j]=data_cache[xypos2index(j,clickpos)];
        ODINLOG(odinlog,normalDebug) << "profile_x[" << j << "]=" << profile_x[j] << STD_endl;
        profpos=int((float)(ny_cache-1)*profile_x[j]);
        ix=xpos2labelxpos(j);
        iy=ypos2labelypos(profpos);
        ODINLOG(odinlog,normalDebug) << "profpos/ix/iy=" << profpos << "/" << ix << "/" <<  iy << STD_endl;
        painter->lineTo(ix,iy);
      }
      emit newProfile(profile_x,nx_cache,true,clickpos);
    break;
  }
  painter->end();
  ODINLOG(odinlog,normalDebug) << "painter done" << STD_endl;

  set_pixmap();
  delete painter;
}


void floatLabel2D::drawcross(int xpos,int ypos) {
  Log<OdinQt> odinlog("floatLabel2D","drawcross");

  ODINLOG(odinlog,normalDebug) << "xpos/ypos=" << xpos << "/" << ypos << STD_endl;

  // position will be centered on voxel by the transformation functions
  int centerx=xpos2labelxpos(xpos);
  int centery=ypos2labelypos(ypos);
  ODINLOG(odinlog,normalDebug) << "centerx/centery=" << centerx << "/" << centery << STD_endl;

  init_pixmap();

  GuiPainter *painter = new GuiPainter(pixmap);

  painter->setPen(_ARRAY_SELECTION_COLOR_);
  painter->moveTo(centerx-CROSSHAIR_SIZE,centery);
  painter->lineTo(centerx+CROSSHAIR_SIZE,centery);

  painter->moveTo(centerx,centery-CROSSHAIR_SIZE);
  painter->lineTo(centerx,centery+CROSSHAIR_SIZE);

  painter->end();

  set_pixmap();
  delete painter;
}


void floatLabel2D::drawroi() {
  Log<OdinQt> odinlog("floatLabel2D","mouseReleaseEvent");

  init_pixmap();

  GuiPainter *painter = new GuiPainter(pixmap);

  QRegion* rgn=painter->draw_region(roi_polygon);

  painter->end();

  set_pixmap();
  delete painter;

  if(rgn) {
    for(unsigned int iy=0; iy<ny_cache; iy++) {
      for(unsigned int ix=0; ix<nx_cache; ix++) {
        if( rgn->contains ( QPoint(xpos2labelxpos(ix),ypos2labelypos(iy)) ) ) roi_mask[iy*nx_cache+ix]=1.0;
        else roi_mask[iy*nx_cache+ix]=0.0;
      }
    }

//    for(unsigned int i=0; i<(nx_cache*ny_cache); i++) roi_mask[i]=secureDivision(roi_mask[i],double(npoints));

    delete rgn;

    ODINLOG(odinlog,normalDebug) << "emitting newMask" << STD_endl;
    emit newMask(roi_mask);
  }

}


floatLabel2D::~floatLabel2D(){
  delete[] imagebuff;
//  delete[] data_cache;
  delete[] profile_x;
  delete[] profile_y;
  delete[] roi_mask;
}
