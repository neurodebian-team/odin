#include "pulsar.h"

PulsarMain::PulsarMain() {
  Log<PulsarComp> odinlog("PulsarMain","PulsarMain(...)");

  view=new PulsarView(GuiMainWindow::get_widget());
  connect(view,SIGNAL(done()),this,SLOT(slotFileDone()));
  connect(view,SIGNAL(newCaption(const char*)),this,SLOT(changeCaption(const char*)));
  view->initPulsarView();
  ODINLOG(odinlog,normalDebug) << "initView() done" << STD_endl;

  initMenuBar();
  ODINLOG(odinlog,normalDebug) << "initMenuBar() done" << STD_endl;

  GuiMainWindow::show(view);
}

void PulsarMain::initMenuBar() {
  Log<PulsarComp> odinlog("PulsarMain","initMenuBar");

  fileMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  fileMenu->insert_item("&New", view, SLOT(slotFileNew()), Qt::CTRL+Qt::Key_N);
  fileMenu->insert_item("&Open...", view, SLOT(slotFileOpen()), Qt::CTRL+Qt::Key_O);
  fileMenu->insert_separator();
  fileMenu->insert_item("&Save", view, SLOT(slotFileSave()), Qt::CTRL+Qt::Key_S);
  fileMenu->insert_item("Save as...", view, SLOT(slotFileSaveAs()), 0);
  fileMenu->insert_separator();
  fileMenu->insert_item("&Quit", this, SLOT(slotFileQuit()), Qt::CTRL+Qt::Key_Q);

  exportMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  exportMenu->insert_item("Export Bruker RF-Pulse", view, SLOT(writeBrukerRf()));
  exportMenu->insert_separator();
  exportMenu->insert_item("Export ASCII RF-Amplitude", view, SLOT(exportRfampASCII()));
  exportMenu->insert_item("Export ASCII RF-Phase", view, SLOT(exportRfphaASCII()));
  exportMenu->insert_item("Export ASCII Gx", view, SLOT(exportGxASCII()));
  exportMenu->insert_item("Export ASCII Gy", view, SLOT(exportGyASCII()));
  exportMenu->insert_item("Export ASCII Gz", view, SLOT(exportGzASCII()));
  exportMenu->insert_separator();
  exportMenu->insert_item("Export ASCII Magnetization Amplitide", view, SLOT(exportMampASCII()));
  exportMenu->insert_item("Export ASCII Magnetization Phase", view, SLOT(exportMphaASCII()));
  exportMenu->insert_item("Export ASCII z-Magnetization", view, SLOT(exportMzASCII()));
  exportMenu->insert_separator();
  exportMenu->insert_item("Export JCAMP-DX Pulse", view, SLOT(exportPulseLDR()));
  exportMenu->insert_item("Export JCAMP-DX Magnetization", view, SLOT(exportMagLDR()));

  settingsMenu = new GuiPopupMenu(GuiMainWindow::get_widget());
  settingsMenu->insert_item("&System Configuration ...", view, SLOT(edit_systemInfo()),0);

  helpMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  helpMenu->insert_item("About...", this, SLOT(slotHelpAbout()), 0);


  GuiMainWindow::insert_menu("&File",     fileMenu);
  GuiMainWindow::insert_menu("Ex&port",   exportMenu);
  GuiMainWindow::insert_menu("&Settings", settingsMenu);
  GuiMainWindow::insert_menu_separator();
  GuiMainWindow::insert_menu("&Help",     helpMenu);
}

bool PulsarMain::queryExit() {
  return(message_question("Do your really want to quit?", "Quit...", GuiMainWindow::get_widget(), true));
}

void PulsarMain::slotFileDone() {
  view->slotFileSave();
  GuiApplication::quit();
}

void PulsarMain::slotFileQuit() {
  if(queryExit()) {
    GuiApplication::quit();
  }
}


void PulsarMain::slotHelpAbout() {
  message_question(IDS_PULSAR_ABOUT, "About...", GuiMainWindow::get_widget());
}


void PulsarMain::changeCaption(const char* text) {
  GuiMainWindow::set_caption((STD_string("Pulsar " VERSION) + " - " + text).c_str());
}





