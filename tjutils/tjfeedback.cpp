#include "tjfeedback.h"
#include "tjtools.h"


/////////////////////////////////////////////////////////////////////////////


bool ProgressMeter::increase_counter(const char* subj) {
  MutexLock lock(mutex);
  display->increase(subj);
  return display->refresh();
}

bool ProgressMeter::refresh_display() {
  MutexLock lock(mutex);
  return display->refresh();
}

ProgressMeter& ProgressMeter::new_task(unsigned int total_steps, const char* txt) {
  MutexLock lock(mutex);
  if(display) display->init(total_steps,txt);
  return *this;
}


/////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMDLINE


void ProgressDisplayConsole::init(unsigned int nsteps, const char* txt) {
  counter=0;
  old_perc=0;
  nsteps_cache=nsteps;
  done=false;
  if(txt) STD_cout << txt << " " << STD_flush;
}


void ProgressDisplayConsole::increase(const char* subj) {
  if(done) return;
  counter++;
  unsigned int perc=(unsigned int)(100.0*secureDivision(counter,nsteps_cache));
  if(perc>old_perc) {
    if(perc>=100) {
      STD_cout << "done" << STD_endl;
      done=true;
    } else {
      if(!(perc%10)) STD_cout << perc << "%" << STD_flush;
      else if(!(perc%2)) STD_cout << "." << STD_flush;
    }
    old_perc=perc;
  }
}

#endif
