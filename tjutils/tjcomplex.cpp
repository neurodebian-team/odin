#include "tjcomplex.h"
#include "tjtest.h"
#include "tjlog.h"

STD_string ctos (const STD_complex& z) {
  STD_string s;
  s=ftos(z.real());
  if(z.imag()>=0.0) s+="+";
  s+=ftos(z.imag())+"i";
  return s;
}

STD_complex stoc(const STD_string& s) {
  char value[250];
  unsigned int index=0,i=0;

  STD_string tt(s);
  tt=replaceStr(tt,"e-","m");
  tt=replaceStr(tt,"E-","m");
  tt=replaceStr(tt,"e+","p");
  tt=replaceStr(tt,"E+","p");
  tt=replaceStr(tt,"e","p");
  tt=replaceStr(tt,"E","p");

  while(index<249 && tt[index] != '+' && tt[index] != '-' && tt[index] != '.'
                                     && (tt[index] < '0' || tt[index] > '9') ) index++;

  if (tt[index] == '+' || tt[index] == '-') {
    value[0]=tt[index];
    i=1;index++;
  } else i=0;
  while(tt[index] != '+' && tt[index] != '-' && index<249) {
    value[i]=tt[index];
    index++; i++;
  } value[i]='\0';
  STD_string valstr_re(value);
  valstr_re=replaceStr(valstr_re,"m","e-");
  valstr_re=replaceStr(valstr_re,"p","e+");
  float val_re=atof(valstr_re.c_str()); i=0;
  while(tt[index] != 'i' && tt[index] != 'I' && index<249) {
    value[i]=tt[index];
    index++; i++;
  } value[i]='\0';
  STD_string valstr_im(value);
  valstr_im=replaceStr(valstr_im,"m","e-");
  valstr_im=replaceStr(valstr_im,"p","e+");

  float val_im=atof(valstr_im.c_str()); i=0;

  return STD_complex(val_re,val_im);
}


#ifndef NO_UNIT_TEST

class ComplexTest : public UnitTest {

 public:
  ComplexTest() : UnitTest("complex") {}

 private:

  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // making sure complex type contains only re/im member
    int expected_size=2*sizeof(float);
    int complex_size=sizeof(STD_complex);
    if(expected_size!=complex_size) {
      ODINLOG(odinlog,errorLog) << "complex type has NOT twice the size of float" << STD_endl;
      return false;
    }

    // check memory alignment
    float re=1.2;
    float im=3.4;
    STD_complex c(re,im);
    float* f=(float*)&c;
    if(f[0]!=re || f[1]!=im) {
      ODINLOG(odinlog,errorLog) << "wrong alignment of complex type" << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_ComplexTest() {new ComplexTest();} // create test instance

#endif

