#include "tjlist.h"
#include "tjlist_code.h"
#include "tjtest.h"
#include "tjlog_code.h"

const char* ListComponent::get_compName() {return "List";}
LOGGROUNDWORK(ListComponent)


#ifndef NO_UNIT_TEST
class ListTest : public UnitTest {

 public:
  ListTest() : UnitTest(ListComponent::get_compName()) {}

 private:

  class StrItem : public ListItem<StrItem>, public STD_string {};

  typedef List<StrItem,StrItem*,StrItem&> StrList;



  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    StrItem* item0=new StrItem;
    item0->STD_string::operator = ("item0");

    StrItem* item1=new StrItem;
    item1->STD_string::operator = ("item1");

    StrItem* item2=new StrItem;
    item2->STD_string::operator = ("item2");

    StrList* slist=new StrList;

    slist->append(*item0);
    slist->append(*item1);
    slist->append(*item2);

    if(slist->size()!=3) {
      ODINLOG(odinlog,errorLog) << "size()!=3" << STD_endl;
      return false;
    }

    int index=0;
    for(StrList::constiter it=slist->get_const_begin(); it!=slist->get_const_end(); ++it) {
      STD_string expected="item"+itos(index);
      if((**it)!=expected) {
        ODINLOG(odinlog,errorLog) << "expected=" << expected << STD_endl;
        return false;
      }
      index++;
    }

    delete item0;
    if(slist->size()!=2) {
      ODINLOG(odinlog,errorLog) << "size()!=2" << STD_endl;
      return false;
    }

    delete item1;
    if(slist->size()!=1) {
      ODINLOG(odinlog,errorLog) << "size()!=1" << STD_endl;
      return false;
    }

    if(item2->numof_references()!=1) {
      ODINLOG(odinlog,errorLog) << "references(pre)=" << item2->numof_references() << STD_endl;
      return false;
    }

    delete slist;

    if(item2->numof_references()!=0) {
      ODINLOG(odinlog,errorLog) << "references(post)=" << item2->numof_references() << STD_endl;
      return false;
    }

    delete item2;

    return true;
  }

};

void alloc_ListTest() {new ListTest();} // create test instance
#endif


