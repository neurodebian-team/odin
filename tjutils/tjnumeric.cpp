#include "tjnumeric.h"
#include "tjvector.h"
#include "tjtest.h"
#include "tjlog_code.h"


class NumericsComp {
 public:
  static const char* get_compName() {return "numerics";}
};
LOGGROUNDWORK(NumericsComp)


///////////////////////////////////////////////////////////////////


// copy & pasted from the GSL library
int solve_cubic (double a, double b, double c,
                      double *x0, double *x1, double *x2)
{
  double q = (a * a - 3 * b);
  double r = (2 * a * a * a - 9 * a * b + 27 * c);

  double Q = q / 9;
  double R = r / 54;

  double Q3 = Q * Q * Q;
  double R2 = R * R;

  double CR2 = 729 * r * r;
  double CQ3 = 2916 * q * q * q;

  if (R == 0 && Q == 0)
    {
      *x0 = - a / 3 ;
      *x1 = - a / 3 ;
      *x2 = - a / 3 ;
      return 3 ;
    }
  else if (CR2 == CQ3)
    {
      /* this test is actually R2 == Q3, written in a form suitable
         for exact computation with integers */

      /* Due to finite precision some double roots may be missed, and
         considered to be a pair of complex roots z = x +/- epsilon i
         close to the real axis. */

      double sqrtQ = sqrt (Q);

      if (R > 0)
        {
          *x0 = -2 * sqrtQ  - a / 3;
          *x1 = sqrtQ - a / 3;
          *x2 = sqrtQ - a / 3;
        }
      else
        {
          *x0 = - sqrtQ  - a / 3;
          *x1 = - sqrtQ - a / 3;
          *x2 = 2 * sqrtQ - a / 3;
        }
      return 3 ;
    }
  else if (CR2 < CQ3) /* equivalent to R2 < Q3 */
    {
      double sqrtQ = sqrt (Q);
      double sqrtQ3 = sqrtQ * sqrtQ * sqrtQ;
      double theta = acos (R / sqrtQ3);
      double norm = -2 * sqrtQ;
      *x0 = norm * cos (theta / 3) - a / 3;
      *x1 = norm * cos ((theta + 2.0 * PII) / 3) - a / 3;
      *x2 = norm * cos ((theta - 2.0 * PII) / 3) - a / 3;

      /* Sort *x0, *x1, *x2 into increasing order */

      if (*x0 > *x1)
        STD_swap(*x0, *x1) ;

      if (*x1 > *x2)
        {
          STD_swap(*x1, *x2) ;

          if (*x0 > *x1)
            STD_swap(*x0, *x1) ;
        }

      return 3;
    }
  else
    {
      double sgnR = (R >= 0 ? 1 : -1);
      double A = -sgnR * pow (fabs (R) + sqrt (R2 - Q3), 1.0/3.0);
      double B = Q / A ;
      *x0 = A + B - a / 3;
      return 1;
    }
}

////////////////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_LIBGSL
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#endif


RandomDist::RandomDist() {
#ifdef HAVE_LIBGSL
  rng = (gsl_rng*)gsl_rng_alloc(gsl_rng_tt800);
//  rng = (gsl_rng*)gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set ((gsl_rng*)rng, time(NULL));
#endif
}

RandomDist::~RandomDist() {
#ifdef HAVE_LIBGSL
  gsl_rng_free((gsl_rng*)rng);
#endif
}

double RandomDist::gaussian(double stdev) const {
#ifdef HAVE_LIBGSL
  return gsl_ran_gaussian ((gsl_rng*)rng, stdev);
#else
  STD_cerr << "ERROR: RandomDist::gaussian: libgsl missing" << STD_endl;
  return 0.0;
#endif
}

double RandomDist::uniform() const {
#ifdef HAVE_LIBGSL
  return gsl_rng_uniform ((gsl_rng*)rng);
#else
  STD_cerr << "ERROR: RandomDist::uniform: libgsl missing" << STD_endl;
  return 0.0;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////

fvector bruteforce_minimize1d(const MinimizationFunction& f, float low, float upp) {
  Log<NumericsComp> odinlog("","bruteforce_minimize1d");

  if(f.numof_fitpars()!=1) {
    ODINLOG(odinlog,errorLog) << "rank of minimization function != 1" << STD_endl;
    return 0.0;
  }
  
  int nvals=10;
  int niter=10;

  fvector xvals(nvals);
  fvector yvals(nvals);

  fvector funcarg(1);
  
  for(int iter=0; iter<niter; iter++) {
    xvals.fill_linear(low,upp);
    ODINLOG(odinlog,normalDebug) << iter << ": xvals" << xvals << STD_endl;
    int i; // fix for MSVC6
    for(i=0; i<nvals; i++) {
      funcarg[0]=xvals[i];
      yvals[i]=f.evaluate(funcarg);
    }
    ODINLOG(odinlog,normalDebug) << iter << ": yvals" << yvals << STD_endl;
    float minval=yvals[0];
    int minindex=0;
    for(i=1; i<nvals; i++) {
      if(yvals[i]<minval) {
        minindex=i;
        minval=yvals[i];
      }
    }
    low=xvals[STD_max(0,minindex-1)];
    upp=xvals[STD_min(nvals-1,minindex+1)];
  }
  
  fvector result(1);
  result[0]=0.5*(low+upp);
  return result;
}

/////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_UNIT_TEST

class MinimizationTestFunction : public MinimizationFunction {

  unsigned int numof_fitpars() const {return 1;}
  float evaluate(const fvector& xvec) const {return pow(xvec[0]-2.0,2);}

};


class NumericsTest : public UnitTest {

 public:
  NumericsTest() : UnitTest(NumericsComp::get_compName()) {}

 private:

  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    MinimizationTestFunction mtf;

    float min=bruteforce_minimize1d(mtf, -12.45, 9.77)[0];

    float expected=2.0;

    if( fabs(expected-min)>1.0e-3 ) {
      ODINLOG(odinlog,errorLog) << "minimize failed, got " << min << " but expected " << expected << STD_endl;
      return false;
    }

    return true;
  }

};


void alloc_NumericsTest() {new NumericsTest();} // create test instance
#endif
