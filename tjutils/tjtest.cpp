#include "tjtest.h"
#include "tjlog_code.h"


#ifndef NO_UNIT_TEST

const char* UnitTest::get_compName() {return "UnitTest";}

void UnitTest::init_static()    {
  tests=new STD_list<UnitTest*>;
}

void UnitTest::destroy_static() {
  for(STD_list<UnitTest*>::const_iterator it=tests->begin(); it!=tests->end(); ++it) {
    delete (*it);
  }
  delete tests;
}

UnitTest::UnitTest(const STD_string& testlabel) {
  set_label(testlabel); 
  tests->push_back(this);
}


int UnitTest::check_all() {
  Log<UnitTest> odinlog("","check_all");
  if(!tests) return 0;
  for(STD_list<UnitTest*>::const_iterator it=tests->begin(); it!=tests->end(); ++it) {
    ODINLOG(odinlog,infoLog) << "Testing " <<  (*it)->get_label() << " ..."  << STD_endl;
    if(!(*it)->check()) {
      ODINLOG(odinlog,errorLog) << "Test of " <<  (*it)->get_label() << " failed" << STD_endl;
      return -1;
    }
  }
  ODINLOG(odinlog,infoLog) << "All tests passed"  << STD_endl;
  return 0;
}

STD_list<UnitTest*>* UnitTest::tests=0;
EMPTY_TEMPL_LIST bool StaticHandler<UnitTest>::staticdone=false;

LOGGROUNDWORK(UnitTest)

#endif
