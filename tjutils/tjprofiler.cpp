#include "tjprofiler.h"
#include "tjstring.h"
#include "tjtools.h"
#include "tjarray.h"
#include "tjhandler_code.h"
#include "tjlog_code.h"


#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

//#ifdef ODIN_DEBUG
#define ENABLE_PROFILER
//#endif


const char* Profiler::get_compName() {return "Prof";}
LOGGROUNDWORK(Profiler)



Profiler::Profiler(const STD_string& func_name) : func_label(func_name) {
#ifdef ENABLE_PROFILER
  starttime=current_time_s();
#endif
}

Profiler::~Profiler() {
#ifdef ENABLE_PROFILER
  func_map->operator [] (func_label).time_spent+=(current_time_s()-starttime);
#endif
}

void Profiler::reset() {
#ifdef ENABLE_PROFILER
  if(func_map) func_map->clear();
#endif
}

void Profiler::dump_final_result() {
#ifdef ENABLE_PROFILER
  Log<Profiler> odinlog("Profiler","dump_final_result");
  if(!func_map) return;
  if(!func_map->size()) return;

  unsigned int maxlen=0;
  STD_map<STD_string,Profiler::elapsed>::const_iterator it;
  for(it=func_map->begin(); it!=func_map->end(); ++it) maxlen=STD_max(maxlen,(unsigned int)it->first.length());
  for(it=func_map->begin(); it!=func_map->end(); ++it) {
    ODINLOG(odinlog,infoLog) << it->first << ": " << STD_string(maxlen-it->first.length(),' ') << it->second.time_spent << STD_endl;
  }
  reset();
#endif
}

STD_string Profiler::get_memory_usage() {
  STD_string result;
#ifdef ENABLE_PROFILER
#ifndef NO_FILEHANDLING
  FILE* file_ptr=ODIN_FOPEN("/proc/self/statm",modestring(readMode));
  if(file_ptr==NULL) {
    return "Profiler::get_memory_usage: Memory usage not available";
  }

  char* cstr = new char [ODIN_MAXCHAR+1];
  int i=fread(cstr,1,ODIN_MAXCHAR,file_ptr);
  if(i<=ODIN_MAXCHAR) cstr[i]='\0';
  fclose(file_ptr);

  svector toks=tokens(cstr);
  delete[] cstr;
  if(toks.size()!=7) return result;

  float psize_mb=1.0;
#ifdef HAVE_UNISTD_H
// MinGW does not have getpagesize
#ifndef USING_WIN32
  psize_mb=float(getpagesize())/1048576.0;
#endif
#endif
  float total=atof(toks[0].c_str())*psize_mb;
  float shared=atof(toks[2].c_str())*psize_mb;
  float ram=total-shared;

  result+="total="+ftos(total)+"MB  ";
  result+="shared="+ftos(shared)+"MB  ";
  result+="ram="+ftos(ram)+"MB";

#endif
#endif
  return result;
}


void Profiler::init_static() {func_map.init("func_map");}
void Profiler::destroy_static() {func_map.destroy();}

template class SingletonHandler<Profiler::FuncMap ,true>;
SingletonHandler<Profiler::FuncMap, true> Profiler::func_map;


EMPTY_TEMPL_LIST bool StaticHandler<Profiler>::staticdone=false;
