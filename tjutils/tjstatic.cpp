#include "tjstatic.h"


void Static::append_to_destructor_list(Static* sp) {
  if(!destructor_list) {
    destructor_list=new STD_list<Static*>;
  }
  destructor_list->push_front(sp);
}


void Static::destroy_all() {
  if(destructor_list) {
    for(STD_list<Static*>::iterator it=destructor_list->begin(); it!=destructor_list->end(); ++it) {
//      STD_cout << "Static::destroy_all(): destructing " << typeid(*(*it)).name() << "<" << STD_endl;
      delete *it;
//      STD_cout << "Static::destroy_all(): done" << STD_endl;
    }
    delete destructor_list;
  }
  destructor_list=0;
}


STD_list<Static*>* Static::destructor_list=0;


