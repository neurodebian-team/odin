#include "tjhandler.h"
#include "tjlog.h"
#include "tjlog_code.h"

const char* HandlerComponent::get_compName() {return "Handler";}
LOGGROUNDWORK(HandlerComponent)


////////////////////////////////////////////////////////////////////////

void SingletonBase::set_singleton_map_external(SingletonMap* extmap) {
//  Log uses SingletonHandler -> do not use Log in here
  singleton_map_external=extmap;

  // in order to cache ptr properly, no singletons should be allocated at this point
  if(singleton_map && singleton_map->size()) {
    STD_cerr << "ERROR: SingletonBase::set_singleton_map_external: There are already singletons allocated:" << STD_endl;
    for(SingletonMap::iterator it=singleton_map->begin(); it!=singleton_map->end(); ++it) {
      STD_cerr << it->first << "/" << (void*)it->second << STD_endl;
    }
  }

}

SingletonBase::SingletonBase() {
  if(!singleton_map) singleton_map=new SingletonMap;
}

SingletonMap* SingletonBase::get_singleton_map() {
  // NOTE:  Debug uses SingletonHandler -> do not use Debug in here
  if(!singleton_map) singleton_map=new SingletonMap;
  return singleton_map;
}

STD_string SingletonBase::get_singleton_label(SingletonBase* sing_ptr) {
  Log<HandlerComponent> odinlog("SingletonBase","get_singleton_label");
  STD_string result;
  if(!(singleton_map || singleton_map_external)) {
    ODINLOG(odinlog,normalDebug) << "neither singleton_map nor singleton_map_external available" << STD_endl;
    return result;
  }

  SingletonMap::iterator mapbegin=singleton_map->begin();
  SingletonMap::iterator mapend  =singleton_map->end();


  if(singleton_map_external) {
    mapbegin=singleton_map_external->begin();
    mapend  =singleton_map_external->end();
  }

  for(SingletonMap::iterator it=mapbegin; it!=mapend; ++it) {
    if(it->second==sing_ptr) result=it->first;
  }
  ODINLOG(odinlog,normalDebug) << "result(" << (void*)sing_ptr << ")=" << result << STD_endl;

  return result;
}


void* SingletonBase::get_external_map_ptr(const STD_string& sing_label) {
  // NOTE:  Log uses SingletonHandler -> do not use Log in here
  if(singleton_map_external) {
    if(singleton_map_external->find(sing_label)==singleton_map_external->end()) {
      STD_cerr << "ERROR: SingletonBase::get_external_map_ptr: singleton >" << sing_label << "< not found in singleton_map_external" << STD_endl;
      return 0;
    }
    return (*singleton_map_external)[sing_label]->get_ptr();
  }
  return 0;
}

SingletonMap* SingletonBase::singleton_map=0;
SingletonMap* SingletonBase::singleton_map_external=0;


////////////////////////////////////////////////////////////////////////

