#include <tjutils/tjlog.h>


template<class C>
  void Log<C>::register_comp() {

    if(!registered) { // retry until LogBase:global is initialized
      registered=register_component(C::get_compName(),&(Log<C>::set_log_level));
      if(registered) {
        const char* env=getenv(C::get_compName());
        if(env) set_log_level(logPriority(atoi(env)));
      }
    }
    if(!registered) {  // global may not be initialized yet

      // Disable tracing until global becomes available
      logLevel=noLog;
      constrLevel=noLog;
    }

  }



template<class C>
  Log<C>::Log(const char* objectLabel, const char* functionName, logPriority level)
   : LogBase(C::get_compName(),objectLabel,0,functionName), constrLevel(level) {
    register_comp();
    ODINLOG(*this,constrLevel) << "START" << STD_endl;
  }


template<class C>
  Log<C>::Log(const Labeled* labeledObject, const char* functionName, logPriority level)
   : LogBase(C::get_compName(),0,labeledObject,functionName), constrLevel(level) {
    register_comp();
    ODINLOG(*this,constrLevel) << "START" << STD_endl;
  }


template<class C>
  Log<C>::~Log() {
    ODINLOG(*this,constrLevel) << "END" << STD_endl;
  }


template<class C>
  logPriority Log<C>::set_log_level(logPriority level) {
    if(level!=ignoreArgument) logLevel=level;
    return logLevel;
  }


/////////////////////////////////////////////////////////////////////////////////////////////////////////


// unregister logging component by local static object if shared library is unloaded 
template<class C>
class LogUnregister {
 public:
  LogUnregister() {}
  ~LogUnregister() {LogBase::unregister_component(C::get_compName());}
};


#define LOGGROUNDWORK(COMPONENT) \
template class Log<COMPONENT>; \
EMPTY_TEMPL_LIST logPriority Log<COMPONENT>::logLevel=RELEASE_LOG_LEVEL; \
EMPTY_TEMPL_LIST bool Log<COMPONENT>::registered=false; \
static LogUnregister<COMPONENT> COMPONENT ## _unregister;
