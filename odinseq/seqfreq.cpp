#include "seqfreq.h"

SeqFreqChanInterface& SeqFreqChanInterface::set_freqoffset(double freqoffset) {
  dvector flist(1);
  flist[0]=freqoffset;
  return set_freqlist(flist);
}

SeqFreqChanInterface& SeqFreqChanInterface::set_phase(double phaseval) {
  dvector plist(1);
  plist[0]=phaseval;
  return set_phaselist(plist);
}


SeqFreqChanInterface& SeqFreqChanInterface::set_phasespoiling(unsigned int size, double incr, double offset) {

  dvector plist(size);
  plist[0]=incr+offset; // recommended initialisation, handbook of MRI
  for(unsigned int iphase=1; iphase<size; iphase++) {
    plist[iphase]=double(int(plist[iphase-1]+iphase*incr+0.5) % 360); //  handbook of MRI
  }

  return set_phaselist(plist);
}


////////////////////////////////////////////////////////////////


SeqFreqChan::SeqFreqChan(const STD_string& object_label) : SeqVector(object_label), freqdriver(object_label+"_freqdriver"), phaselistvec(object_label+"_phaselistvec")  {
  Log<Seq> odinlog(this,"SeqFreqChan(...)");
  set_label(object_label); // this is neccessary because SeqClass is a virtual base class
  phaselistvec.user=this;
}


SeqFreqChan::SeqFreqChan(const STD_string& object_label,const STD_string& nucleus,
    const dvector& freqlist,const dvector& phaselist) : SeqVector(object_label), freqdriver(object_label+"_freqdriver"), phaselistvec(object_label+"_phaselistvec") {
  Log<Seq> odinlog(this,"SeqFreqChan(...)");
  nucleusName=nucleus;
  frequency_list=freqlist;
  SeqFreqChan::set_phaselist (phaselist); // Do NOT call virtual function in constructor
  phaselistvec.user=this;
}

SeqFreqChan::SeqFreqChan(const SeqFreqChan& sfc) {
  SeqFreqChan::operator = (sfc);
}

SeqFreqChan& SeqFreqChan::operator = (const SeqFreqChan& sfc) {
  Log<Seq> odinlog(this,"operator = (...)");
  SeqVector::operator = (sfc);
  nucleusName=sfc.nucleusName;
  freqdriver=sfc.freqdriver;
  phaselistvec=sfc.phaselistvec;
  frequency_list=sfc.frequency_list;
  phaselistvec.user=this; // precaution
  return *this;
}



SeqFreqChanInterface& SeqFreqChan::set_nucleus(const STD_string& nucleus) {
  Log<Seq> odinlog(this,"set_nucleus");
  nucleusName=nucleus;
  return *this;
}

STD_string SeqFreqChan::get_iteratorcommand(objCategory cat) const {
  return freqdriver->get_iteratorcommand(cat,get_freqlistindex());
}


SeqValList SeqFreqChan::get_freqvallist(freqlistAction action) const {
  Log<Seq> odinlog(this,"get_freqvallist");
  SeqValList result(get_label());
  result.set_value(get_frequency());
  return result;
}

STD_string SeqFreqChan::get_pre_program(programContext& context, objCategory cat, const STD_string& instr_label) const {
  return freqdriver->get_pre_program(context,cat,instr_label,get_default_frequency(),get_default_phase());
}


double SeqFreqChan::get_frequency() const {
  if(frequency_list.size()) return frequency_list[get_current_index()];
  else return 0.0;
}




bool SeqFreqChan::prep() {
  Log<Seq> odinlog(this,"prep");

  if(!SeqClass::prep()) return false;

  freqdriver->prep_driver(nucleusName,get_freqlist());

  prep_iteration(); // to initialize constant frequency offset

  return true;
}

bool SeqFreqChan::prep_iteration() const {
  Log<Seq> odinlog(this,"prep_iteration");
  double phase=get_phase();
  double freq=get_frequency();
  ODINLOG(odinlog,normalDebug) << "freq/phase=" << freq << "/" << phase << STD_endl;
  freqdriver->prep_iteration(freq,phase,get_freqchan_duration());
  return true;
}


double SeqFreqChan::closest2zero(const dvector& v) {
  Log<Seq> odinlog("SeqFreqChan","closest2zero");
  unsigned int n=v.length();
  if(!n) return 0.0;
  double result=v[0];
  double min=fabs(result);
  for(unsigned int i=0; i<v.length(); i++) {
    double absval=fabs(v[i]);
    if(absval<min) {
      min=absval;
      result=v[i];
    }
  }
  return result;
}


EMPTY_TEMPL_LIST bool StaticHandler<SeqFreqChan>::staticdone=false;

