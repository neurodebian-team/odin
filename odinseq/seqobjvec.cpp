#include "seqobjvec.h"
#include "seqgrad.h"
#include "seqparallel.h"
#include "seqgradchanlist.h"

SeqObjVector::SeqObjVector(const STD_string& object_label)
  : SeqVector(object_label), SeqObjBase(object_label) {
  set_label(object_label);
}

SeqObjVector::SeqObjVector(const SeqObjVector& sov) {
  SeqObjVector::operator = (sov);
}

SeqObjVector& SeqObjVector::operator = (const SeqObjVector& sov) {
  SeqObjBase::operator = (sov);
  SeqVector::operator = (sov);
  clear();
  for(constiter it=sov.get_const_begin();it!=sov.get_const_end();++it) {
    append(**it);
  }
  return *this;
}

SeqObjVector& SeqObjVector::operator += (const SeqObjBase& soa) {
  append(soa);
  return *this;
}

SeqObjVector& SeqObjVector::operator += (SeqGradObjInterface& sgoa) {
  SeqParallel* par=new SeqParallel(sgoa.get_label());
  par->set_temporary();
  par->set_gradptr(&sgoa);
  (*this)+=(*par);
  return *this;
}

SeqObjVector& SeqObjVector::operator += (SeqGradChanList& sgcl) {
  SeqParallel* par=new SeqParallel(sgcl.get_label());
  par->set_temporary();
  (*par) /= sgcl;
  (*this)+=(*par);
  return *this;
}

STD_string SeqObjVector::get_program(programContext& context) const {
  STD_string result;
  constiter it=get_current();
  if(it!=get_const_end()) result=(*it)->get_program(context);
  return result;
}


double SeqObjVector::get_duration() const {
  double result=0.0;
  constiter it=get_current();
  if(it!=get_const_end()) result=(*it)->get_duration();
  return result;
}

unsigned int SeqObjVector::event(eventContext& context) const {
  unsigned int result=0;
  constiter it=get_current();
  if(it!=get_const_end()) result+=(*it)->event(context);
  return result;
}



SeqValList SeqObjVector::get_freqvallist(freqlistAction action) const {
  SeqValList result;
  constiter it=get_current();
  if(it!=get_const_end()) result=(*it)->get_freqvallist(action);
  return result;
}


SeqValList SeqObjVector::get_delayvallist() const {
  SeqValList result;
  constiter it=get_current();
  if(it!=get_const_end()) result=(*it)->get_delayvallist();
  return result;
}


double SeqObjVector::get_rf_energy() const {
  double result=0.0;
  constiter it=get_current();
  if(it!=get_const_end()) result=(*it)->get_rf_energy();
  return result;
}

void SeqObjVector::clear_container() {
  List<SeqObjBase, const SeqObjBase*, const SeqObjBase&>::clear();
}


void SeqObjVector::query(queryContext& context) const {
  SeqTreeObj::query(context); // defaults

  if(context.action==count_acqs) {
    constiter it=get_current();
    if(it!=get_const_end()) (*it)->query(context);
    return;
  }

  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    context.parentnode=this; // reset to this because it might changed by last query
    (*it)->query(context);
  }

}


RecoValList SeqObjVector::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  RecoValList result(get_label());
  constiter it=get_current();
  if(it!=get_const_end()) result=(*it)->get_recovallist(reptimes,coords);
  return result;
}


List<SeqObjBase, const SeqObjBase*, const SeqObjBase&>::constiter SeqObjVector::get_current() const {
  unsigned int i=0;
  unsigned int index=get_current_index();

  constiter result=get_const_end();

  for(constiter it=get_const_begin(); it!=get_const_end(); ++it) {
    if(i==index) {
      result=it;
      return result;
    }
    i++;
  }

  return result;
}
