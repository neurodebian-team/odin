#include "seqgrad.h"

float SeqGradInterface::get_gradintegral_norm() const {
  fvector intgegralvec=get_gradintegral();
  return norm3(intgegralvec[0],intgegralvec[1],intgegralvec[2]);
}

