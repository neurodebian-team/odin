#include "seqdelay.h"


SeqDelay::SeqDelay(const STD_string& object_label,float delayduration,
               const STD_string& command,const STD_string& durationVariable)
             : SeqObjBase(object_label),SeqDur(object_label,delayduration), delaydriver(object_label) {
  cmd=command;
  durcmd=durationVariable;
}


SeqDelay::SeqDelay(const SeqDelay& sd) : delaydriver(sd.get_label()) {
  SeqDelay::operator = (sd);
}

SeqDelay& SeqDelay::set_command(const STD_string& command) {
  cmd=command;
  return *this;
}



SeqDelay& SeqDelay::operator = (const SeqDelay& sd) {
  SeqObjBase::operator = (sd);
  SeqDur::operator = (sd);
  delaydriver=sd.delaydriver;
  cmd=sd.cmd;
  durcmd=sd.durcmd;
  return *this;
}

SeqDelay& SeqDelay::operator = (float dur) {
  set_duration(dur);
  return *this;
}


STD_string SeqDelay::get_program(programContext& context) const {
  return delaydriver->get_program(context,get_duration(),cmd,durcmd);
}

