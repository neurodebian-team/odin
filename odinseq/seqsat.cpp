#include "seqsat.h"


SeqPulsarSat::SeqPulsarSat(const STD_string& object_label,satNucleus nuc,float bandwidth)
  : SeqPulsar(object_label,false,false) {
  Log<Seq> odinlog(this,"SeqPulsarSat");

  double offset_ppm=0.0;
  if(nuc==fat) offset_ppm=-3.28;

  double offset_freq=systemInfo->get_nuc_freq("")*offset_ppm*1.0e-6;
  set_dim_mode(zeroDeeMode);

  ODINLOG(odinlog,normalDebug) << "offset_freq=" << offset_freq << STD_endl;

  set_Tp(secureDivision(3.0,bandwidth));
  resize(128);
  SeqPulsar::set_flipangle(114.0); // flipangle determined experimentally with oil phantom  (do NOT call virtual function in constructor)
  set_shape("Const");
  set_trajectory("Const(0.0,1.0)");
  set_filter("Gauss");
  set_freqoffset( offset_freq );
  SeqPulsar::set_pulse_type(saturation); // do NOT call virtual function in constructor

  refresh();
  set_interactive(true);
}



SeqPulsarSat::SeqPulsarSat(const STD_string& object_label,float bandwidth,
                           double freqoffset, float flipangle, float rel_filterwidth)
  : SeqPulsar(object_label,false,false) {
  Log<Seq> odinlog(this,"SeqPulsarSat");
  ODINLOG(odinlog,normalDebug) << "SeqPulsarSat.." << STD_endl;
  double effectiv_bandwidth;

  ODINLOG(odinlog,normalDebug) << "SeqPulsarSat bandwidth/ satoffset_freq / flipangle / rel_filterwidth" <<STD_endl;
  ODINLOG(odinlog,normalDebug) <<  bandwidth << " / "
	<< freqoffset << " / " << flipangle  << " / " << rel_filterwidth << STD_endl;

  set_dim_mode(zeroDeeMode);

  effectiv_bandwidth=rel_filterwidth * bandwidth;

  ODINLOG(odinlog,normalDebug) << "rel_filterwidth / effectiv_bandwidth " << rel_filterwidth << " / " << effectiv_bandwidth
	<< STD_endl;
  //  set_Tp(secureDivision(3.0,bandwidth));
  set_Tp(secureDivision(1,effectiv_bandwidth));
  resize(128);
  SeqPulsar::set_flipangle(flipangle); // do NOT call virtual function in constructor
  set_shape("Const");
  set_trajectory("Const(0.0,1.0)");
  set_filter("Gauss");
  set_freqoffset( freqoffset );
  SeqPulsar::set_pulse_type(saturation); // do NOT call virtual function in constructor

  refresh();
  set_interactive(true);
}




SeqPulsarSat::SeqPulsarSat(const SeqPulsarSat& spg) {
  SeqPulsarSat::operator = (spg);
}

SeqPulsarSat& SeqPulsarSat::operator = (const SeqPulsarSat& spg) {
  SeqPulsar::operator = (spg);
  refresh();
  return *this;
}



//////////////////////////////////////////////////////////////////


SeqSat::SeqSat(const STD_string& object_label,satNucleus nuc,float bandwidth, unsigned int npulses)
  : SeqObjList(object_label),
  puls(object_label+"_pulse",nuc,bandwidth),
  spoiler_read_pos(object_label+"_spoiler_read_pos",readDirection,0.6*systemInfo->get_max_grad(),2.0),
  spoiler_slice_neg(object_label+"_spoiler_slice_neg",sliceDirection,-0.6*systemInfo->get_max_grad(),2.0),
  spoiler_read_neg(object_label+"_spoiler_read_neg",readDirection,-0.6*systemInfo->get_max_grad(),2.0),
  spoiler_slice_pos(object_label+"_spoiler_slice_pos",sliceDirection,0.6*systemInfo->get_max_grad(),2.0),
  spoiler_phase_pos(object_label+"_spoiler_phase_pos",phaseDirection,0.6*systemInfo->get_max_grad(),2.0),
  npulses_cache(npulses) {
  SeqPulsInterface::set_marshall(&puls);
  SeqFreqChanInterface::set_marshall(&puls);
  build_seq();
}

SeqSat::SeqSat(const SeqSat& spg) {
  SeqPulsInterface::set_marshall(&puls);
  SeqFreqChanInterface::set_marshall(&puls);
  SeqSat::operator = (spg);
}

SeqSat& SeqSat::operator = (const SeqSat& spg) {
  SeqObjList::operator = (spg);
  puls=spg.puls;
  spoiler_read_pos=spg.spoiler_read_pos;
  spoiler_slice_neg=spg.spoiler_slice_neg;
  spoiler_read_neg=spg.spoiler_read_neg;
  spoiler_slice_pos=spg.spoiler_slice_pos;
  spoiler_phase_pos=spg.spoiler_phase_pos;
  npulses_cache=spg.npulses_cache;
  build_seq();
  return *this;
}

void SeqSat::build_seq() {
  clear();
  (*this)+=(spoiler_read_pos/spoiler_slice_neg);
  for(unsigned int i=0; i<npulses_cache; i++) {
    (*this)+=puls;
    if(i<(npulses_cache-1)) (*this)+=spoiler_phase_pos;
  }
  (*this)+=(spoiler_read_neg/spoiler_slice_pos);
}

SeqGradInterface& SeqSat::set_strength(float gradstrength) {
  spoiler_read_pos.set_strength(gradstrength);
  spoiler_slice_neg.set_strength(-gradstrength);
  spoiler_read_neg.set_strength(-gradstrength);
  spoiler_slice_pos.set_strength(gradstrength);
  spoiler_phase_pos.set_strength(gradstrength);
  return *this;
}


SeqGradInterface& SeqSat::invert_strength() {
  spoiler_read_pos.invert_strength();
  spoiler_slice_neg.invert_strength();
  spoiler_read_neg.invert_strength();
  spoiler_slice_pos.invert_strength();
  spoiler_phase_pos.invert_strength();
  return *this;
}


float SeqSat::get_strength() const {
  return spoiler_read_pos.get_strength();
}

fvector SeqSat::get_gradintegral() const {
  return spoiler_read_pos.get_gradintegral()
        +spoiler_slice_neg.get_gradintegral()
        +spoiler_read_neg.get_gradintegral()
        +spoiler_slice_pos.get_gradintegral()
        +(npulses_cache-1)*spoiler_phase_pos.get_gradintegral();
}


double SeqSat::get_gradduration() const {
  return 2.0*spoiler_read_pos.get_gradduration()+(npulses_cache-1)*spoiler_phase_pos.get_gradduration();
}

SeqGradInterface& SeqSat::set_gradrotmatrix(const RotMatrix& matrix) {
  spoiler_read_pos.set_gradrotmatrix(matrix);
  spoiler_slice_neg.set_gradrotmatrix(matrix);
  spoiler_read_neg.set_gradrotmatrix(matrix);
  spoiler_slice_pos.set_gradrotmatrix(matrix);
  spoiler_phase_pos.set_gradrotmatrix(matrix);
  return *this;
}

