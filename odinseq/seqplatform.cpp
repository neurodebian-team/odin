#include "seqplatform.h"
#include "seqdriver.h"

#include <tjutils/tjhandler_code.h>

/////////////////////////////////////////////////////////////////////////////////


void SeqPlatformProxy::set_current_platform(odinPlatform pF) {
  if(platforms) platforms->set_current(pF);
}

odinPlatform SeqPlatformProxy::get_current_platform() {
  if(platforms) return platforms->get_current_platform_id();
  else return SeqPlatformInstances::pf_during_platform_construction;
}

int SeqPlatformProxy::load_systemInfo(const STD_string& filename) {
  Log<Seq> odinlog("SeqPlatformProxy","load_systemInfo");

  SeqPlatformProxy(); // create platform instances

  LDRstring pfdummy("","Platform");
  // determine the platform first
  pfdummy.load(filename);
  if(STD_string(pfdummy)=="") return -1;
  ODINLOG(odinlog,normalDebug) << "pfdummy=" << STD_string(pfdummy) << STD_endl;

  odinPlatform pF=odinPlatform(0);
  svector poss(get_possible_platforms());
  for(unsigned int i=0; i<poss.size(); i++) {
    ODINLOG(odinlog,normalDebug) << "poss[" << i << "]=" << poss[i] << STD_endl;
    if(poss[i]==STD_string(pfdummy)) pF=odinPlatform(i);
  }
  set_current_platform(pF);


  // then load the whole systemInfo into current platform plugin
  return SystemInterface()->LDRblock::load(filename);
}

STD_string SeqPlatformProxy::get_platform_str(odinPlatform pF) {
  if(platforms->instance[pF]) return platforms->instance[pF]->get_label();
  return "NotYetRegistered";
}


svector SeqPlatformProxy::get_possible_platforms() {
  Log<Seq> odinlog("SeqPlatformProxy","get_possible_platforms");
  SeqPlatformProxy(); // create platform instances
  svector result; result.resize(numof_platforms);
  for(unsigned int i=0; i<numof_platforms; i++) {
    result[i]=get_platform_str(odinPlatform(i));
  }
  return result;
}

STD_string SeqPlatformProxy::get_platforms_usage() {
  STD_string result;
#ifndef NO_CMDLINE
  SeqPlatformProxy(); // create platform instances
  for(unsigned int i=0; i<numof_platforms; i++) {
    if(platforms->instance[i]) {
      result+=STD_string(platforms->instance[i]->get_label())+" ACTIONS:\n\n";
      result+=SeqCmdLine::format_actions(platforms->instance[i]->get_actions_usage());
    }
  }
#endif
  return result;
}


int SeqPlatformProxy::get_platform_for_action(const STD_string& action) {
#ifndef NO_CMDLINE
  Log<Seq> odinlog("SeqPlatformProxy","get_platform_for_action");
  SeqPlatformProxy(); // create platform instances
  for(unsigned int ipf=0; ipf<numof_platforms; ipf++) {
    if(platforms->instance[ipf]) {
      SeqCmdlineActionList actions=platforms->instance[ipf]->get_actions_usage();
      ODINLOG(odinlog,normalDebug) << "platform/actions.size()=" << ipf << "/" << actions.size() << STD_endl;
      for(STD_list<SeqCmdlineAction>::const_iterator it=actions.begin(); it!=actions.end(); ++it) {
        STD_string pfact=it->action;
        ODINLOG(odinlog,normalDebug) << "pfact=" << pfact << STD_endl;
        if(pfact==action) return ipf;
      }
    }
  }  
#endif
  return -1;
}


SeqPlatform* SeqPlatformProxy::get_platform_ptr() {return platforms->get_current();}


void SeqPlatformProxy::init_static() {
  Log<Seq> odinlog("SeqClass","init_static");
  platforms.init("platforms");
}

void SeqPlatformProxy::destroy_static() {
  Log<Seq> odinlog("SeqPlatformProxy","destroy_static");
  platforms.destroy();
}


template class SingletonHandler<SeqPlatformInstances,false>;
SingletonHandler<SeqPlatformInstances,false> SeqPlatformProxy::platforms;

EMPTY_TEMPL_LIST bool StaticHandler<SeqPlatformProxy>::staticdone=false;
