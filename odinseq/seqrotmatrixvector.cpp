#include "seqrotmatrixvector.h"
#include "seqloop.h"
#include "seqgradchan.h"

#include <tjutils/tjhandler_code.h>


SeqRotMatrixVector::SeqRotMatrixVector(const STD_string& object_label): SeqVector(object_label){
  Log<Seq> odinlog(this,"SeqRotMatrixVector(const STD_string&)");
  set_label(object_label);
}

SeqRotMatrixVector::SeqRotMatrixVector(const SeqRotMatrixVector& srmv) {
  Log<Seq> odinlog(this,"SeqRotMatrixVector(SeqRotMatrixVector)");
  SeqRotMatrixVector::operator = (srmv);
}

SeqRotMatrixVector& SeqRotMatrixVector::operator = (const SeqRotMatrixVector& srmv){
  Log<Seq> odinlog(this,"operator =");
 SeqVector::operator = (srmv);
 rotMatrixList=srmv.rotMatrixList;
 return *this;
};

SeqRotMatrixVector::~SeqRotMatrixVector() {
  Log<Seq> odinlog(this,"~SeqRotMatrixVector");
}

 const RotMatrix& SeqRotMatrixVector::operator [] (unsigned long index) const{
 unsigned long i=0;
 STD_list<RotMatrix>::const_iterator it;
 for ( it=rotMatrixList.begin(); it !=rotMatrixList.end();++it){
   if(i==index) return *it;
   i++;
 }
 return dummyrotmat;
};

const RotMatrix& SeqRotMatrixVector::get_current_matrix () const{
  if(get_vectorsize()) return (*this)[get_current_index()];
  return dummyrotmat;
}


SeqRotMatrixVector& SeqRotMatrixVector::create_inplane_rotation(unsigned int nsegments) {
  Log<Seq> odinlog(this,"create_inplane_rotation");
  clear();
  for(unsigned int i=0; i<nsegments; i++) {
    RotMatrix matrix( "rotmatrix"+itos(i) );
    float phi=2.0*PII*float(i)/float(nsegments);

    matrix.set_inplane_rotation(phi);
    ODINLOG(odinlog,normalDebug) << "[" << i << "]=" << matrix.print() << STD_endl;

    append(matrix);
  }
  return *this;
}


RotMatrix SeqRotMatrixVector::get_maxMatrix () const {
  RotMatrix temp,maxMat;

 STD_list<RotMatrix>::const_iterator it;
 it=rotMatrixList.begin();
 maxMat=*it;
 for ( it=rotMatrixList.begin(); it !=rotMatrixList.end();++it){
      temp=*it;
   for(unsigned int i=0;i<3;i++)
    for(unsigned int j=0;j<3;j++)
      if(fabs(maxMat[i][j])<fabs(temp[i][j]))
      maxMat[i][j]=temp[i][j];
 }

  return(maxMat);
}


bool SeqRotMatrixVector::prep_iteration() const {
  Log<Seq> odinlog(this,"prep_iteration");
  bool result=true;
  return result;
}



//RotMatrix SeqRotMatrixVector::dummyrotmat;

template class Handled<const SeqRotMatrixVector* >;
