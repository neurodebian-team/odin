#include "seqdriver.h"
#include "seqplatform.h"


#ifdef PARAVISION_PLUGIN
#include "../platforms/Paravision/odinseq_paravision/seqparavision.h"
#endif


#ifdef IDEA_PLUGIN
#include "../platforms/IDEA_n4/odinseq_idea/seqidea.h"
#endif

#ifdef STANDALONE_PLUGIN
#include "../platforms/StandAlone/odinseq_standalone/seqstandalone.h"
#endif

#ifdef EPIC_PLUGIN
#include "../platforms/EPIC/odinseq_epic/seqepic.h"
#endif


/////////////////////////////////////////////////////////////////////




SeqPlatformInstances::SeqPlatformInstances() {
  Log<Seq> odinlog("SeqPlatformInstances","SeqPlatformInstances");
  PlatformRegistration da;
  int i;
  for(i=0; i<numof_platforms; i++) instance[i]=0;

#ifdef STANDALONE_PLUGIN
  SystemInterface::set_current_pf(pf_during_platform_construction=standalone);
  instance[standalone]=new SeqStandAlone(da);
#endif

#ifdef PARAVISION_PLUGIN
  SystemInterface::set_current_pf(pf_during_platform_construction=paravision);
  ODINLOG(odinlog,normalDebug) << "allocating SeqParavision" << STD_endl;
  instance[paravision]=new SeqParavision(da);
#endif

#ifdef IDEA_PLUGIN
  SystemInterface::set_current_pf(pf_during_platform_construction=numaris_4);
  instance[numaris_4]=new SeqIdea(da);
#endif

#ifdef EPIC_PLUGIN
  SystemInterface::set_current_pf(pf_during_platform_construction=epic);
  instance[epic]=new SeqEpic(da);
#endif


  // set current_pf to the first available platform
  for(i=0; i<numof_platforms; i++) {
    if(instance[i]) {
      SystemInterface::set_current_pf(odinPlatform(i));
      break;
    }
  }
  ODINLOG(odinlog,normalDebug) << "SystemInterface::current_pf=" << SystemInterface::get_current_pf() << STD_endl;
}

SeqPlatformInstances::~SeqPlatformInstances() {
  for(int i=0; i<numof_platforms; i++) if(instance[i]) delete instance[i];
}

bool SeqPlatformInstances::set_current(odinPlatform pf) {
  Log<Seq> odinlog("SeqPlatformInstances","set_current");
  if(instance[pf]) SystemInterface::set_current_pf(pf);
  else {
    ODINLOG(odinlog,errorLog) << "Platform No "+itos(pf)+" not available" << STD_endl;
    return false;
  }
  ODINLOG(odinlog,normalDebug) << "pf/SystemInterface::current_pf=" << pf << "/" << SystemInterface::get_current_pf() << STD_endl;
  return true;
}

odinPlatform SeqPlatformInstances::pf_during_platform_construction=odinPlatform(0);

