#include "seqdur.h"


SeqDur::SeqDur(const STD_string& object_label, float duration) : SeqTreeObj() {
  set_label(object_label); // this is neccessary because SeqClass is a virtual base class
  set_duration(duration);
}

SeqDur::SeqDur(const STD_string& object_label) : SeqTreeObj() {
  set_label(object_label); // this is neccessary because SeqClass is a virtual base class
  set_duration(0.0);
}


SeqDur::SeqDur(const SeqDur& sd) {
  SeqDur::operator = (sd);
}

SeqDur& SeqDur::operator = (const SeqDur& sd) {
  SeqTreeObj::operator = (sd);
  set_duration(sd.dur);
  return *this;
}



double SeqDur::get_duration() const {
  return dur;
}


SeqDur& SeqDur::set_duration(float duration) {
  dur=duration;
  if (duration<systemInfo->get_min_duration(delayObj)) {
    dur=systemInfo->get_min_duration(delayObj);
  }
  return *this;
}

