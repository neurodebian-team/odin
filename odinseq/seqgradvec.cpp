#include "seqgradvec.h"
#include "seqgradconst.h"
#include "seqloop.h"


SeqGradVector::SeqGradVector(const STD_string& object_label,direction gradchannel,
    float maxgradstrength,const fvector& trimarray, double gradduration)
       : SeqGradChan(object_label,gradchannel,maxgradstrength,gradduration),
         SeqVector(object_label) {
  parent=0;
  set_trims(trimarray);
}


SeqGradVector::SeqGradVector(const SeqGradVector& sgv) {
  parent=0;
  SeqGradVector::operator = (sgv);
}


SeqGradVector::SeqGradVector(const STD_string& object_label)
   : SeqGradChan(object_label), SeqVector(object_label)  {
  parent=0;
}


SeqGradVector& SeqGradVector::set_trims(const fvector& trims) {
  trimvals=trims;
  return *this;
}


unsigned int SeqGradVector::get_vectorsize() const {return trimvals.size();}


svector SeqGradVector::get_vector_commands(const STD_string& iterator) const {
  return graddriver->get_vector_commands(iterator);
}


SeqGradVector& SeqGradVector::operator = (const SeqGradVector& sgv) {
  Log<Seq> odinlog(this,"SeqGradVector::operator = ");
  SeqGradChan::operator = (sgv);
  SeqVector::operator = (sgv);
  trimvals=sgv.trimvals;
  return *this;
}


STD_string SeqGradVector::get_grdpart(float matrixfactor) const {
  Log<Seq> odinlog(this,"get_grdpart");

  ODINLOG(odinlog,normalDebug) << "matrixfactor=" << matrixfactor << STD_endl;

  if(parent) {
    ODINLOG(odinlog,normalDebug) << "has parent " << parent->get_label() << STD_endl;
    return parent->get_grdpart(matrixfactor);
  }


  if(SeqVector::is_handled()) {
    return graddriver->get_vector_program(get_strength(),matrixfactor,get_current_reord_index());
  }

  return graddriver->get_const_program(get_current_strength(),matrixfactor); // Generate const-gradient entry if vector not handled (for Bruker)

}


float SeqGradVector::get_current_strength() const {
  unsigned int count=get_current_index();
  if(parent) count=parent->get_current_index();
  float trim=1.0;
  if(count<trimvals.size()) trim=trimvals[count];
  return get_strength()*trim;
}


bool SeqGradVector::prep() {
  Log<Seq> odinlog(this,"prep");
  if(!SeqGradChan::prep()) return false;
  bool result=graddriver->prep_vector(get_strength(),get_grdfactors_norot(),get_gradduration(),trimvals,get_index_matrix(),get_nesting_relation());
  return result;
}


svector SeqGradVector::get_reord_vector_commands(const STD_string& iterator) const {
  Log<Seq> odinlog(this,"get_reord_vector_commands");
  svector result=SeqVector::get_reord_vector_commands(iterator);
  unsigned int result_size=result.size();

  svector reordvec=graddriver->get_reord_commands();
  ODINLOG(odinlog,normalDebug) << "reordvec=" << reordvec.printbody() << STD_endl;

  if(reordvec.size()) {
    if(!result_size) return reordvec;
    svector newresult; newresult.resize(result_size+reordvec.size());
    unsigned int i;
    for(i=0; i<result.size(); i++)   newresult[i]=result[i];
    for(i=0; i<reordvec.size(); i++) newresult[result_size+i]=reordvec[i];
    return newresult;
  }
  return result;
}


SeqGradChan& SeqGradVector::get_subchan(double starttime, double endtime) const {
  SeqGradVector* sgv= new SeqGradVector(*this);
  sgv->set_label(STD_string(get_label())+"_("+ftos(starttime)+"-"+ftos(endtime)+")");
  sgv->set_duration(endtime-starttime);
  sgv->set_temporary();
  sgv->parent=this;
  return *sgv;
}


bool SeqGradVector::prep_iteration() const {
  Log<Seq> odinlog(this,"prep_iteration");
  int count=get_current_index();
  if(parent) count=parent->get_current_index();
  ODINLOG(odinlog,normalDebug) << "count/vectorsize=" << count << "/" << get_vectorsize() << STD_endl;
  return graddriver->prep_vector_iteration(count);
}

