#include "seqacqread.h"
#include "seqlist.h"


void SeqAcqRead::common_init() {
  SeqAcqInterface::set_marshall(&acq);
  SeqFreqChanInterface::set_marshall(&acq);
}

SeqAcqRead::SeqAcqRead(const STD_string& object_label,double sweepwidth,unsigned int read_size,
            float fov, direction gradchannel,float os_factor,
            float partial_fourier, bool partial_fourier_at_end,
            const STD_string& nucleus,const dvector& phaselist,const dvector& freqlist,
            float timestep, rampType rampmode )
  : SeqParallel(object_label),
  corrected_partfour(STD_max(0.0f,STD_min(1.0f,partial_fourier))), // check and correct range
  acq(object_label+"_acq",(1.0-0.5*corrected_partfour)*read_size+0.5,sweepwidth,os_factor,nucleus,phaselist,freqlist),
  read(object_label+"_read"),
  middelay(object_label+"_middelay",0.0),
  midgrad(object_label+"_midgrad",gradchannel,0.0),
  tozero(object_label+"_tozero",0.0) {
  Log<Seq> odinlog(this,"SeqAcqRead");

  common_init();

  float gradstrength=secureDivision(2.0*PII*acq.get_sweepwidth(),systemInfo->get_gamma(nucleus)*fov); // Use adjusted sweepwidth
  double constgradduration=secureDivision(acq.get_npts(),acq.get_sweepwidth());

  double rastertime=systemInfo->get_rastertime(gradObj);
  if(rastertime>0.0) {
    int nraster=(int)secureDivision(constgradduration,rastertime);
    if(rastertime*nraster!=constgradduration) nraster++; // always increase length of constant part
    constgradduration=nraster*rastertime;
  }

  read=SeqGradTrapez(object_label+"_read",gradchannel, gradstrength, constgradduration, timestep, rampmode);
  tozero=SeqDelay(object_label+"_tozero",read.get_offramp_duration()+systemInfo->get_inter_grad_delay());

  float rel_center=secureDivision(0.5*(1.0-corrected_partfour),1.0-0.5*corrected_partfour);
  ODINLOG(odinlog,normalDebug) << "rel_center=" << rel_center << STD_endl;

  if(partial_fourier_at_end) rel_center=1.0-rel_center;
  
  acq.set_rel_center(rel_center);

  float dephintegral=-(read.get_onramp_integral()+rel_center*read.get_constgrad_integral());
  float rephintegral=-(read.get_offramp_integral()+(1.0-rel_center)*read.get_constgrad_integral());

  // Initialze once so that consecutive calls to get_dephgrad return always the same objects
//  readdephgrad=SeqGradTrapez(object_label+"_readdephgrad",
//                             dephintegral,
//                             gradchannel,0.5 *read.get_constgrad_duration(),
//                             timestep,rampmode);

  readdephgrad=SeqGradTrapez(object_label+"_readdephgrad",dephintegral,gradstrength,gradchannel,timestep,rampmode);


//  readrephgrad=SeqGradTrapez(object_label+"_readrephgrad",
//                             rephintegral,
//                             gradchannel,0.5 *read.get_constgrad_duration(),
//                             timestep,rampmode);

  readrephgrad=SeqGradTrapez(object_label+"_readrephgrad",rephintegral,gradstrength,gradchannel,timestep,rampmode);

  build_seq();
}




SeqAcqRead::SeqAcqRead(const STD_string& object_label)
   : SeqParallel(object_label) {
  common_init();
}


SeqAcqRead::SeqAcqRead(const SeqAcqRead& sar) {
  common_init();
  SeqAcqRead::operator = (sar);
}


double SeqAcqRead::get_acquisition_center() const {
  double result=0.0;
  result+=SeqParallel::get_pulprogduration();
  result+=middelay.get_duration();
  result+=acq.get_acquisition_center();
  return result;
}


double SeqAcqRead::get_acquisition_start() const {
  double result=0.0;
  result+=SeqParallel::get_pulprogduration();
  result+=middelay.get_duration();
  result+=acq.get_acquisition_start();
  return result;
}


SeqAcqInterface& SeqAcqRead::set_sweepwidth(double sw, float os_factor) {
  Log<Seq> odinlog(this,"set_sweepwidth");
  ODINLOG(odinlog,warningLog) << "Ignoring request to change sweepwidth after construction" << STD_endl;
  return *this;
}


SeqAcqRead& SeqAcqRead::operator = (const SeqAcqRead& sar) {
  SeqParallel::operator = (sar);
  acq=sar.acq;
  read=sar.read;
  middelay=sar.middelay;
  midgrad=sar.midgrad;
  tozero=sar.tozero;
  readdephgrad=sar.readdephgrad;
  readrephgrad=sar.readrephgrad;
  build_seq();
  return *this;
}


void SeqAcqRead::build_seq() {
  Log<Seq> odinlog(this,"build_seq");
  bool seqset=false;

  double ppgdur=SeqParallel::get_pulprogduration();
  double gradstart=systemInfo->get_grad_shift_delay()+read.get_onramp_duration();
  double acqstart=ppgdur+acq.get_acquisition_start();

  double shift=gradstart-acqstart;

  ODINLOG(odinlog,normalDebug) << "ppgdur/gradstart/acqstart/shift=" << ppgdur << "/" << gradstart << "/" << acqstart << "/" << shift << STD_endl;

  if(shift>=systemInfo->get_min_duration(delayObj)) {
    ODINLOG(odinlog,normalDebug) << "acq is played out later, because gradient switching is delayed" << STD_endl;
    middelay.set_duration(shift);
    SeqParallel::operator = ( read / ( middelay + acq + tozero ) );
    seqset=true;
  }

  if((-shift)>=systemInfo->get_min_duration(gradObj)) {
    ODINLOG(odinlog,normalDebug) << "gradients are played out later, because acq is delayed" << STD_endl;
    midgrad.set_duration(-shift);
    SeqParallel::operator = ( (midgrad+read) / ( acq + tozero ) );
    seqset=true;
  }

  if(!seqset) {
    ODINLOG(odinlog,normalDebug) << "acq and gradients are played out simultaneously" << STD_endl;
    SeqParallel::operator = ( read / ( acq + tozero ) );
  }

}


const SeqVector* SeqAcqRead::get_dephgrad(SeqGradChanParallel& dephobj, bool rephase) const {
  SeqGradTrapez* sgt;
  if(rephase) sgt=new SeqGradTrapez(readrephgrad); // Create deep copy so user can modify SeqAcqDeph safely
  else        sgt=new SeqGradTrapez(readdephgrad); // Create deep copy so user can modify SeqAcqDeph safely
  sgt->set_temporary();
  dephobj+=(*sgt);
  return 0;
}


