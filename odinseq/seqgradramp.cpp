#include "seqgradramp.h"

fvector SeqGradRamp::makeGradRamp(rampType type, float beginVal, float endVal, unsigned int n_vals, bool reverseramp) {
  unsigned int i;
  fvector result(n_vals);

  if(n_vals==1) {
    result[0]=0.5*(beginVal+endVal);
    return result;
  }

  if(type==linear) result.fill_linear(beginVal,endVal);

  if(type==sinusoidal) {
    for(i=0; i<n_vals; i++) {
      float x=secureDivision(i,n_vals-1);
      result[i]= beginVal + (endVal-beginVal) * 0.5 * (sin (PII*(x-0.5)) + 1.0);
    }
  }

  if(type==half_sinusoidal) {
    for(i=0; i<n_vals; i++) {
      float x=secureDivision(i,n_vals-1);
      float sin_factor;
      if(reverseramp) {
        x=1.0-x;
        sin_factor= 1.0 - sin (0.5*PII*x);
      } else sin_factor=  sin (0.5*PII*x);

      result[i]= beginVal + (endVal-beginVal)  * sin_factor;
    }
  }

  for(i=0; i<n_vals; i++) if(fabs(result[i])<1.0e-6) result[i]=0.0;
  return result;
}


unsigned int SeqGradRamp::npts4ramp(rampType type, float beginVal, float endVal, float maxIncrement) {
  int result=0;
  float delta=fabs(beginVal-endVal);

  if(type==linear) {
    float quot=secureDivision( delta , fabs(maxIncrement) );
    result=int(quot+0.5);
  }

  if(type==sinusoidal) {
    float quot=secureDivision( PII*delta , 2.0*fabs(maxIncrement) );
    result=int(quot+0.5);
  }

  if(type==half_sinusoidal) {
    float quot=secureDivision( PII*delta , 2.0*fabs(maxIncrement) );
    result=int(quot+0.5);
  }

  if(result<0) result=0;
  result++;
  return result;
}

unsigned int SeqGradRamp::npts4ramp(double totaldur, double timestep) {
  float quot=secureDivision( totaldur , timestep );
  int result=int(quot+0.5);
  if(result<1) result=1;
  return result;
}



SeqGradRamp::SeqGradRamp(const STD_string& object_label,direction gradchannel,
	         float initgradstrength, float finalgradstrength, double timestep,rampType type,
	         float steepnessfactor, bool reverse) :
     SeqGradWave(object_label,gradchannel,0.0,0.0,LDRfloatArr()) {
  Log<Seq> odinlog(this,"SeqGradRamp(1...)");
  initstrength=initgradstrength;
  finalstrength=finalgradstrength;
  dt=timestep;
  steepness=steepnessfactor;
  steepcontrol=true;
  ramptype=type;
  reverseramp=reverse;
  generate_ramp();
}


SeqGradRamp::SeqGradRamp(const STD_string& object_label,direction gradchannel,double gradduration,
	         float initgradstrength, float finalgradstrength, double timestep,
	         rampType type, bool reverse) :
     SeqGradWave(object_label,gradchannel,gradduration,0.0,LDRfloatArr()) {
  Log<Seq> odinlog(this,"SeqGradRamp(2...)");
  initstrength=initgradstrength;
  finalstrength=finalgradstrength;
  dt=timestep;
  steepness=secureDivision(fabs(finalgradstrength-initgradstrength), gradduration*systemInfo->get_max_slew_rate());
  ODINLOG(odinlog,normalDebug) << "steepness=" << steepness << STD_endl;
  steepcontrol=false;
  ramptype=type;
  reverseramp=reverse;
  generate_ramp();
}


SeqGradRamp::SeqGradRamp(const SeqGradRamp& sgr) {
  SeqGradRamp::operator = (sgr);
}


SeqGradRamp::SeqGradRamp(const STD_string& object_label) : SeqGradWave(object_label) {
  Log<Seq> odinlog(this,"SeqGradRamp(const STD_string&)");
  initstrength=0.0;
  finalstrength=0.0;
  dt=0.0;
  steepness=1.0;
  steepcontrol=false;
  ramptype=linear;
  reverseramp=false;
}

SeqGradRamp& SeqGradRamp::set_ramp(float initgradstrength, float finalgradstrength,
     double timestep,rampType type,float steepnessfactor, bool reverse) {
  initstrength=initgradstrength;
  finalstrength=finalgradstrength;
  dt=timestep;
  steepness=steepnessfactor;
  if(steepness==0.0) steepcontrol=false;
  else steepcontrol=true;
  ramptype=type;
  reverseramp=reverse;
  generate_ramp();
  return *this;
}

SeqGradRamp&  SeqGradRamp::set_ramp(double gradduration,float initgradstrength,
     float finalgradstrength, double timestep,rampType type, bool reverse) {
  Log<Seq> odinlog(this,"set_ramp");
  set_duration(gradduration);
  initstrength=initgradstrength;
  finalstrength=finalgradstrength;
  dt=timestep;
  steepness=secureDivision(fabs(finalgradstrength-initgradstrength), gradduration*systemInfo->get_max_slew_rate());
  ODINLOG(odinlog,normalDebug) << "steepness=" << steepness << STD_endl;
  steepcontrol=false;
  ramptype=type;
  reverseramp=reverse;
  generate_ramp();
  return *this;
}



SeqGradRamp& SeqGradRamp::operator = (const SeqGradRamp& sgr) {
  SeqGradWave::operator = (sgr);
  initstrength=sgr.initstrength;
  finalstrength=sgr.finalstrength;
  dt=sgr.dt;
  steepness=sgr.steepness;
  steepcontrol=sgr.steepcontrol;
  ramptype=sgr.ramptype;
  reverseramp=sgr.reverseramp;
  return *this;
}


SeqGradInterface& SeqGradRamp::set_strength(float gradstrength) {
  Log<Seq> odinlog(this,"set_strength");

  float oldstrength=SeqGradWave::get_strength();

  float maxstrength=secureDivision(fabs(oldstrength), steepness);

  float val=gradstrength;

  ODINLOG(odinlog,normalDebug) << "old/max/val/steepness=" << oldstrength << "/" << maxstrength << "/" << val << "/" << steepness << STD_endl;

  float sign=secureDivision(gradstrength, fabs(gradstrength));

  if(fabs(gradstrength) > fabs(maxstrength)) {

    val=sign*maxstrength;

    ODINLOG(odinlog,warningLog) << "limiting strength to " << val << STD_endl;
  }

  SeqGradWave::set_strength(val);

  return *this;
}


void SeqGradRamp::generate_ramp() {
  Log<Seq> odinlog(this,"generate_ramp");


  if(steepness<=0.0) {
    steepness=1.0;
  }

  if(steepness>1.0) {
    ODINLOG(odinlog,warningLog) << "steepness(" << steepness << ")>1, setting to 1" << STD_endl;
    steepness=1.0;
  }


  float gradstrength=0.0;
  bool init_is_max=false;
  if(fabs(initstrength) > fabs(gradstrength)) {init_is_max=true; gradstrength=initstrength;}
  if(fabs(finalstrength) > fabs(gradstrength)) gradstrength=finalstrength;
  SeqGradWave::set_strength(gradstrength);

  ODINLOG(odinlog,normalDebug) << "get_duration()/dt=" << get_duration() << "/" << dt << STD_endl;

  unsigned int npts;
  if (steepcontrol) {
    ODINLOG(odinlog,normalDebug) << "in steepcontrol mode" << STD_endl;
    double maxIncr=steepness*dt*systemInfo->get_max_slew_rate();
    ODINLOG(odinlog,normalDebug) << "maxIncr=" << maxIncr << STD_endl;
    npts=npts4ramp(ramptype,initstrength,finalstrength,maxIncr);
    ODINLOG(odinlog,normalDebug) << "npts=" << npts << STD_endl;
    set_duration(npts*dt);
  } else {
    ODINLOG(odinlog,normalDebug) << "NOT in steepcontrol mode" << STD_endl;

    npts=npts4ramp(get_duration(),dt);
    double maxIncr=dt*systemInfo->get_max_slew_rate();
    unsigned int npts_ramp=npts4ramp(ramptype,initstrength,finalstrength,maxIncr);

    ODINLOG(odinlog,normalDebug) << "initstrength/finalstrength=" << initstrength << "/" << finalstrength << STD_endl;
    ODINLOG(odinlog,normalDebug) << "npts/maxIncr/npts_ramp=" << npts << "/" << maxIncr << "/" << npts_ramp << STD_endl;

    if(npts_ramp>npts) {
      ODINLOG(odinlog,warningLog) << "ramp too short (" << (npts*dt) << "), setting to " << (npts_ramp*dt) << STD_endl;
      npts=npts_ramp;
      set_duration(npts*dt);
    }

  }

  fvector rampwav(npts);

  float rel_initstrength=secureDivision(initstrength,gradstrength);
  float rel_finalstrength=secureDivision(finalstrength,gradstrength);

  if(init_is_max) {
    if(rel_initstrength<0.0) {
      rel_initstrength=-rel_initstrength;
      rel_finalstrength=-rel_finalstrength;
    }
  } else {
    if(rel_finalstrength<0.0) {
      rel_initstrength=-rel_initstrength;
      rel_finalstrength=-rel_finalstrength;
    }
  }


  ODINLOG(odinlog,normalDebug) << "rel_initstrength/rel_finalstrength=" << rel_initstrength << "/" << rel_finalstrength << STD_endl;

  rampwav=makeGradRamp(ramptype,rel_initstrength,rel_finalstrength,npts,reverseramp);



  ODINLOG(odinlog,normalDebug) << "max/minvalue=" << rampwav.maxvalue() << "/" << rampwav.minvalue() << STD_endl;


  set_wave(rampwav);
}
