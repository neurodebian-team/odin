#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

#ifdef IDEA_PLUGIN
#include "../platforms/IDEA_n4/odinseq_idea/seqidea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqacq_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqacqepi_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqcounter_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqdec_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqdelay_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqfreq_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqgradchan_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqgradtrapez_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqlist_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqparallel_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqphase_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqpuls_idea.cpp"
#include "../platforms/IDEA_n4/odinseq_idea/seqtrigg_idea.cpp"
#endif
