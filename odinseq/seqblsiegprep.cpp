/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "seqblsiegprep.h"

SeqBlSiegPrep::SeqBlSiegPrep ( const STD_string& object_label,
                               float dur,
                               float fa,
                               float off,
                               float wd,
                               float sl ) : SeqPulsar ( object_label ) {
    set_shape ( "Fermi" );
    set_dim_mode ( zeroDeeMode );
    set_filter ( "NoFilter" );

    width = wd;
    width.set_description ( "Distance of turning points of Fermi shaped pulse" ).set_label ( "FermiWidth" );
    width.set_minmaxval ( 0,1 );
    pars.append ( width );

    slope = sl;
    slope.set_description ( "Slope of Fermi shaped pulse" ).set_label ( "FermiSlope" );
    slope.set_minmaxval ( 0,150 );
    pars.append ( slope );

    duration = dur;
    duration.set_description ( "Duration of the Fermi pulse" ).set_label ( "Duration" );
    duration.set_minmaxval ( 0,100 );
    pars.append ( duration );

    angle = fa;
    angle.set_description ( "Flipangle of Fermi pulse [ deg ]" ).set_label ( "Flipangle" );
    angle.set_minmaxval ( 0,1000 );
    pars.append ( angle );


    offset = off;
    offset.set_description ( "Frequency offset of Fermi pulse [ Hz ]" ).set_label ( "Offset" );
    offset.set_minmaxval ( -100000,100000 );
    pars.append ( offset );

    // set information parameters
    amplitude.set_description ( "Pulse Amplitude [ uT ]" ).set_label ( "PulseAmplitude" );
    amplitude.set_parmode ( noedit );
    info.append ( amplitude );

    weighting.set_description ( "Weighting factor (Info) in [rad / uT^2]" ).set_label ( "Weighting" );
    weighting.set_parmode ( noedit );
    info.append ( weighting );

    info.set_description ( "Infos about Bloch-Siegert preparation" ).set_label ( "Info" );

    pars.append ( info );
    pars.set_description ( "Parameters for the Bloch-Siegert preparation for B1-Mapping (see Sacolick et al. MRM(65)2010: 1315-1322)" );

    prep();

}

SeqBlSiegPrep::SeqBlSiegPrep ( const SeqBlSiegPrep& sbsp ) {
    SeqBlSiegPrep::operator=(sbsp);
}

bool SeqBlSiegPrep::prep() {
    Log<Seq> odinlog ( this,"prep" );

    set_shape ( "Fermi" );
    set_shape_parameter ( "slope",ftos ( slope ) );
    set_shape_parameter ( "width",ftos ( width ) );
    set_pulsduration ( duration );
    set_flipangle ( angle );
    set_freqoffset ( offset );


    // calculate the info parameters
    cvector rfWave = get_B1();
    weighting=0;
    for ( unsigned int i = 0; i< rfWave.size(); i++ ) {
        weighting += ( abs ( rfWave[i] ) *abs ( rfWave[i] ) );
        ODINLOG ( odinlog,verboseDebug ) << "abs(rfWave[" << i << "]) = " << abs ( rfWave[i] ) << STD_endl;
    }

    weighting *= duration/ rfWave.size() *                  // [ ms ]
                 pow ( get_systemInfo().get_gamma ( "1H" ),2 ) /  // [ ( rad * MHz / T ) ^ 2  ]
                 ( 2 * offset ) *                           // [ 1 / Hz / rad  ]
                 0.001 ;                                    // --> final unit : [ rad / (uT ^ 2) ]
    amplitude = get_B10() *1000;                            // [ uT ]

    return true;

}

SeqBlSiegPrep& SeqBlSiegPrep::operator= ( const SeqBlSiegPrep& sbsp ) {
    ( *this ).SeqPulsar::operator= ( sbsp );
    ( *this ).pars = sbsp.pars;
    ( *this ).info = sbsp.info;
    return *this;
}

