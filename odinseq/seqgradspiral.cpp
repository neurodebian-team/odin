#include "seqgradspiral.h"
#include "seqgradramp.h"
#include "odinpulse.h" // for _TRAJ_OPTIMIZE_PARLABEL_

#define TEST_NPTS 1000


float SeqGradSpiral::readout_npts() const {
  Log<Seq> odinlog(this,"readout_npts");

  if(!traj_cache) return -1.0;

  // make a test run to approximate the tangential step size, maximum gradient strengt and slew rate
  float deltaKtangential=0.0;

  float last_kx=0.0;
  float last_ky=0.0;

  float last_Gx=0.0;
  float last_Gy=0.0;

  float max_grad_magn=0.0;
  float max_grad_diff=0.0;

  for(unsigned int i=0; i<TEST_NPTS; i++) {
    float s=1.0-float(i)/float(TEST_NPTS-1);
    const kspace_coord& tds=traj_cache->calculate(s);
    if(i) {
      deltaKtangential=STD_max(deltaKtangential,float(norm(tds.kx-last_kx,tds.ky-last_ky))); // casts
      max_grad_diff=STD_max(max_grad_diff,float(fabs(tds.Gx-last_Gx)));                      // required
      max_grad_diff=STD_max(max_grad_diff,float(fabs(tds.Gy-last_Gy)));                      // for GCC6/C++11
    }

    max_grad_magn=STD_max(max_grad_magn,float(fabs(tds.Gx))); // casts required
    max_grad_magn=STD_max(max_grad_magn,float(fabs(tds.Gy))); // for GCC6/C++11

    last_kx=tds.kx;
    last_ky=tds.ky;
    last_Gx=tds.Gx;
    last_Gy=tds.Gy;
  }

  ODINLOG(odinlog,normalDebug) << "deltaKtangential/max_grad_magn/max_grad_diff = " << deltaKtangential << "/" << max_grad_magn << "/" << max_grad_diff << STD_endl;

  if(deltaKtangential==0.0) {
    ODINLOG(odinlog,errorLog) << "Zero trajectory" << STD_endl;
    return 0;
  }

  // the desired (relative) k-space step size
  float deltaKdesired=secureInv(sizeRadial_cache);

  // calculate the total number of points to get a sufficient coverage of k-space
  float npts=float(TEST_NPTS)*secureDivision(deltaKtangential,deltaKdesired);
  float k0=secureDivision(PII,resolution_cache);
  float gradfactor=secureDivision(k0, gamma_cache*npts*dt_cache);

  // Estimate maximum gradient strength and slew rate according to the test trajectory
  float dt_test=dt_cache*secureDivision(npts,TEST_NPTS);
  float maxgrad_test = max_grad_magn * gradfactor;
  float maxslew_test = secureDivision(max_grad_diff * gradfactor, dt_test);
  ODINLOG(odinlog,normalDebug) << "dt_test/maxgrad_test/maxslew_test = " << dt_test << " / " << maxgrad_test << " / " << maxslew_test << STD_endl;


  // Correct npts if max grad strength or slew is exceeded
  float prolongfactor=1.0;
  if(maxgrad_test>systemInfo->get_max_grad()) {
    prolongfactor=STD_max(double(prolongfactor),secureDivision(maxgrad_test,systemInfo->get_max_grad()));
  }
  if(maxslew_test>systemInfo->get_max_slew_rate()) {
    prolongfactor=STD_max(double(prolongfactor),secureDivision(maxslew_test,systemInfo->get_max_slew_rate()));
  }
  ODINLOG(odinlog,normalDebug) << "prolongfactor = " << prolongfactor << STD_endl;
  if(prolongfactor>1.0) npts*=prolongfactor;

  return npts;
}


float SeqGradSpiral::evaluate(const fvector& spirpar) const {
  Log<Seq> odinlog(this,"evaluate");
  if(!traj_cache) return -1.0;
  ODINLOG(odinlog,normalDebug) << "spirpar=" << spirpar[0] << STD_endl;
  if(!traj_cache->set_parameter(_TRAJ_OPTIMIZE_PARLABEL_,ftos(spirpar[0]))) {
    ODINLOG(odinlog,normalDebug) << _TRAJ_OPTIMIZE_PARLABEL_ << " not available" << STD_endl;
    return -1.0;
  }
  float result=readout_npts();
  ODINLOG(odinlog,normalDebug) << "result(" << spirpar[0] << ")=" << result << STD_endl;
  return result;
}



SeqGradSpiral::SeqGradSpiral(const STD_string& object_label, LDRtrajectory& traj, double dt, float resolution, unsigned int sizeRadial, unsigned int numofSegments, bool inwards, bool optimize, const STD_string& nucleus)
    : SeqGradChanParallel(object_label), predelay(0.0),
      dt_cache(dt), resolution_cache(resolution), sizeRadial_cache(sizeRadial), gamma_cache(systemInfo->get_gamma(nucleus)) {
  Log<Seq> odinlog(this,"SeqGradSpiral(...)");

  common_init();


  unsigned int i;

  if(traj.get_function_mode()!=twoDeeMode) {
   ODINLOG(odinlog,errorLog) << "traj has wrong funcMode" << STD_endl;
   return;
  }

  if(!numofSegments) numofSegments=1;

  // set the number of cycles according to the step size in radial direction
  traj.set_parameter("NumCycles",itos(int(secureDivision(sizeRadial,(2*numofSegments)))));

  traj_cache=&traj;

  if(optimize && traj.set_parameter(_TRAJ_OPTIMIZE_PARLABEL_,ftos(0.0))) { // perform optimization only if appropriate parameter is available

    float minpar=bruteforce_minimize1d(*this, 0.0, 1.0)[0];
    ODINLOG(odinlog,normalDebug) << "minpar=" << minpar << STD_endl;
    traj.set_parameter(_TRAJ_OPTIMIZE_PARLABEL_,ftos(minpar));
  }


  float fnpts=readout_npts();
  if(fnpts<=0.0) {
   ODINLOG(odinlog,errorLog) << "Cannot calculate readout length" << STD_endl;
   return;
  }
  unsigned int npts=(unsigned int)(fnpts+0.5);


  float k0=secureDivision(PII,resolution);
  float gradfactor=secureDivision(k0, gamma_cache*npts*dt);

  spiral_dur=npts*dt;

  ODINLOG(odinlog,normalDebug) << "npts / k0 / spiral_dur = " << npts << " / " << k0 << " / " << spiral_dur << STD_endl;

  if(spiral_dur==0.0) {
    ODINLOG(odinlog,errorLog) << "Zero duration spiral" << STD_endl;
    return;
  }

  fvector Gx(npts);
  fvector Gy(npts);
  denscomp.resize(npts);
  kx.resize(npts);
  ky.resize(npts);

  float sign=1.0;
  if(!inwards) sign=-1.0; // trajectory is traversed backwards
  // calculate the gradient arrays
  for (i=0; i<npts; i++) {
    float s=float(i)/float(npts-1);
    if(!inwards) s=1.0-s;
    const kspace_coord& tds=traj.calculate (s);
    Gx[i] = gradfactor*tds.Gx;
    Gy[i] = gradfactor*tds.Gy;
    kx[i] = sign*gamma_cache*gradfactor*spiral_dur*tds.kx;
    ky[i] = sign*gamma_cache*gradfactor*spiral_dur*tds.ky;
    denscomp[i] = /*norm(Gx[i],Gy[i])* */ tds.denscomp;
  }

  // normalize_gradient:
  float gradstrength = STD_max (Gx.maxabs(), Gy.maxabs());
  Gx/=gradstrength;
  Gy/=gradstrength;


  denscomp.normalize();

  // generate appropriate ramps
  SeqGradRamp max_ramp("max_ramp",readDirection,0.0,gradstrength,dt);
  double ramp_dur=max_ramp.get_duration();
  unsigned int ramp_npts=max_ramp.get_npts();
  ODINLOG(odinlog,normalDebug) << "ramp_dur / ramp_npts = " << ramp_dur << " / " << ramp_npts << STD_endl;

  SeqGradRamp Gx_ramp,Gy_ramp;
  if(inwards) {
    Gx_ramp=SeqGradRamp("Gx_ramp",readDirection, ramp_dur,0.0,gradstrength*Gx[0],dt);
    Gy_ramp=SeqGradRamp("Gy_ramp",phaseDirection,ramp_dur,0.0,gradstrength*Gy[0],dt);
  } else {
    Gx_ramp=SeqGradRamp("Gx_ramp",readDirection, ramp_dur,gradstrength*Gx[npts-1],0.0,dt);
    Gy_ramp=SeqGradRamp("Gy_ramp",phaseDirection,ramp_dur,gradstrength*Gy[npts-1],0.0,dt);
  }

  fvector Gx_with_ramp(npts+ramp_npts);
  fvector Gy_with_ramp(npts+ramp_npts);

  unsigned int offset=0;
  if(inwards) offset=ramp_npts;
  for(i=0; i<npts; i++) {
    Gx_with_ramp[offset+i]=Gx[i];
    Gy_with_ramp[offset+i]=Gy[i];
  }

  float gxscale=secureDivision(Gx_ramp.get_strength(),gradstrength);
  float gyscale=secureDivision(Gy_ramp.get_strength(),gradstrength);

  offset=npts;
  if(inwards) offset=0;
  for(i=0; i<ramp_npts; i++) {
    Gx_with_ramp[offset+i]=gxscale*Gx_ramp[i];
    Gy_with_ramp[offset+i]=gyscale*Gy_ramp[i];
  }

  double totaldur=spiral_dur+ramp_dur;
  ODINLOG(odinlog,normalDebug) << "totaldur = " << totaldur << STD_endl;

  gx=SeqGradWave(object_label+"_gx",readDirection, totaldur,gradstrength,Gx_with_ramp);
  gy=SeqGradWave(object_label+"_gy",phaseDirection,totaldur,gradstrength,Gy_with_ramp);


  gxdelay=SeqGradDelay(object_label+"_gxdelay",readDirection,  0.0);
  gydelay=SeqGradDelay(object_label+"_gydelay",phaseDirection, 0.0);


  build_seq();
}

SeqGradSpiral::SeqGradSpiral(const STD_string& object_label) : SeqGradChanParallel(object_label) {
  common_init();
}

SeqGradSpiral::SeqGradSpiral(const SeqGradSpiral& sgs) {
  common_init();
  SeqGradSpiral::operator = (sgs);
}

SeqGradSpiral& SeqGradSpiral::operator = (const SeqGradSpiral& sgs) {
  SeqGradChanParallel::operator = (sgs);
  gx=sgs.gx;
  gy=sgs.gy;
  gxdelay=sgs.gxdelay;
  gydelay=sgs.gydelay;
  kx=sgs.kx;
  ky=sgs.ky;
  denscomp=sgs.denscomp;
  spiral_dur=sgs.spiral_dur;
  predelay=sgs.predelay;
  build_seq();
  return *this;
}

SeqGradSpiral& SeqGradSpiral::set_predelay_duration(double dur) {
  predelay=dur;
  build_seq();
  return *this;
}


fvector SeqGradSpiral::get_ktraj(direction channel) const {
  Log<Seq> odinlog(this,"get_ktraj");

  fvector result(spiral_size());
  result=0.0;
  if(channel==readDirection)  result=kx;
  if(channel==phaseDirection) result=ky;
  return result;
}


void SeqGradSpiral::build_seq() {
  clear();

  gxdelay.set_duration(predelay);
  gydelay.set_duration(predelay);

  if(predelay>0.0) {
    (*this) += (gxdelay+gx) /
               (gydelay+gy);
  } else {
    (*this) += gx / gy;
  }
}
