/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SEQBLSIEGPREP_H
#define SEQBLSIEGPREP_H

#include <odinseq/seqpulsar.h>



/**
  * @addtogroup odinseq
  * @{
  */


/**
  * \brief Bloch-Siegert preparation module for B1-mapping
  *
  * Preparation module for B1 mapping according to
  * Sacolick et al. MRM(65)2010: 1315-1322):
  * Fermi-shaped pulse which is applied off-resonance to produce a
  * B0-shift depending on the Fermi-shaped pulse's B1 distribution
  */
class SeqBlSiegPrep : public SeqPulsar {
    public:
        /**
          * Constructs a Fermi-shaped pulse with the following properties:
          * - duration [ms]
          * - angle [degree]
          * - offset [Hz]
          * - width: fractional distance of the turning points of the fermipulse
          * - slope: steepnes of the ramp of the fermipulse
          */
        SeqBlSiegPrep ( const STD_string& object_label = "unnamedSeqBlSiegPrep",
                        float duration = 8,
                        float angle = 1000,
                        float offset = 4000,
                        float width = 0.8,
                        float slope = 130 );
        /**
          * constructs a copy of "sbsp"
          */
        SeqBlSiegPrep ( const SeqBlSiegPrep& sbsp );

        /**
          * copies "sbsp"
          */
        SeqBlSiegPrep& operator = ( const SeqBlSiegPrep& sbsp );

        /**
          * destructor
          */
        ~SeqBlSiegPrep() {}

        /**
          * prepares the Fermi-shaped rf-pulse -> ready for use
          */
        bool prep();

        LDRblock pars;
        LDRblock info;
        LDRdouble duration;
        LDRdouble angle;
        LDRdouble offset;
        LDRdouble width;
        LDRdouble slope;
        LDRdouble weighting;
        LDRdouble amplitude;

    private:
        

};

/** @}
*/
#endif // SEQBLSIEGPREP_H
