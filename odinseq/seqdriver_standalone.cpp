#ifndef TJUTILS_CONFIG_H
#define TJUTILS_CONFIG_H
#include <tjutils/config.h>
#endif

#ifdef STANDALONE_PLUGIN
#include "../platforms/StandAlone/odinseq_standalone/seqplot_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqstandalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqacq_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqacqepi_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqcounter_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqdec_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqdelay_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqfreq_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqgradchan_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqgradtrapez_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqlist_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqparallel_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqphase_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqpuls_standalone.cpp"
#include "../platforms/StandAlone/odinseq_standalone/seqtrigg_standalone.cpp"
#endif
