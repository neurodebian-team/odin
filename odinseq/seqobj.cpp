#include "seqobj.h"


SeqObjBase::SeqObjBase(const STD_string& object_label)
: SeqTreeObj() {
  Log<Seq> odinlog(object_label.c_str(),"SeqObjBase()");
  set_label(object_label);
}

SeqObjBase::SeqObjBase(const SeqObjBase& soa) {
  SeqObjBase::operator = (soa);
}


SeqObjBase& SeqObjBase::operator = (const SeqObjBase& soa) {
  SeqTreeObj::operator = (soa);
  return *this;
}


