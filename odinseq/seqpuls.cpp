#include "seqpuls.h"
#include "seqdelay.h"

#include <tjutils/tjfeedback.h>



SeqPulsInterface& SeqPulsInterface::set_flipangles(const fvector& flipangles) {
  float ang=get_flipangle();
  fvector scales(flipangles.size());
  if(ang) scales=flipangles/ang;
  else scales=0.0;
  set_flipscales(scales);
  return *this;
}


///////////////////////////////////////////////////////////////////////////////////


bool SeqFlipAngVector::prep_iteration() const {
  if(user) {
    return user->pulsdriver->prep_flipangle_iteration(get_current_index());
  }
  return true;
}

svector SeqFlipAngVector::get_vector_commands(const STD_string& iterator) const {
  svector result;
  if(user) result=user->pulsdriver->get_flipvector_commands(iterator);
  return result;
}


///////////////////////////////////////////////////////////////////////////////////


SeqPuls::SeqPuls(const STD_string& object_label,const cvector& waveform,float pulsduration,
            float pulspower,const STD_string& nucleus,
            const dvector& phaselist,const dvector& freqlist,float rel_magnetic_center)
          : SeqObjBase(object_label),
            SeqFreqChan(object_label,nucleus,freqlist,phaselist), SeqDur(object_label,pulsduration),
            pulsdriver(object_label), flipvec(object_label+"_flipvec",this) {
  Log<Seq> odinlog(this,"SeqPuls(...)");
  wave=waveform;
  power=pulspower;
  system_flipangle=90.0;
  B1max_mT=0.0;
  relmagcent=rel_magnetic_center;
}

SeqPuls::SeqPuls(const STD_string& object_label)
          : SeqObjBase(object_label),
            SeqFreqChan(object_label),
            SeqDur(object_label), pulsdriver(object_label), flipvec(object_label+"_flipvec",this) {
  power=0.0;
  system_flipangle=90.0;
  B1max_mT=0.0;
  relmagcent=0.5;
}

SeqPuls::SeqPuls(const SeqPuls& sp) : flipvec(STD_string(sp.get_label())+"_flipvec",this) {
  SeqPuls::operator = (sp);
}


SeqPuls& SeqPuls::set_wave(const cvector& waveform) {
  wave=waveform;
  return *this;
}

double SeqPuls::get_duration() const {
  double result=get_pulsstart()+get_pulsduration();
  result+=pulsdriver->get_postdelay();
  return result;
}


SeqPulsInterface& SeqPuls::set_pulsduration(float pulsduration) {
  Log<Seq> odinlog(this,"SeqPuls::set_pulsduration");
  ODINLOG(odinlog,normalDebug) << "=" << pulsduration << STD_endl;
  SeqDur::set_duration(pulsduration);
  return *this;
}

double SeqPuls::get_pulsduration() const {
  Log<Seq> odinlog(this,"SeqPuls::get_pulsduration");
  ODINLOG(odinlog,normalDebug) << "=" << SeqDur::get_duration() << STD_endl;
  return  SeqDur::get_duration();
}


float SeqPuls::get_magnetic_center() const {
  Log<Seq> odinlog(this,"get_magnetic_center");
  ODINLOG(odinlog,normalDebug) << "relmagcent=" << relmagcent << STD_endl;
  ODINLOG(odinlog,normalDebug) << "get_pulsduration()=" << get_pulsduration() << STD_endl;
  ODINLOG(odinlog,normalDebug) << "get_pulsstart()=" << get_pulsstart() << STD_endl;
  return  get_pulsstart()+relmagcent*get_pulsduration();
}


bool SeqPuls::prep() {
  Log<Seq> odinlog(this,"prep");

  if(!SeqFreqChan::prep()) return false;

  if(wave.length()==0) {
    ODINLOG(odinlog,warningLog) << "Empty waveform" << STD_endl;
  }
  if(wave.maxabs()==STD_complex(0,0)) {
    ODINLOG(odinlog,warningLog) << "Zero filled waveform" << STD_endl;
  }

  ODINLOG(odinlog,normalDebug) << "B1max_mT=" << B1max_mT << STD_endl;

  fvector flipscale=flipvec.flipanglescale;
  ODINLOG(odinlog,normalDebug) << "flipscale=" << flipscale.printbody() << STD_endl;

  return pulsdriver->prep_driver(wave,get_pulsduration(),get_magnetic_center(),B1max_mT,power,system_flipangle,flipscale,plstype);
}


STD_string SeqPuls::get_program(programContext& context) const {
  STD_string result=SeqFreqChan::get_pre_program(context,pulsObj,pulsdriver->get_instr_label());
  result+=pulsdriver->get_program(context,get_phaselistindex(),get_channel(),get_iteratorcommand(pulsObj));
  return result;
}


SeqPuls& SeqPuls::set_B1max(float b1max) {
  Log<Seq> odinlog(this,"set_B1max");
  B1max_mT=b1max;
  ODINLOG(odinlog,normalDebug) << "B1max_mT=" << B1max_mT << STD_endl;
  return *this;
}


SeqValList SeqPuls::get_freqvallist(freqlistAction action) const {
  Log<Seq> odinlog(this,"get_freqvallist");
  SeqValList result(get_label());
  double newfreq=SeqFreqChan::get_frequency();

  if(action==calcDeps) pulsdriver->new_freq(newfreq);

  if(action==calcList) {
    if(pulsdriver->has_new_freq()) result.set_value(newfreq);
  }
  return result;
}


SeqPuls& SeqPuls::set_system_flipangle(float angle) {
  system_flipangle=angle;
  return *this;
}

double SeqPuls::get_rf_energy() const {
  return pulsdriver->get_rf_energy();
}

SeqPuls& SeqPuls::operator = (const SeqPuls& sp) {
  Log<Seq> odinlog(this,"operator = ");
  SeqObjBase::operator = (sp);
  SeqFreqChan::operator = (sp);
  SeqDur::operator = (sp);
  pulsdriver=sp.pulsdriver;
  wave=sp.wave;
  power=sp.power;
  system_flipangle=sp.system_flipangle;
  B1max_mT=sp.B1max_mT;
  ODINLOG(odinlog,normalDebug) << "B1max_mT=" << B1max_mT << STD_endl;
  relmagcent=sp.relmagcent;
  plstype=sp.plstype;
  return *this;
}



STD_string SeqPuls::get_properties() const {
  return "Samples="+itos(wave.length())+", B1="+ftos(B1max_mT);
}

SeqPulsInterface& SeqPuls::set_pulse_type(pulseType type) {
  plstype=type;
  return *this;
}

pulseType SeqPuls::get_pulse_type() const {return plstype;}


unsigned int SeqPuls::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  double start=context.elapsed+get_pulsstart();
  SeqTreeObj::event(context);

  if(context.action==seqRun) {
    ODINLOG(odinlog,normalDebug) << "start=" << start << STD_endl;
    SeqFreqChan::pre_event(context,start);
    pulsdriver->event(context,start);
    SeqFreqChan::post_event(context,start+get_pulsduration());
  }
  context.increase_progmeter();
  return 1;
}

