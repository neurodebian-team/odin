#include "seqgradchanlist.h"
#include "seqparallel.h"
#include "seqgradchanparallel.h"

#include <tjutils/tjhandler_code.h>


void bad_serial(const Labeled& s1, const Labeled& s2) {
  Log<Seq> odinlog("","bad_serial");
  ODINLOG(odinlog,errorLog) << s1.get_label() << "+=" << s2.get_label() << ": different channels";
}



SeqGradChanList::SeqGradChanList(const STD_string& object_label) {
  set_label(object_label);
}

SeqGradChanList::SeqGradChanList(const SeqGradChanList& sgcl) {
  Log<Seq> odinlog(this,"SeqGradChanList");
  SeqGradChanList::operator = (sgcl);
}


SeqGradChanList::~SeqGradChanList() {
  clear();
}


SeqGradChanList& SeqGradChanList::operator = (const SeqGradChanList& sgcl) {
  Log<Seq> odinlog(this,"operator = (...)");
  SeqTreeObj::operator = (sgcl);
  clear();
  for(constiter it=sgcl.get_const_begin();it!=sgcl.get_const_end();++it) {
    append(**it);
  }
  return *this;
}


double SeqGradChanList::get_duration() const {
  Log<Seq> odinlog(this,"SeqGradChanList::get_duration");
  SeqGradChanList dummylist(*this);
  SeqGradChanParallel dummypar;
  dummypar+=dummylist;
  SeqParallel dummy;
  dummy.set_gradptr((SeqGradObjInterface*)&dummypar);    // will use SeqParallel to calculate duration
  ODINLOG(odinlog,normalDebug) << "duration=" << dummy.get_duration() << STD_endl;
  return dummy.get_duration();
}

direction SeqGradChanList::get_channel() const {
  Log<Seq> odinlog(this,"get_channel");
  if(size()) return (*get_const_begin())->get_channel();
  else return readDirection;
}


fvector SeqGradChanList::get_gradintegral() const {
  fvector result(3);
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    result=result+(*it)->get_gradintegral();
  }
  return result;
}




SeqGradInterface& SeqGradChanList::set_strength(float gradstrength) {
  Log<Seq> odinlog(this,"set_strength");
  for(iter it=get_begin();it!=get_end();++it) {
    (*it)->set_strength(gradstrength);
  }
  return *this;
}

SeqGradInterface& SeqGradChanList::invert_strength() {
  Log<Seq> odinlog(this,"invert_strength");
  for(iter it=get_begin();it!=get_end();++it) {
    (*it)->invert_strength();
  }
  return *this;
}


float SeqGradChanList::get_strength() const {
  Log<Seq> odinlog(this,"get_strength");
  float result=0.0;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    float tmp=(*it)->get_strength();
    if(fabs(tmp)>fabs(result)) result=tmp;
  }
  return result;
}

double SeqGradChanList::get_gradduration() const {
  Log<Seq> odinlog(this,"SeqGradChanList::get_gradduration");
  double result=0.0;
  ODINLOG(odinlog,normalDebug) << "size()=" << size() << STD_endl;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    ODINLOG(odinlog,normalDebug) << "dur[" << (*it)->get_label() << "]=" << (*it)->get_gradduration() << STD_endl;
    result+=(*it)->get_gradduration();
  }
  return result;
}

SeqGradInterface& SeqGradChanList::set_gradrotmatrix(const RotMatrix& matrix) {
  Log<Seq> odinlog(this,"set_gradrotmatrix");
  for(iter it=get_begin();it!=get_end();++it) (*it)->set_gradrotmatrix(matrix);
  return *this;
}





SeqGradChanList& SeqGradChanList::operator += (SeqGradChanList& sgcl) {
  Log<Seq> odinlog(this,"SeqGradChanList::operator += (SeqGradChanList)");

  if(size() && sgcl.size()) {
    if(get_channel()!=sgcl.get_channel()) {
      bad_serial(*this,sgcl);
      return *this;
    }
  }

  constiter it; // avoid redefenition error with VC6
  SeqGradChanList chanlistbuff; // use buffer to avoid deadlocks
  for(it=sgcl.get_const_begin();it!=sgcl.get_const_end();++it) {
    ODINLOG(odinlog,normalDebug) <<  "appending >" << (*it)->get_label() << "<" << STD_endl;
    chanlistbuff+=(**it);
  }

  for(it=chanlistbuff.get_const_begin();it!=chanlistbuff.get_const_end();++it) {
    ODINLOG(odinlog,normalDebug) <<  "appending >" << (*it)->get_label() << "<" << STD_endl;
    (*this)+=(**it);
  }

  return *this;
}

SeqGradChanList& SeqGradChanList::operator += (SeqGradChan& sgc) {
  Log<Seq> odinlog(this,"SeqGradChanList::operator += (SeqGradChan)");
  ODINLOG(odinlog,normalDebug) <<  "appending >" << sgc.get_label() << "<" << STD_endl;

  if(size()) {
    if(get_channel()!=sgc.get_channel()) {
      bad_serial(*this,sgc);
      return *this;
    }
  }

  append(sgc);
  return *this;
}

void SeqGradChanList::clear_container() {clear();}


unsigned int SeqGradChanList::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  unsigned int result=0;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    result+=(*it)->event(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
  }
  return result;
}


void SeqGradChanList::query(queryContext& context) const {
  SeqTreeObj::query(context); // defaults
  if(context.action==count_acqs) return; // nothing to do

  context.treelevel++;

  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    context.parentnode=this; // reset to this because it might changed by last query
    (*it)->query(context);
  }

  context.treelevel--;
}




STD_string SeqGradChanList::get_properties() const {
  return "NumOfChanObjs="+itos(size());
}

fvector SeqGradChanList::get_switchpoints() const {
  Log<Seq> odinlog(this,"get_switchpoints");
  fvector result(size());
  double elapsed_sp=0.0;
  unsigned int index=0;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    elapsed_sp+=(*it)->get_gradduration();
    result[index]=elapsed_sp;
    ODINLOG(odinlog,normalDebug) << "switchpoint(" << (*it)->get_label() << ")=" << elapsed_sp << STD_endl;
    index++;
  }
  return result;
}




SeqGradChanList& SeqGradChanList::get_chanlist4gp(const fvector& switchpoints) {
  Log<Seq> odinlog(this,"get_chanlist4gp");
  ODINLOG(odinlog,normalDebug) << "switchpoints=" << (1000.0*switchpoints).printbody() << STD_endl;

  SeqGradChanList *retso=new SeqGradChanList(STD_string(get_label())+"_4gp");
  retso->set_temporary();
  double elapsed_sp=0.0;
  for(unsigned int isp=0; isp<switchpoints.size(); isp++) {
    double startelapsed=elapsed_sp;
    elapsed_sp=switchpoints[isp];
    double midtime=0.5*(startelapsed+elapsed_sp);
    int graddur=int((elapsed_sp-startelapsed)*1000.0+0.5);  // in us
    double chanstart;
    SeqGradChan* chanptr=get_chan(chanstart,midtime);
    double starttime=startelapsed-chanstart;
    double endtime=elapsed_sp-chanstart;
    ODINLOG(odinlog,normalDebug) << "starttime/endtime=" << 1000.0*starttime << "/" << 1000.0*endtime << STD_endl;

    if(chanptr) {
      if(int(chanptr->get_gradduration()*1000.0+0.5)==graddur) retso->append(*chanptr);
      else {
        SeqGradChan& chanseg=chanptr->get_subchan(startelapsed-chanstart,elapsed_sp-chanstart);
        chanseg.set_gradrotmatrix(chanptr->gradrotmatrix);
        retso->append(chanseg);
      }
    }
  }
  return *retso;
}


SeqGradChan* SeqGradChanList::get_chan(double& chanstart, double midtime) {
  Log<Seq> odinlog(this,"get_chan");
  SeqGradChan* result=0;
  double elapsed_sp=0.0;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    double startelapsed=elapsed_sp;
    elapsed_sp+=(*it)->get_gradduration();
    if( (midtime>startelapsed) && (midtime<elapsed_sp) ) {
      chanstart=startelapsed;
      result=(*it);
    }
  }
  return result;
}

template class Handler<SeqGradChanList*>;
