#include "seqveciter.h"
#include "seqdelay.h"

SeqVecIter::SeqVecIter(const STD_string& object_label, unsigned int start)
 : SeqCounter(object_label), SeqObjBase(object_label), startindex(start) {
}


SeqVecIter::SeqVecIter(const SeqVecIter& svi) : startindex(0) {
  SeqVecIter::operator = (svi);
}


SeqVecIter& SeqVecIter::operator = (const SeqVecIter& svi) {
  SeqCounter::operator = (svi);
  SeqObjBase::operator = (svi);
  startindex=svi.startindex;
  return *this;
}


STD_string SeqVecIter::get_program(programContext& context) const {
  counterdriver->outdate_cache();
  counterdriver->update_driver(this,0,&vectors);
  return counterdriver->get_program_iterator(context);
}


double SeqVecIter::get_duration() const {
  counterdriver->update_driver(this,0,&vectors);
  return counterdriver->get_postduration_inloop();
}


unsigned int SeqVecIter::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");

  unsigned int result=0;

  counterdriver->update_driver(this,0,&vectors);

  if(context.action==seqRun) {

    if(!context.seqcheck ) { // do not increment in test run
      increment_counter();
      if(get_counter()>=get_times()) init_counter(); // cyclical iteration
    }

    // prep next iteration
    counterdriver->pre_vecprepevent(context);
    prep_veciterations();
    counterdriver->post_vecprepevent(context,-1);
  }

  double iteratordur=counterdriver->get_postduration_inloop();
  if(iteratordur) {
    SeqDelay sd("iteratordur",iteratordur);
    result+=sd.event(context);
  }

  return result;
}


STD_string SeqVecIter::get_properties() const {
  return "VecSize="+itos(get_times())+", NumOfVectors="+itos(n_vectors())+", "+SeqObjBase::get_properties();
}


bool SeqVecIter::is_acq_iterator() const {
  Log<Seq> odinlog(this,"is_acq_iterator");
  bool result=false;
  for(veciter=get_vecbegin(); veciter!=get_vecend(); ++veciter) {
    if((*veciter)->is_acq_vector()) {
      result=true;
      ODINLOG(odinlog,normalDebug) << "detected acq iterator" << STD_endl;
      break;
    }
  }
  return result;
}


void SeqVecIter::query(queryContext& context) const {
  Log<Seq> odinlog(this,"query");

  SeqCounter::query(context); // default

  if(context.action==check_acq_iter) context.check_acq_iter_result=is_acq_iterator();
}


RecoValList SeqVecIter::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  Log<Seq> odinlog(this,"get_recovallist");
  RecoValList dummy;

  if(is_acq_iterator()) {
    counterdriver->update_driver(this,0,&vectors);

    increment_counter();
    if(get_counter()>=get_times()) init_counter(); // cyclical iteration

    // prep next iteration
    prep_veciterations();
  }

  return dummy;
}


bool  SeqVecIter::prep() {
  if(!SeqObjBase::prep()) return false;
  if(!SeqCounter::prep()) return false;

  init_counter(startindex); // set to first element
  if(!prep_veciterations()) return false;; // prep 1st iteration

  return true;
}


