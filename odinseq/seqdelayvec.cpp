#include "seqdelayvec.h"

SeqDelayVector::SeqDelayVector(const STD_string& object_label,const dvector& delaylist) :
    SeqObjBase(object_label), SeqVector(object_label), delayvecdriver(object_label), delayvec(delaylist) {
}


SeqDelayVector::SeqDelayVector(const STD_string& object_label) :
    SeqObjBase(object_label), SeqVector(object_label), delayvecdriver(object_label) {
}

SeqDelayVector::SeqDelayVector(const SeqDelayVector& sdv) {
  SeqDelayVector::operator = (sdv);
}

SeqDelayVector& SeqDelayVector::operator = (const SeqDelayVector& sdv) {
  SeqObjBase::operator = (sdv);
  SeqVector::operator = (sdv);
  delayvecdriver=sdv.delayvecdriver;
  delayvec=sdv.delayvec;
  return *this;
}

SeqDelayVector& SeqDelayVector::set_delayvector(const dvector& delays) {
  delayvec=delays;
  return *this;
}

dvector SeqDelayVector::get_delayvector() const {return  delayvec;}

double SeqDelayVector::get_duration() const {
  double result=systemInfo->get_min_duration(delayObj);
  double value=0.0;
  if(get_vectorsize()) {
    value=delayvec[get_current_index()];
  }
  if(value>result) result=value;
  return result;
}

STD_string SeqDelayVector::get_program(programContext& context) const {
  Log<Seq> odinlog(this,"get_program");
  double delaydur=0.0;
  if(get_vectorsize()) {
    delaydur=delayvec[get_current_index()];
  }
  ODINLOG(odinlog,normalDebug) << "context.mode/delaydur=" << context.mode << "/" << delaydur << STD_endl;
  return delayvecdriver->get_program(context,delaydur);
}


SeqValList SeqDelayVector::get_delayvallist() const {
  Log<Seq> odinlog(this,"get_delayvallist");
  SeqValList result(get_label());
  result.set_value(get_duration());
  return result;
}


unsigned int SeqDelayVector::get_vectorsize() const {return delayvec.size();}

bool SeqDelayVector::prep() {
  if(!SeqObjBase::prep()) return false;
  return delayvecdriver->prep_driver();
}

