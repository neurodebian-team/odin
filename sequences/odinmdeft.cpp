#include <odinseq/seqall.h>


#define DUMMY_SCANS 3

#define INTER_ACQ_DELAY 2.0


class METHOD_CLASS : public SeqMethod {

 public:
  METHOD_CLASS(const STD_string& label);

  // implementing virtual functions of SeqMethod
  void method_pars_init();
  void method_seq_init();
  void method_rels();
  void method_pars_set();


 private:

  SeqPulsar inv;
  SeqPulsar exc;

  SeqGradEcho grech;

  SeqDelay inv2exc;
  SeqDelay exc2inv;
  SeqDelay dummypad;
  SeqDelay interacq;

  SeqGradConstPulse spoiler;


  SeqObjLoop segloop;
  SeqObjLoop peloop;
  SeqObjLoop sliceloop;
  SeqObjLoop dummyloop;

  SeqObjList prep;
  SeqObjList kernel;
  SeqObjList readout;
  SeqObjList dummyreadout;
  SeqObjList scanseq;
};

/////////////////////////////////////////////////////////////////////////////

METHOD_CLASS::METHOD_CLASS (const STD_string& label)
                                   : SeqMethod(label) {

  set_description("Multi-slice MDEFT sequence for high-contrast T1-weighted images (DG Norris, J Magn Reson Imag 11:445-451 (2000)).");

}


void METHOD_CLASS::method_pars_init() {

  // default values
  commonPars->set_RepetitionTime(1300.0);
  commonPars->set_MatrixSize(phaseDirection,128);
  commonPars->set_MatrixSize(readDirection,128,noedit);
  commonPars->set_EchoTime(0.0,noedit);

}



void METHOD_CLASS::method_seq_init() {
  Log<Seq> odinlog(this,"method_seq_init");


  ///////////////// Inversion Pulse: /////////////////////

  inv=SeqPulsar("inv",false,false);
  inv.set_dim_mode(zeroDeeMode);
  inv.set_Tp(10.0);
  inv.resize(256);
  inv.set_shape("Wurst(2.96,26.48)");  // bandwidth ~ 1kHz
  inv.set_pulse_type(inversion);
  inv.refresh(); // because it's non-interactive

  ODINLOG(odinlog,normalDebug) << "Inversion Pulse done" << STD_endl;


  ///////////////// Excitation Pulses and rotation matrix of pilot: /////////////////////

  exc=SeqPulsarSinc("exc",geometryInfo->get_sliceThickness(),false);
  exc.set_freqlist(systemInfo->get_gamma()*exc.get_strength()/(2.0*PII)*geometryInfo->get_sliceOffsetVector());


  ////////////////// Geometry: /////////////////////////////////

  // condition for reduced power multi-slice MDEFT
  unsigned int pesize=commonPars->get_MatrixSize(phaseDirection)/geometryInfo->get_nSlices();
  commonPars->set_MatrixSize(phaseDirection,pesize*geometryInfo->get_nSlices());
  ODINLOG(odinlog,normalDebug) << "MatrixSizePhase=" << commonPars->get_MatrixSize(phaseDirection) << STD_endl;

  // uniform resolution in read and phase direction
  // Does not work on Siemens as ADC can only have certain sizes
  if(systemInfo->get_platform()!=numaris_4) {
    float resolution=secureDivision( geometryInfo->get_FOV(phaseDirection) , commonPars->get_MatrixSize(phaseDirection) );
    commonPars->set_MatrixSize(readDirection,int(secureDivision(geometryInfo->get_FOV(readDirection),resolution)+0.5),noedit);
  }


  ///////////////// Delays: /////////////////////

  inv2exc=SeqDelay("inv2exc");

  exc2inv=SeqDelay("exc2inv");

  dummypad=SeqDelay("dummypad");

  interacq=SeqDelay("interacq",INTER_ACQ_DELAY);

  ODINLOG(odinlog,normalDebug) << "Delays done" << STD_endl;


  ///////////////// Spoiler Gradient: /////////////////////

  spoiler=SeqGradConstPulse("spoiler",readDirection,0.5*systemInfo->get_max_grad(),5.0);


  ODINLOG(odinlog,normalDebug) << "Spoiler Gradient done" << STD_endl;


  //////////////// Preparation: ///////////////

  prep=SeqObjList("prep");

  prep= exc2inv + inv + spoiler + inv2exc ;

  ODINLOG(odinlog,normalDebug) << "Preparation done" << STD_endl;



  //////////////// Loops: ///////////////

  peloop=SeqObjLoop("peloop");

  sliceloop=SeqObjLoop("sliceloop");

  segloop=SeqObjLoop("segloop");

  dummyloop=SeqObjLoop("dummyloop");





  //////////////// Gradient Echo Module (2D mode): //////////////////////////////

  grech=SeqGradEcho("grech", exc, commonPars->get_AcqSweepWidth(),
              commonPars->get_MatrixSize(readDirection), geometryInfo->get_FOV(readDirection),
              commonPars->get_MatrixSize(phaseDirection), geometryInfo->get_FOV(phaseDirection),
              linearEncoding, blockedSegmented, geometryInfo->get_nSlices(), commonPars->get_ReductionFactor(),
              DEFAULT_ACL_BANDS, false, commonPars->get_PartialFourier());

  grech.set_freq_reorder_scheme(rotateReorder); // rotate slice order

  ODINLOG(odinlog,normalDebug) << "kernel done" << STD_endl;



  //////////////// Constructing the Sequence: ///////////////

  kernel=SeqObjList("kernel");

  readout=SeqObjList("readout");

  dummyreadout=SeqObjList("dummyreadout");

  scanseq=SeqObjList("scanseq");

  kernel = grech;


  kernel += interacq;  // Reuired to avoid SAR issue on Siemens

  // iterate over slices
  readout = sliceloop (
              kernel
            ) [grech.get_exc_vector()][grech.get_pe_reorder_vector()];

  dummyreadout = sliceloop( exc + dummypad )[exc];


  // spoiler after each pack of slices
  readout += spoiler;

  scanseq =
    segloop (
      peloop (
        prep + readout
      ) [grech.get_pe_vector()]
    ) [grech.get_freq_reorder_vector()];



  set_sequence( dummyloop( prep + dummyreadout )[DUMMY_SCANS] + scanseq );

  ODINLOG(odinlog,normalDebug) << "set_sequence done" << STD_endl;

}


void METHOD_CLASS::method_rels() {
  Log<Seq> odinlog(this,"method_rels");


  //////////////// Timings for MDEFT condition: ///////////////

  float readoutduration=readout.get_duration();

  float inv2exc_duration=( inv.get_duration() - inv.get_magnetic_center() ) + spoiler.get_duration() + 0.5*readoutduration;
  inv2exc.set_duration(0.5*commonPars->get_RepetitionTime()-inv2exc_duration);

  float exc2inv_duration= inv.get_magnetic_center() + 0.5*readoutduration;
  exc2inv.set_duration(0.5*commonPars->get_RepetitionTime()-exc2inv_duration);

  dummypad=secureDivision(readoutduration,geometryInfo->get_nSlices())-exc.get_duration();

  commonPars->set_EchoTime(grech.get_echo_time(),noedit);
}



void METHOD_CLASS::method_pars_set() {
}



/////////////////////////////////////////////////////////////////////////////

// entry point for the sequence module
ODINMETHOD_ENTRY_POINT

