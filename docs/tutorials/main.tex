\documentclass{article}

%\usepackage{pslatex} % math font does not work
\usepackage{latexsym}
%\usepackage[german,american]{babel}
\usepackage{amsbsy}
\usepackage{amsmath}
\usepackage[dvips]{graphicx}
\usepackage{epsfig}
\usepackage{wrapfig}
\usepackage{graphics}
\usepackage{graphicx}
%\usepackage{umlaut}
\usepackage{makeidx}
\usepackage{fancyhdr}
\usepackage{psfrag}
\usepackage{amsfonts}
\usepackage{cite}
\usepackage{index}
%\usepackage{doublespace}
\usepackage{eurosym}
\usepackage{xspace}
\usepackage{ulem}
\usepackage{moreverb}

% more space on page
%\addtolength{\voffset}{-1.5cm}
\addtolength{\textheight}{3.0cm}
\addtolength{\hoffset}{-1.0cm}
\addtolength{\textwidth}{2.0cm}

% More compact headings
\usepackage[compact]{titlesec}


% Lamport's page style:
\pagestyle{fancy}
\addtolength{\headwidth}{\marginparsep}
\addtolength{\headwidth}{\marginparwidth}
%\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancyhf{}
\fancyhead[LE,RO]{\bfseries\thepage}
\fancyhead[LO]{\bfseries\rightmark}
\fancyhead[RE]{\bfseries\leftmark}
\fancypagestyle{plain}{
   \fancyhead{} 
   \renewcommand{\headrulewidth}{0pt}
}

% Arial font, either one of these lines works
\renewcommand{\rmdefault}{phv}\renewcommand{\sfdefault}{phv}
%\usepackage{helvet} \renewcommand{\familydefault}{\sfdefault} 


\newcommand{\refsec}[1]{section \ref{#1}}
\newcommand{\reffig}[1]{Fig. \ref{#1}}



% keywords
\newcommand{\keyword}[1]{\emph{#1}\index[ind]{#1}}


\newcommand{\srckeyword}[1]{\keyword{\tt#1\normalfont}}


% Notations, Math symbols
\newcommand{\symb}[2]{\index[not]{#1: #2}}


% definitions of abbreviations
\newcommand{\abbrev}[2]{#1 (#2)\symb{#2}{#1}}


% definitions of abbreviations
\newcommand{\keywordabbrev}[2]{\emph{#1} (#2)\index[ind]{#1}\symb{#2}{#1}}


% figure and table caption
\newcommand{\tjcaption}[3]{\caption[#1]{\label{#2} \small  #3 \normalsize \\$\quad$\\ }}


\newcommand{\TE}{T\!E}
\newcommand{\TR}{T\!R}

\newcommand{\kspace}{$k$-space~}

\newcommand{\grad}{\ensuremath{^{\circ}}}


\input{lineno}


\makeindex

\newindex{not}{ndx}{nnd}{List of Abbreviations}
\newindex{ind}{idx}{ind}{Index}
%\shortindexingon
%\proofmodetrue



\begin{document}


\normalem


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\author{Thies H. Jochimsen}
\title{Short Tutorial for ODIN, the Object-oriented Development Interface for NMR}
\maketitle

%\Huge
%This is work in progress!
%\normalsize


\tableofcontents

\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{Using ODIN}

%\subsection{Graphical User Interface}

%\subsection{Command-Line Interface}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Sequence Programming}

\subsection{Introduction}
%
In this step-by-step tutorial, we will discover how to program a
\abbrev{magnetic resonance}{MR} sequence with ODIN.
%
As a representative example, a \abbrev{fast spin-echo}{FSE} sequence
(also termed turbo spin echo or RARE sequence)
will be implemented in this tutorial to introduce
ODIN's sequence programming interface and to highlight
the benefits of using a truly object-oriented approach
for sequence development.
%
To fully appreciate this tutorial, you should be familiar
with the concepts of \abbrev{object-oriented programming}{OOP}
(data encapsulation, inheritance, polymorphism),
and in particular, with OOP programming in C++.
%
Before going into detail, here is the full source code
of the FSE sequence:

\listinginput{1}{sequence_listing.cpp} %listing with line numbers from moreverb package

This code can be compiled  by saving it to a
file with extension .cpp and open it in the ODIN user interface by File$\rightarrow$Open.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The MR sequence is a C++ class}
%
Because each MR sequence is an entity with local data
(sequence parameters, sequence objects) and functions
to operate on this data (initialization, execution, simulation),
it is obvious to bundle this structure in a C++ class.
%
This is exactly how  sequences are implemented in ODIN:
You create a class that is derived from the base class of
all sequences, \srckeyword{SeqMethod}.
This is done in \LINENOclass.
%
Please note that instead of using the term sequence, 
the term \keyword{method} is often used instead within ODIN to
emphasize that the sequence as whole is addressed instead
of single sequence objects.

Three remarks:
First, the \verb|#include| statement in \LINENOinclude integrates all headers necessary
for sequence programming.
%
Second, the macro \srckeyword{METHOD\_CLASS} must be used as the class
label for every sequence.\footnote{It will be replaced by a unique string each time
the sequence is compiled within the ODIN user interface so that
each time it is linked into the process space, a unique symbol
is available.} 
%
Third, the macro \srckeyword{ODINMETHOD\_ENTRY\_POINT} in \LINENOentrypoint expands
to some extra code required by ODIN and must be at the end of each sequence file.

Next, we need a public constructor that accepts a string as
a single argument and passes this string to \verb|SeqMethod|
as done in \LINENOconstructor.
%
This constructor is necessary to be able to create an instance
of the class while giving it a unique label which is used
throughout the ODIN framework to refer to this sequence.
%
The wicked \srckeyword{STD\_string} is equivalent to \verb|std::string|
for internal reasons.\footnote{Some ancient systems have none or a broken
C++ standard library and no namespaces. Thus, the macros starting
with \tt{}STD\_\normalfont~ are used instead of \tt{}std::\normalfont~ so that ODIN can
replace them by its own implementations, if necessary.}

Finally, to complete our skeleton of the sequence,
we must implement a bunch of virtual functions as shown in \LINENOvirtual.
%
The purpose of these functions is:

\begin{tabular}{l l}
& \\
\srckeyword{method\_pars\_init} & Initialize sequence parameters \\
\srckeyword{method\_seq\_init}  & Initialize sequence objects and create sequence layout \\
\srckeyword{method\_rels}      & (Re)calculate sequence timings \\
\srckeyword{method\_pars\_set}  & Final preparations before executing the sequence \\
& \\
\end{tabular}

\noindent
The ODIN framework will call these functions during
initialization/preparation of the sequence.
In order to realize our custom sequence, we have to
implement these functions.
Hence, in the remainder of this tutorial, we will fill these
functions with the appropriate code to program our FSE sequence.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Sequence Parameters and Their Initialization}
%
In the following, the term \keyword{sequence parameters}
refers to properties that modify the layout, and hence, the resolution,
timing, etc.~of the sequence. Examples are the \abbrev{echo time}{$\TE$},
the \abbrev{repetition time}{$\TR$} and the \abbrev{field of view}{FOV}.
%
A number of pre-defined sequence parameters are available within
the sequence class. They can be accessed via the following pointers
and contain the specified class of parameters:

\begin{tabular}{l l l}
& & \\
Pointer label:            & Pointing to type:     & Description:  \\
\srckeyword{commonPars}   & \srckeyword{SeqPars}  & Common MR parameters (e.g. $\TE$, $\TR$) \\
\srckeyword{geometryInfo} & \srckeyword{Geometry} & Geometry parameters (e.g. FOV) \\
\srckeyword{systemInfo}   & \srckeyword{System}   & System settings (e.g. field strength) \\
& & \\
\end{tabular}

\noindent
Single parameters of these \keyword{parameter blocks}
can be retrieved and modified with appropriate \verb|get_|
and \verb|set_| methods, as we will see later.
%
We will also refer to these global objects as \keyword{proxies}
because the hide platform specific stuff behind a common interface.
%
Please refer to the online manual for a list of available member functions.


If you write a custom sequence, you probably want
to be able to define and use an extra set of parameters.
%
As it turns out, this is pretty easy in ODIN, all you have to
do is to declare the parameter as a (private) member of your
method class and tell ODIN that this is a new sequence parameter.
%
For example, we will need an extra parameter, \verb|Shots|,
in our FSE sequence for the number of excitations for each image.
%
Thus, we first add the parameter as a member to our class (\LINENOshotsdecl).
%
\srckeyword{LDRint} is the type of our new parameter and makes \verb|Shots|
behave exactly like a built-in \verb|int|.\footnote{There are many
other types emulating built-in types, such as \tt{}LDRfloat\normalfont~
and \tt{}LDRdouble\normalfont. Composite types are also available,
such as \tt{}LDRstring\normalfont, \tt{}LDRcomplex\normalfont~ and arrays.
Please refer to the online manual for a complete list.}
%
Next, we can assign a default value to the parameter in \verb|method_pars_init| (\LINENOshotsdefault).
%
To register the parameter so that it is recognized by ODIN,
for example to display it in the \abbrev{graphical user interface}{GUI},
we must call the function \srckeyword{append\_parameter} in \verb|method_pars_init|
of our sequence (\LINENOshotsappend).
%
After compiling the method, you will see a field with the new
parameter on the right-hand side of the ODIN GUI.
%
As we will see later, the parameter can be used exactly like a regular
\verb|int| in our method code.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Designing the Sequence}
%
In this section, we will create all the sequence objects
required for our FSE sequence.
The term \keyword{sequence objects} refers to all
basic elements of an MR sequence, such as RF pulses,
gradient pulses and periods of data acquisitions.
%
In ODIN, these sequence objects are data members
of the sequence class.
%
In addition, we will see how these sequence objects
are combined into sequence containers and used together
with sequence loops to form the whole sequence.


\subsubsection{RF Pulses}
%
As the FSE sequence is basically a \abbrev{Carr-Purcell-Meiboom-Gill}{CPMG}
sequence, we will need an excitation pulse and a refocusing pulse.
%
The excitation pulse \verb|exc| is declared in \LINENOexcdecl. It is of type
\srckeyword{SeqPulsar}  because it uses the same functions as the Pulsar
program to calculate pulses.
%
The pulse is initialized in \LINENOexcinit by using the constructor
of the specialized pulse class \srckeyword{SeqPulsarSinc}\footnote{
The following goes on behind the scenes: First, a temporary (unnamed)
object of type \tt{}SeqPulsarSinc\normalfont~ is created with the
given parameters. Next, the assignment operator of \tt{}SeqPulsar\normalfont~
is called. Because \tt{}SeqPulsarSinc\normalfont~ is derived from
\tt{}SeqPulsar\normalfont, all data members of \tt{}SeqPulsar\normalfont~
of the temporary object are copied over to \tt{}exc\normalfont,
thereby initializing \tt{}exc\normalfont.}.
Several parameters are passed to the constructor:
The first assigns a label to the object created. It is convenient
to use the same label as that used in source code. The label
will be used to refer to the sequence object, for example
when plotting the sequence.
Next, the constructor expects the slice thickness which
is simply taken from the global geometry proxy pointed to by \verb|geometryInfo|.
The third parameter is set to \verb|true|, to automatically add a rephasing gradient
to the excitation pulse.
Finally, we specify the pulse duration (4th parameter) and
the flip angle (5th parameter).

In an FSE sequence, it is usually a good idea to use the same
pulse shape for excitation and refocusing, except for the flip angle.
%
Thus, for the refocusing pulse \verb|refoc| (declared in \LINENOrefocdecl) we
make use of the assignment operator of \verb|SeqPulsar| to create
a copy of \verb|exc| and change only those properties which differ
(\LINENOrefocinit):
%
First, the label is changed to \verb|refoc|.
Then, the rephasing gradient pulse is removed because
it is not necessary for a refocusing pulse.
Next, the phase is set to 90\grad (CPMG condition) and the flip angle is changed.
Finally, the pulse type is set to \srckeyword{refocusing}\footnote{
This has no direct effect on the sequence time course, but makes
calculation gradient moments (e.g. \kspace analysis)
possible in the plotting GUI.}.


\subsubsection{Gradient Pulses}
%
For phase encoding, i.e scanning of lines in \kspace,
two gradient pulses with variable strength
are required to create a linear phase prior to data acquisition
and to rewind this phase after acquisition.
%
We could use the class \srckeyword{SeqGradVector} to achieve this,
but then we would have to calculate the gradient strengths
ourselves to cover a certain FOV for a certain resolution.
Since we are lazy, and ODIN provides a convenient class
\srckeyword{SeqGradPhaseEnc} to achieve just that,  we will use it
to declare both objects (\LINENOpedecl).
%
The initialization of the first phase encoding gradient
\verb|pe1| in \LINENOpeinit requires some explanation:
%
After specifying the label, resolution (matrix size) and FOV,
the channel of the gradient pulse is set to \verb|phaseDirection|\footnote{
This is one of the three possible channels
\srckeyword{readDirection}, \srckeyword{phaseDirection} and \srckeyword{sliceDirection}
which correspond to the three orthogonal directions in the logical patient
coordinate system, which usually differs from the coordinate system
spanned by the three gradient coils if the imaging plane is tilted.
}.
%
Next, the maximum gradient strength is set to 1/4 of that
possible with the available gradient hardware. The duration
of the gradient pulse will then be calculated to achieve
the desired phase encoding.
%
The argument \srckeyword{centerOutEncoding} specifies
that instead of linear encoding (scanning \kspace from one
edge to the other in sequential order), the central
lines are acquired first and then moving outwards from the center.
%
Finally, the last two arguments are used to separate
the phase encoding lines into
\verb|Shots| interleaves by using \srckeyword{interleavedSegmented}
as the \keyword{reordering scheme}.
We will see later how iterate through the phase
encoding interleaves and shots by sequence loops.
%
In \LINENOpecopy, the phase rewinder \verb|pe2| is initialized
by making it a copy of \verb|pe1| and inverting its polarity.


To suppress free-induction signal of the refocusing pulses,
a pair of spoiler gradients is required around each refocusing
pulse.
%
This fairly simple constant gradient pulse of type \srckeyword{SeqGradConstPulse}
is declared in \LINENOspoilerdecl
and initialized in \LINENOspoilerinit with 1/2 of the maximum gradient
strength and 1 ms duration.


\subsubsection{Data Acquisition}

To sample one line in \kspace between two refocusing pulses in our
FSE sequence, an acquisition period has to be
created together with a simultaneous read gradient.
%
We could create this composite object ourselves by using
a simple acquisition of type \srckeyword{SeqAcq} and a
trapezoidal gradient \srckeyword{SeqGradTrapez}, but again,
ODIN provides a convenient composite class to achieve this, namely
\srckeyword{SeqAcqRead} which is an acquisition together with a
gradient pulse of the same duration.
%
The object \verb|read| of this type is declared in \LINENOreaddecl
and initialized in \LINENOreadinit.
%
As usual, the first argument to the constructor of \verb|SeqAcqRead|
is the object's label. Then, the sweep width (sampling frequency)
is specified together with the matrix size in read direction
(number of sampling points) and the desired FOV. The desired
gradient channel for the gradient is the \verb|readDirection|.


This is a good place to explain the concept of \keyword{Interface base classes},
sometimes called \keyword{interface classes}:
%
An Interface base class
is a class which provides a set of (mostly virtual)
member function. This class cannot be instantiated,
i.e. no object of this class can be created,
but it serves as a base class to concrete implementations
of this set of member functions (the interface).
%
For example the class \verb|SeqAcqRead| implements
two interfaces, namely \srckeyword{SeqAcqInterface},
which is the base class for all acquisition classes,
and \srckeyword{SeqGradInterface}, which is the base
class for all classes with one or more gradient pulses.
%
In this way, all acquisition (or gradient) objects share
a common interface. For instance, the member function
\srckeyword{get\_acquisition\_start} is reimplemented in each
acquisition class to return the duration from the start
of the sequence object to the beginning of the actual
acquisition (this is necessary for proper timing calculations,
see below).
%
The implementation of this function can be very different,
depending on the concrete acquisition class used:
%
\verb|SeqAcq| will return the duration to trigger the
digitizer, and \verb|SeqAcqRead| will also consider
the duration of the gradient ramp of the read gradient.
%
However, the programming interface is the same in both
cases which makes it easy to remember the member functions and, more importantly,
allows \keyword{polymorphism}: If different acquisition
types should be supported within one sequence, member
functions can be called via a pointer
(or reference) to type \verb|SeqAcqInterface|
instead of several \verb|if|-statements.


In order to start sampling at one edge of \kspace in read
direction, we also need a pre-dephasing gradient pulse.
%
Again, we could create this ourselves using a \verb|SeqGradConstPulse|
object, but there is a more convenient class \srckeyword{SeqAcqDeph}
which can be employed. It is declared in \LINENOdephdecl.
After the obligatory label as the first argument to
the constructor (\LINENOdephinit), a reference to 
an object derived from \verb|SeqAcqInterface| is expected for which
the dephaser will be created.
%
As we have seen above, the object \verb|read| is derived
from \verb|SeqAcqInterface| and can therefore be used as
the second argument so that a suitable pre-dephaser will be created.
%
The last argument \verb|spinEcho| specifies that the pre-dephaser
will have the same polarity as the read gradient which is suitable
for spin-echo (and, therefore, CPMG) acquisition.



\subsubsection{Sequence Containers and Delays}
%
Having initialized all our basic sequence elements, we now start
to group them together to build the whole MR sequence.
%
For this purpose, \keyword{sequence containers} of type
\srckeyword{SeqObjList} are used: For example,
we will group the objects within one refocusing interval together 
in \LINENOechopartinit and assign this composite
object to \verb|echopart| (declared in \LINENOechopartdecl)
so that we can loop over this part of the sequence later.
%
As you can see, sequence objects are concatenated by using the + operator.

However, just concatenating objects is not enough for our
FSE sequence, we must also fulfill CPMG timing constraints.
%
Therefore, the \keyword{sequence delay} \verb|echodelay| of type
\srckeyword{SeqDelay} (declared in \LINENOechodelaydecl
and initialized in \LINENOechodelayinit\footnote{Although
setting the label is not crucial, it helps debugging as this label
shows up in all visualizations of the sequence.}) is used to insert
certain waiting periods into the sequence.
%
We will calculate the duration of this delay later together
with the duration of other delay objects.
%
For now, we just create the basic layout of the sequence.

It is also convenient to group the preparation phase
into the object \verb|prep| (\LINENOprepdecl) as shown in
\LINENOprepinit.
%
Here, the delay \verb|prepdelay| (\LINENOprepdelaydecl, \LINENOprepdelayinit) will
be used to achieve CPMG timing.
%
Again, we use the + operator to concatenate sequence
objects. But since \verb|spoiler| and \verb|deph|
are on different gradient channels, they can be played
out simultaneously. This is exactly what the / operator
does: It makes sequence objects simultaneous, if possible.
Otherwise, it will generate an error message.



\subsubsection{Sequence Loops}
%
Next, we will build the sequence part of one shot, that is,
the preparation part \verb|prep| followed by a loop over
\verb|echopart| (the sequence part of one echo).
%
In ODIN, loops are possible with the class \srckeyword{SeqObjLoop},
which is, like \verb|SeqObjList|, a container to hold other
sequence objects in sequential order. In the following,
this contents will be called the \keyword{kernel} of the loop.
%
In addition, \verb|SeqObjLoop| allows looping over the kernel
to repeat its execution.
%
At each iteration, properties of so-called \keyword{sequence vectors}
can be changed. These sequence vectors are all classes
derived from \srckeyword{SeqVector}.
%
Thereby, a loop can be employed as follows:
%
First, a loop object is declared in \LINENOecholoopdecl.
Then, to iterate over a kernel \verb|kern| while iterating
properties of \verb|vec1|, \verb|vec2|, ..., the basic syntax
is\footnote{This syntax becomes possible by overloading the ( ) and
[ ] operator of \tt{}SeqObjLoop\normalfont.}
\begin{verbatim}
echoloop ( kern ) [vec1][vec2]...
\end{verbatim}
%
In our sequence, this syntax is used in \LINENOoneshotinit
to iterate over \verb|echopart| while iterating the values
of \verb|pe1| and \verb|pe2|. Here iterating simply means
to change the strength of the phase encoding gradient so
that different lines in \kspace are scanned at each echo,
according to the encoding and reordering scheme specified
above.



But we are not done yet because we have only build the sequence
for one shot.
%
Thus, we need another loop \verb|shotloop| (\LINENOshotloopdecl)
to iterate over the different shots.
%
At each of these iterations, \verb|pe1| and \verb|pe2|
have to be aware that the next interleave of phase encoding
steps has to be used.
%
For this purpose, each sequence vector has a \keyword{reordering vector}.
Whenever a certain reordering scheme is used
(as done above by the constructor of \verb|SeqGradPhaseEnc|
or by the function  \srckeyword{set\_reorder\_scheme}),
the reordering vector is used to iterate over the set
of modified arrangements of the values of the vector,
for example over the interleaves, if \verb|interleavedSegmented|
is used as the reordering scheme.
%
The reordering vector, just like every other vector,
can be attached to a loop to iterate over it.
%
It can be retrieved from its parent vector by the
function \srckeyword{get\_reorder\_vector}.
%
This is done in \LINENOsetsequence: The reordering
vectors of \verb|pe1| and \verb|pe2| are attached
to the loop \verb|shotloop| while repeating \verb|oneshot|.
%
A padding delay \verb|relaxdelay| (\LINENOrelaxdelaydecl, \LINENOrelaxdelayinit) is also added
which we use later to adjust the correct $\TR$.

Within the same line, the function \srckeyword{set\_sequence}
of the base class \verb|SeqMethod| is used to specify the whole
sequence, i.e. to tell the ODIN framework what to use
when plotting/executing/simulating our sequence.
%
Using this function is usually the last step in \verb|method_seq_init|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{(Re)Calculating Sequence Timings}

In the previous section, we have created the basic layout of the sequence,
but ignored the timing constraints of our FSE sequence.
%
This will be done now, namely in the member function \verb|method_rels|
of our method class\footnote{Using a separate member function for timing
calculations, which are usually fast compared to all the initialization
stuff in \tt{}method\_seq\_init\normalfont, has the advantage that,
if only timing settings of the sequence change, \tt{}method\_rels\normalfont~
is the only function to be called.
}.
%
First, we will calculate the minimum $\TE$ possible
which is equivalent to the CPMG refocusing interval.
%
This either limited by the minimum duration possible with
our preparation (\LINENOminTEprep),
or by the refocusing interval (\LINENOminTEecho).
%
Here, the functions \srckeyword{get\_duration}
and \srckeyword{get\_magnetic\_center}
return the duration of the corresponding sequence object\footnote{
The container \tt{}echopart\normalfont~ is a sequence object itself
whereby \tt{}get\_duration\normalfont~ returns the total duration
of the concattenated objects it contains.}
and the time point of the center of the pulse relative to its
beginning, respectively.
%
Next, we calculate the maximum of both in \LINENOminTE
and set the global $\TE$ (in \verb|commonars|) to this value (\LINENOsetTE).
Here, the second argument \srckeyword{noedit} indicates
that the parameter cannot be edited by the user, it will
be indicated as such in the GUI.
%
Finally, we adjust the duration of the padding delays to match the
calculated $\TE$ in \LINENOprepdelayset and \LINENOechodelayset
using the \srckeyword{set\_duration} function.

A similar technique is used to set the minimum $\TR$ possible
and adjust the corresponding padding delay (\LINENOsetTR).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Image Reconstruction}

The sequence is now complete and could be executed on the scanner,
but image reconstruction would fail because ODIN does not know where
to put each line, which was acquired with the \verb|read| object, in \kspace.
We have to  inform ODIN explicitely that the vector object \verb|pe1| was responsible
for phase encoding. This is done in \LINENOrecoline. The first argument
of \srckeyword{set\_reco\_vector} is of type \srckeyword{recoDim} which
is an enum used to specify so-called \keyword{reconstruction dimensions}.
These can be, for instance:
%
\begin{itemize}
\item Phase encoding step (\srckeyword{line})
\item Slice index (\srckeyword{slice})
\item Echo number (\srckeyword{echo})
\item User-defined index (\srckeyword{userdef})
\end{itemize}
%
The second argument is a vector object which was responsible to iterate
through this dimension.
%
For example, if we would add multi-slice excitation, we would
add a frequency list to \verb|exc| (using \srckeyword{set\_freqlist})
and then use \verb|exc| as the slice vector.




After execution of the sequence,
the complex data of the acquired NMR signal is stored on disk
in the so-called \abbrev{scan directory}{scandir} in the order it was acquired.
This is achieved by using the native mechanisms (e.g. on Bruker and GE) or by
a custom reconstruction routine which does nothing but dumping the raw
data to the hard drive (e.g. on Siemens).
%
However, just storing the raw data would not be enough to reconstruct
an image. Extra information like the one above (location of each acquisition
in \kspace) is required.
Therefore, an extra file \srckeyword{recoInfo}, which contains this information
in human-readable ASCII format, is stored together with the raw data in the scandir.
%
The file is a serialized version of the class \srckeyword{RecoPars}, which means
that on object of this type can read/write the file and queried subsequently
for its values.

Finally, the program \srckeyword{odinreco}, which is part of ODIN will retrieve
all information from \verb|recoInfo| and generate image data.
It is executed in the scandir on the command line with
\begin{verbatim}
odinreco
\end{verbatim}
to create the reconstructed image data. Please refer to the online manual for
further documentation how to fine tune the reconstruction.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\subsection{Sequence Internals}

%In this section, the internal layout of the example sequence will be described,
%i.e. how the sequence objects used are composed of other more basic sequence objects.

%\subsubsection{Excitation pulse}
%
%We will start with the excitation pulse \verb|exc|: It is of type \srckeyword{SeqPulsarSinc}
%which is derived from \srckeyword{SeqPulsar} (class for representing arbitrary RF pulses).
%In the constructor of \srckeyword{SeqPulsarSinc}, various pulse parameters (shape, filter, ...)
%of \srckeyword{SeqPulsar} are set so that a sinc-shaped pulse is generated.


%The class \srckeyword{SeqPulsar} has two base classes: \srckeyword{SeqPulsNdim}
%and \srckeyword{OdinPulse}. The latter contains the logic to calculate
%RF and gradient waveforms for frequency and/or spatially selective pulses.
%It has a number of \verb|set|/\verb|get| functions to set pulse specific
%parameters.
%
%Since \srckeyword{OdinPulse} is a bass class of \srckeyword{SeqPulsar}, these
%\verb|set|/\verb|get| functions are all available in \srckeyword{SeqPulsar}
%to specify all details of a pulse in the method.
%
%However, \srckeyword{OdinPulse} does not know how to insert itself into the sequence.
%Therefore, the second base class \srckeyword{SeqPulsNdim} contains sub-objects
%to play out the calculated RF and gradient shapes in the sequence:
%
%In essence, it contains an atomic RF object \srckeyword{SeqPuls} and three
%atomic \srckeyword{SeqGradWave} to play out the gradient waveform(s).

%The class \srckeyword{SeqPuls} will talk to the actual scanner hardware via
%the driver plugin for RF (the member \verb|pulsdriver|).
%
%This member is a wrapper which has a pointer-like syntax.
%With this 'pointer', a driver interface \srckeyword{SeqPulsDriver} is accessible.
%The actual object behind this interface is the an implementation of \srckeyword{SeqPulsDriver}
%of the current platform.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\addcontentsline{toc}{section}{\numberline{}Index}
\printindex[ind]

\newpage
\addcontentsline{toc}{section}{\numberline{}List of Abbreviations}
\printindex[not]


\end{document}
